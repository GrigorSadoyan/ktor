<?php
/**
 * Created by PhpStorm.
 * User: sadoy
 * Date: 3/17/2018
 * Time: 1:53 PM
 */

namespace Model;
use Core\Model;

class PagesContent extends Model
{

    protected $table = 'pages_content';
    protected  $pdoObject;
    public $result;

    public function __construct()
    {
        parent::__construct();
    }
}

//        page_id   == 1   =>  ГЛАВНАЯ
//        page_id   == 2   =>  ЗАКАЗ ШТОР
//        page_id   == 3   =>  ЦЕНЫ
//        page_id   == 4   =>  ВИДЫ ШТОР
//        page_id   == 5   =>  ПОРТФОЛИО