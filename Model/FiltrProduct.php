<?php

namespace Model;
use Core\Model;
use PDO as PDO;
/**
 * Description of AdminLog
 *
 * @author Karen
 */
class FiltrProduct extends Model{

    protected $table = 'filtr_product';

    public function __construct()
    {
        parent::__construct();
    }

    public function getSelectedGroup($productId)
    {
        $querySql = "SELECT `filtr_name`.* FROM `filtr_product` ".
                    "JOIN `filtr_name` ON `filtr_name`.`id` = `filtr_product`.`filtr_name_id`".
                    "WHERE `filtr_product`.`product_id`=:productId GROUP BY `filtr_product`.`filtr_name_id`";

        $query = $this->db->prepare($querySql);
        $query->execute(array(":productId"=>$productId));
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getSelectedFiltr($productId)
    {
        $querySql = "SELECT `filtrs_id` FROM `filtr_product` WHERE `product_id`=:productId ";

        $query = $this->db->prepare($querySql);
        $query->execute(array(":productId"=>$productId));
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}