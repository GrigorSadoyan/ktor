<?php

namespace Model;
use Core\Model;
use PDO as PDO;
/**
 * Description of AdminLog
 *
 * @author Karen
 */
class Filtrs extends Model{

    protected $table = 'filtrs';

    public function __construct()
    {
        parent::__construct();
    }

    public function getCurrentFiltr(array $product)
    {
        $allFiltrView = array();
        $aMainFiltrView = array();
        $mFiltrProduct = new FiltrProduct;

        foreach ($product as $valProduct){
            $aFiltrProduct = $mFiltrProduct->findByMultyName(
                array(
                    "product_id"=>$valProduct['id']
                )
            );
            if($aFiltrProduct && count($aFiltrProduct) > 0){
                //$allFiltrView[] = $aFiltrProduct;
                if($aFiltrProduct && count($aFiltrProduct) > 0){
                    foreach ($aFiltrProduct as $val){
                        $existFiltr = true;
                        foreach ($allFiltrView as $item) {
                            if($val['filtrs_id'] == $item['filtrs_id']){
                                $existFiltr = false;
                                break;
                            }
                        }
                        if($existFiltr){
                            $allFiltrView[] = $val;
                        }
                    }

                }
            }

        }

        $aFiltrViewGroup = $this->_group_by($allFiltrView,'filtr_name_id');
       
        $mFiltrName = new FiltrName;
        $eachI = 0;
        //$mFiltrs = new Filtrs();
        foreach ($aFiltrViewGroup as $key=>&$value){

            foreach ($value as &$val){
                $val['filtrs_origin'] = $this->findById($val['filtrs_id']); // $this = $mFiltrs
            }
            $aMainFiltrView[$eachI] = $mFiltrName->findById($key);
            $aMainFiltrView[$eachI]['filtrs'] = $aFiltrViewGroup[$aMainFiltrView[$eachI]['id']];
            $eachI++;

        }
        return $aMainFiltrView;
    }
}