<?php
/**
 * Created by PhpStorm.
 * User: sadoy
 * Date: 3/17/2018
 * Time: 1:53 PM
 */

namespace Model;
use Core\Model;

class Album extends Model
{

    protected $table = 'album';
    protected  $pdoObject;
    public $result;

    public function __construct()
    {
        parent::__construct();
    }
}

//        page_id   == 1   =>  НОВОСТИ КОМПАНИИ
//        page_id   == 2   =>  ПОЛЕЗНЫЕ СТАТЬИ
//        page_id   == 3   =>  ВСЕ О ТЕКСТИЛЕ