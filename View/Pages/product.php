<?php
$urls = trim($_SERVER['REQUEST_URI'], '/\\');
$url= explode("/", $urls);
?>
<div class="col-lg-12 nopadding clear">
    <div class="success_bag">
        Товар Успешно Добовлен.
    </div>
    <div class="content clear">
        <div class="col-lg-12 nopadding clear prod_mains">
            <div class="col-lg-6">
                <div class="prod_img">
                    <img src="<?=$baseurl?>/assets/images/product/<?=$params['result']['image']?>" />
                </div>
                <div class="prod_desc mobnone">
                    <?=$params['result']['description']?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="prod_name">
                    <p><?=$params['result']['name']?></p>
                </div>
                <div class="prod_code">
                    <p><?=$params['result']['code']?></p>
                </div>
                <?php if($params['result']['type_id'] == '1'){ ?>
<!--                    --><?php // echo '<pre>'; var_dump($params['prices']);die;  ?>
                    <div class="prod_price">
                        <p>
                            <span>
                                <?=$params['prices'][0]['sale_price'] == 0
                                || $params['prices'][0]['sale_price'] == ''
                                || is_null($params['prices'][0]['sale_price'])
                                    ?
                                    $params['prices'][0]['price']
                                    :
                                    '<del>'. $params['prices'][0]['price'] .' руб</del>'. $params['prices'][0]['sale_price']
                                ?>
                            </span>
                            руб
                        </p>
                    </div>
                    <div class="prod_colors">
                        <span class="pr_c_text">Цвет:</span>
                        <?php foreach($params['fil_colors'] as $color){ ?>
                        <span class="pr_c_cols" data-colid="<?=$color['main_color']['id']?>" style="background-color: #<?=$color['main_color']['name']?>"></span>
                        <?php } ?>
                    </div>
                <?php  } ?>
                <?php if($params['result']['type_id'] == '2'){ ?>
                    <div class="prod_price" style="display: none">
                        <p><span></span> руб</p>
                    </div>
                    <div class="kar_ryads_main clear">
                        <p>Количество рядов</p>
                        <div class="kar_ryads"  data-ryad="0">
                            <p class="kar_ry_count activeRyad">1</p>
                            <p class="kar_ry_items">
                                <span class="kar_ry_items_one"></span>
                            </p>
                        </div>
                        <div class="kar_ryads" data-ryad="1">
                            <p class="kar_ry_count">2</p>
                            <p class="kar_ry_items">
                                <span></span>
                                <span></span>
                            </p>
                        </div>
                    </div>
                    <?php foreach ($params['data'] as $data){ ?>
                        <div class="kar_ryads_dlin clear dlin_of_ryad_<?=key($data)?>">
                            <p>Длина ,см</p>
                            <?php foreach ($data as $val){ ?>
                                <span class="dlina_items" data-dlina="<?=$val['id']?>" data-price="<?= $val['price']?>" data-sale-price="<?= $val['sale_price']?>"><?=$val['dlina']?></span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                <?php  } ?>
                <script>

                </script>
                <div class="prod_count clear">
                    <div class="count_text">
                        Количество:
                    </div>
                    <div class="count_arrow bor_none_arr" data-arrow="-">
                        <i class="fa fa-angle-down"></i>
                    </div>
                    <div class="count_int">1</div>
                    <div class="count_arrow"  data-arrow="+">
                        <i class="fa fa-angle-up"></i>
                    </div>
                </div>
                <div class="prod_to clear mobnone">
                    <div class="col-lg-6 nopadleft">
                        <p class="prod_v_izb">в избранное</p>
                    </div>
                    <div class="col-lg-6 nopadright">
                        <p class="prod_v_korz _to_bag">в корзину</p>
                    </div>
                </div>
                <div class="prod_opt">
                    <div class="prod_xar">
                        <p class="pr_xar">
                            Характеристики
                            <span class="pr_close"><i class="fa fa-angle-down"></i></span>
                            <span class="pr_open op_arr"><i class="fa fa-angle-up"></i></span>
                        </p>
                        <div class="pr_xar_text">
                            <?=$params['result']['description']?>
                        </div>
                    </div>
                </div>
                <div class="prod_opt">
                    <div class="prod_xar">
                        <p class="pr_xar">
                            избранное
                            <span class="pr_close"><i class="fa fa-angle-down"></i></span>
                            <span class="pr_open op_arr"><i class="fa fa-angle-up"></i></span>
                        </p>
                        <div class="pr_xar_text clear">
                            <div class="col-lg-12 pr_example">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding pr_example pr_example_for">
                                    <div class="izb_butt">
                                        <p>в корзину</p>
                                    </div>
                                    <img src="<?=$baseurl?>/assets/images/content/ktor1.jpg" />
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 pr_example">
                                    <p class="izb_prod_name">Неизвестное имя</p>
                                    <p class="izb_prod_price">4550,00 руб</p>
                                </div>
                            </div>
                            <div class="col-lg-12 pr_example">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding pr_example pr_example_for">
                                    <div class="izb_butt">
                                        <p>в корзину</p>
                                    </div>
                                    <img src="<?=$baseurl?>/assets/images/content/ktor1.jpg" />
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 pr_example">
                                    <p class="izb_prod_name">Неизвестное имя</p>
                                    <p class="izb_prod_price">4550,00 руб</p>
                                </div>
                            </div>
                            <div class="col-lg-12 pr_example">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding pr_example pr_example_for">
                                    <div class="izb_butt">
                                        <p>в корзину</p>
                                    </div>
                                    <img src="<?=$baseurl?>/assets/images/content/ktor1.jpg" />
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 pr_example">
                                    <p class="izb_prod_name">Неизвестное имя</p>
                                    <p class="izb_prod_price">4550,00 руб</p>
                                </div>
                            </div>




<!--                            <div class="col-lg-4 col-xs-6  pr_exmp">-->
<!--                                <div class="col-lg-12 pr_example">-->
<!--                                    <img src="--><?//=$baseurl?><!--/assets/images/content/ktor2.jpg" />-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-lg-4 col-xs-6  pr_exmp">-->
<!--                                <div class="col-lg-12 pr_example">-->
<!--                                    <img src="--><?//=$baseurl?><!--/assets/images/content/ktor3.jpg" />-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-lg-4 col-xs-6  pr_exmp">-->
<!--                                <div class="col-lg-12 pr_example">-->
<!--                                    <img src="--><?//=$baseurl?><!--/assets/images/content/ktor1.jpg" />-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-lg-12 pr_exmp_b">-->
<!--                                <span>показать больше</span>-->
<!--                            </div>-->
                        </div>
                    </div>
                </div>

                <div class="prod_to clear pcnone">
                    <div class="col-lg-6 col-xs-6 nopadleft">
                        <p class="prod_v_izb">в избранное</p>
                    </div>
                    <div class="col-lg-6 col-xs-6 nopadright">
                        <p class="prod_v_korz">в корзину</p>
                    </div>
                </div>
                <div class="prod_desc pcnone">
                    <p> <?=$params['result']['description']?>​</p>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        if($('.pr_c_cols').length === 1){
            $('.pr_c_cols')[0].click();
        }
        $('.dlin_of_ryad_0').css({
            'display':'block'
        });
        $('.dlin_of_ryad_1').css({
            'display':'none'
        });
    })
    $('.pr_c_cols').click(function () {
        $('.pr_c_cols').removeClass('activeColor');
        $(this).addClass('activeColor');
    })
    $('.count_arrow').click(function () {
        var nshan = $(this).attr('data-arrow');
        var myInt = parseInt($('.count_int').text());
        if(nshan == '-'){
            if(myInt == 1)
                return false;
            var myInt = myInt -= 1;
            $('.count_int').text(myInt);
        }
        if(nshan == '+'){
            var myInt = myInt += 1;
            $('.count_int').text(myInt);
        }
    })
    $('.pr_close').click(function () {
        $('.pr_xar_text').slideUp(300);
        $('.pr_close').removeClass('op_arr');
        $('.pr_open').addClass('op_arr');
        $(this).next('.pr_open').removeClass('op_arr');
        $(this).addClass('op_arr');
        $(this).parent('p').next('.pr_xar_text').slideDown(300);
    })
    $('.pr_open').click(function () {
        $('.pr_xar_text').slideUp(300);
        $('.pr_close').removeClass('op_arr');
        $('.pr_open').addClass('op_arr');
        $(this).prev('.pr_close').removeClass('op_arr');
        $(this).addClass('op_arr');
        $(this).parent('p').next('.pr_xar_text').slideUp(300);
    })

    $('.kar_ryads').click(function () {
        $('.kar_ry_count').removeClass('activeRyad');
        $(this).children('.kar_ry_count').addClass('activeRyad');
        var ryadid = $(this).attr('data-ryad');
        $('.kar_ryads_dlin').css({
            'display':'none'
        });
        $('.dlin_of_ryad_'+ryadid).css({
            'display':'block'
        })
    });
    $('.kar_ryads_dlin span').click(function () {
        $('.kar_ryads_dlin span').removeClass('activeDlin');
        $(this).addClass('activeDlin');
        var price = $(this).attr('data-price');
        var sale_price = $(this).attr('data-sale-price');
        if(sale_price === '0'){
            $('.prod_price p span').text(price);
        }else{
            var delsText = '<del>'+price+'</del>'+ sale_price;
            $('.prod_price p span').html(delsText);
        }

        $('.prod_price').css({
            'display':'block'
        })
    });

    $('._to_bag').click(function () {
        var typeId = <?=$url[1]?>;
        if(typeId == '1'){
            var ColorId = $('.prod_colors').find('.activeColor').attr('data-colid');
            var count = $('.count_int').text();
            var prodId = <?=$params['result']['id']?>;
            if(typeof ColorId == 'undefined'){
                alert('Выберите Цвет');
                return false;
            };
            var body = 'id='+prodId+'&colorid='+ColorId+'&count='+count+'&typeId='+typeId;
            var url = base+"/bag/add/";
            requestPost(url,body,function(){
                if(this.readyState == 4){
                    var result = JSON.parse(this.responseText);
                    if(result.error){
                        $('.count_int').text('1');
                        $('.success_bag').css({
                            'top':'50px'
                        });
                        setTimeout(function () {
                            $('.success_bag').css({
                                'top':'-10000px'
                            });
                        },2000);
                        var hCount = $('.count_kr').text();
                        console.log(hCount);
                        $('.count_kr').text(parseInt(hCount) + 1);
                    }else{
                        alert('Something Went Wrong. Please Try again');
                    }
                }
            })
        }else if(typeId == '2'){
            var prodId = <?=$params['result']['id']?>;
            var count = $('.count_int').text();
            var dlinaId = $('.activeDlin').attr('data-dlina');
            if(typeof dlinaId == 'undefined'){
                alert('Выберите Длину Карниза');
                return false;
            };
            var body = 'id='+prodId+'&dlinaId='+dlinaId+'&count='+count+'&typeId='+typeId;
            var url = base+"/bag/add/";
            requestPost(url,body,function(){
                if(this.readyState == 4){
                    var result = JSON.parse(this.responseText);
                    if(result.error){
                        $('.count_int').text('1');
                        $('.success_bag').css({
                            'top':'50px'
                        });
                        setTimeout(function () {
                            $('.success_bag').css({
                                'top':'-10000px'
                            });
                        },2000);
                        var hCount = $('.count_kr').text();
                        console.log(hCount);
                        $('.count_kr').text(parseInt(hCount) + 1);
                    }else{

                    }
                }
            })
        }

    })

</script>