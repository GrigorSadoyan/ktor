<div class="col-lg-12 nopadding">
    <div class="cover">
        <img src="<?=$baseurl?>/assets/images/content/<?=$params['cover_info']['image']?>">
        <div class="cover_white"></div>
        <div class=" text_block">
            <p class="cover_title"><?=$params['cover_info']['text_1']?></p>
            <p class="cover_text"><?=$params['cover_info']['text_2']?></p>
            <p class="cover_text"><span><?=$params['cover_info']['text_3']?></span></p>
        </div>
    </div>
</div>
<div class="col-lg-12 nopadding clear">
    <div class="podb_v">Цены на пошив штор на заказ</div>
    <div class="content">
        <div class="col-lg-12 nopadding oc_main clear">
            <?php foreach ($params['result'] as $val) { ?>
            <div class="col-lg-4 col-xs-12 col-sm-6 pod_main  clear">
                <div class="col-lg-12 pod_items nopadding clear">
                    <div class="zitems_g">
                        <img src="<?=$baseurl?>/assets/images/content/ceni/<?=$val['image']?>" alt="room1" />
                    </div>
                    <div class="">
                        <div class="podb_v_name_title"><?=$val['name']?></div>
                        <div class="podb_v_name_text"><?=$val['text']?></div>
                    </div>
                    <div class="col-lg-12 nopadding clear pod_pri">
                        <div class="col-lg-4 col-xs-4 ">
                            <p class="podb_price_name">Basic</p>
                            <p class="podb_price">
                                от <b><?=$val['basic_price']?> </b><img src="<?=$baseurl?>/assets/images/content/icons/rubli.png" />
                            </p>
                        </div>
                        <div class="col-lg-4 col-xs-4">
                            <p class="podb_price_name">Style</p>
                            <p class="podb_price">
                                от <b><?=$val['style_price']?> </b><img src="<?=$baseurl?>/assets/images/content/icons/rubli.png" />
                            </p>
                        </div>
                        <div class="col-lg-4 col-xs-4">
                            <p class="podb_price_name">Premium</p>
                            <p class="podb_price">
                                от <b><?=$val['premium_price']?> </b><img src="<?=$baseurl?>/assets/images/content/icons/rubli.png" />
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-12 nopadding pod_bottom">
                        <p>Категории между собой отличаются классом ткани.</p>
                    </div>
                </div>
            </div>
          <?php } ?>



    </div><i class="fa fa-ruble-sign"></i>
</div>