<?php
//    echo '<pre>';
//    var_dump($params['result']);die;
?>

<div class="col-lg-12 nopadding clear">
    <div class="podb_v"><?=$params['result']['title']?></div>
    <div class="content">
        <div class="col-lg-12 nopadding oc_main clear">
            <div class="col-lg-6">
                <div class="tc_img">
                    <img src="<?=$baseurl?>/assets/images/content/album/<?=$params['result']['image']?>" alt="room1"/>
                </div>
            </div>
            <div class="col-lg-6">
                <!--                <p class="tc_text">Портьеры - основа текстильного оформления Вашего интерьера.-->
                <!--                    Ранее портьерами называли плотные шторы, которыми украшали Французские замки, утеплив окна и создав роскошный уют.-->
                <!--                    Сегодня, в современном мире, портьерами называют прямые шторы, располагавшиеся симметрично по краям окна.​</p>-->
                <!--                <p class="tc_title">Портьеры это:</p>-->
                <!--                <p class="tc_text">-->
                <!--                    Универсальное решение для любого помещения-->
                <!--                    Подходящий вид оформления окна для любого стиля-->
                <!--                    Прекрасная защита от солнца и посторонних взглядов​​.-->
                <!--                </p>-->
                <!--                <p class="tc_text">-->
                <!--                    Игрой тканью и декоративными элементами дизайнеры Homm воплотят любые Ваши желания. Например, легкой портьерой из льна, Вы можете привнести деревенский уют в Вашу комнату, а с помощью бархатных портьер - изысканную роскошь.-->
                <!--                </p>-->
                <!--                <p class="tc_title">-->
                <!--                    Декоративные портьеры</p>-->
                <!--                <p class="tc_text">-->
                <!--                    Прямые шторы, расположенные по краям окна, выполняют лишь эстетическую задачу декора окна. Декоративные шторы часто используют в помещениях, где портьеры не зашторивают или в сочетании с другими функциональными шторами, например, с римскими или рулонными шторами.-->
                <!--                </p>-->
                <!--                <p class="tc_title">-->
                <!--                    Функциональные портьеры</p>-->
                <!--                <p class="tc_text">-->
                <!--                    Шторы, которые выполняют непосредственную свою задачу - закрывают от солнца и посторонних взглядов. Конечно, рабочие шторы должны свободно двигаться по карнизу для ежедневного использования.-->
                <!--                </p>-->
                <?=$params['result']['text']?>
            </div>
            <div class="col-lg-12">
                <p class="ord_tc">Заказать <?=$params['result']['title']?></p>
            </div>
        </div>
        <div class="col-lg-12 nopadding oc_main clear">
            <div class="col-lg-12 clear nopadding pol_all">
                <?php foreach ($params['all_result'] as $all){
                        if($all['id'] == $params['result']['id']){continue;}else{
                ?>
                    <div class="col-lg-4 port_item">
                        <a href="<?=$baseurl?>/album/<?=$all['id']?>/">
                            <div class="port_img">
                                <img src="<?=$baseurl?>/assets/images/content/album/<?=$all['image']?>" />
                                <div class="type_img_wh"></div>
                            </div>
                            <div class="port_name">
                                <p><?=$all['title']?> </p>
                            </div>
                        </a>
                    </div>
                <?php } } ?>
            </div>
        </div>
    </div>
</div>
