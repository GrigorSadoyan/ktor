<?php
//echo '<pre>';
//    var_dump($params['result']);
?>
<div class="col-lg-12  clear">
    <div class="podb_v">корзина</div>
    <div class="content clear">
        <div class="col-lg-12 clear   center mobnone ">
            <div class="borderBottom clear">
                <div class="col-lg-6 nopadding ">
                    Примеры (<span class="counts_in_bag"><?=$this->bagCount?></span>)
                </div>
                <div class="col-lg-2 bagthcenter nopadding ">
                    <span>Цена</span>
                </div>
                <div class="col-lg-2 bagthcenter nopadding ">
                    <span>К-во</span>
                </div>
                <div class="col-lg-2 bagth nopadding ">
                    <span>Итого</span>
                </div>
            </div>
        </div>

<!--        ********************************BagBody*****************************************-->
        <div class="col-lg-12 clear  mobnopadd  center ">
            <?php $i = 0; foreach($params['result'] as $val){?>
                <div class="trBag clear">
                    <div class="col-lg-6 col-xs-12 trBag_on  nopadding ">
                        <div class="col-lg-5 col-xs-4 bagImg ">
                            <img src="<?=$baseurl?>/assets/images/product/<?=$val['product']['image']?>" alt="<?=$val['product']['name']?>">
                        </div>
                        <div class="col-lg-7 col-xs-8 blockInfoProBag ">
                            <p class="bagtitle">
                                <?=$val['product']['name']?>
                            </p>
                            <p class="codeBag"><?=$val['product']['code']?></p>
                            <?php  if($val['type_id'] == '1'){ ?>
                                <p class="bagColor"><span style="background-color: #<?=$val['feature']['name']?>"></span></p>
                            <?php  }elseif($val['type_id'] == '2'){ ?>
                                <p class="bagColor">Ряд: <?=$val['feature']['shtor_ryad_id']?></p>
                                <p class="bagColor">Длина: <?=$val['feature']['dlina']?> см</p>
                            <?php } ?>
                            <span class="bagRemove" data-prid="<?=$val['id']?>">Удалить</span>
                        </div>

                    </div>
                    <div class="col-lg-2 col-xs-12  mobnone bagprice nopadding ">
                        <?php  if($val['type_id'] == '1'){ ?>
                            <?php  if(is_null($val['feature']['cena'][0]['sale_price']) || $val['feature']['cena'][0]['sale_price']== '0' || $val['feature']['cena'][0]['sale_price'] == ''){ ?>
                                <p><span class="one_pr_<?=$i?>"><?=$val['feature']['cena'][0]['price']?></span> руб.</p>
                            <?php  }else{ ?>
                                <p><span class="one_pr_<?=$i?>"><del><?=$val['feature']['cena'][0]['price']?> </del> <?=$val['feature']['cena'][0]['sale_price']?></span> руб.</p>
                            <?php  } ?>
                        <?php  }elseif($val['type_id'] == '2'){ ?>
                            <?php  if(is_null($val['feature']['sale_price']) || $val['feature']['sale_price'] == '0' || $val['feature']['sale_price'] == ''){ ?>
                                <p><span class="one_pr_<?=$i?>"><?=$val['feature']['price']?></span> руб.</p>
                            <?php }else{ ?>
                                <p><span class="one_pr_<?=$i?>"><del><?=$val['feature']['sale_price']?> </del> <?=$val['feature']['sale_price']?></span> руб.</p>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="col-lg-2 col-xs-5 col-xs-offset-4  col-lg-offset-0  mobCount bagpadd nopadding  ">
                        <div class="prod_count bagcount clear">
                            <div class="count_arrow bor_none_arr" data-arrow="-" data-back="<?=$val['id']?>">
                                <i class="fa fa-angle-down"></i>
                            </div>
                            <div class="count_int op_count_<?=$i?>"><?=$val['count']?></div>
                            <div class="count_arrow" data-arrow="+"  data-back="<?=$val['id']?>">
                                <i class="fa fa-angle-up"></i>
                            </div>
                            <i class="fa fa-times-circle badMobDel fa-2x" aria-hidden="true"></i>
                        </div>

                    </div>
                    <div class="col-lg-2 col-xs-3  priceMobile nopadding  ">
                        <p><span class="op_pr_<?=$i?>"></span> РУБ.</p>
                    </div>
                </div>
            <?php
                $i++;
            }
            ?>
        </div>
    <!--        ******************************Finish**BagBody*****************************************-->

    </div>
</div>
<div class="content">
<div class="col-lg-12 col-xs-12 col-sm-offset-5 col-lg-offset-0 col-sm-7">
    <div class="col-lg-6 col-xs-12 nopadding ">
<!--        <p class="cupon">Использовать купон</p>-->
<!--        <div class="baginputgrup ">-->
<!--            <input type="text" placeholder="укажите код купона">-->
<!--            <button>Отправить</button>-->
<!--            <span class="tag"><i class="fa fa-tags" aria-hidden="true"></i></span>-->
<!--        </div>-->
    </div>
    <div class="col-lg-5 col-xs-12 col-lg-offset-1  ">
<!--        <div class="chekBlockBag clear">-->
<!--            <div class="col-lg-12 nopadding ">-->
<!--                <div class="col-lg-6 col-xs-6 mobpadd  ">-->
<!--                    <p class="suma">Сумма</p>-->
<!--                    <span class="SumBtnBag activeBag" data-type="1"><span class="chek"><i class="fa fa-check" aria-hidden="true"></i></span>самовывоз</span>-->
<!--                </div>-->
<!--                <div class="col-lg-6 col-xs-6 mobpadd ">-->
<!--                    <p class=" suma text_right "><span class="_result_priceing"></span> руб.</p>-->
<!--                    <span class="SumBtnBag" data-type="2"><span class="chek"><i class="fa fa-check" aria-hidden="true"></i></span>доставка</span>-->
<!--                </div>-->
<!--            </div>-->
<!--             <div class="col-lg-12 mobpadd ">-->
<!--                 <div class="blockcountry"><span class="country">Россия:</span>-->
<!--                     <select>-->
<!--                         <option>Armenia</option>-->
<!--                         <option>Russa</option>-->
<!--                     </select>-->
<!--                 </div>-->
<!--             </div>-->
<!--            <div class="col-lg-12 mobpadd ">-->
<!--                <div class="col-lg-6 col-xs-6  nopadding ">-->
<!--                    <p class=" suma ">Доставка</p>-->
<!--                </div>-->
<!--                <div class="col-lg-6 col-xs-6 nopadding ">-->
<!--                    <p class=" suma text_right">БЕСПЛАТНО</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <div class="col-lg-12 mobpadd ">
            <div class="col-lg-6 col-xs-6 nopadding ">
                <p class=" suma boldResult ">Итого</p>
            </div>
            <div class="col-lg-6 col-xs-6 nopadding ">
                <p class=" suma boldResult text_right"><span class="_result_priceing"></span> руб.</p>
            </div>
        </div>
        <p class="text_right"><span class="btnSEnd">отправить заявку</span></p>
    </div>
</div>
</div>
<div class="clear"></div>
<div class="deverly_main">
    <div class="delivery_second">
        <div class="close_sels"><i class="fa fa-times" aria-hidden="true"></i></div>
        <div class="delivery_title">
            Оформите Заказ
        </div>
        <div class="bag_rows">
            <label>Имя</label>
            <input type="text" name="name" class="del_input_col req_bag_col name_bag_inp" />
        </div>
        <div class="bag_rows">
            <label>Телефон</label>
            <input type="text" name="phone" class=" phone_bag_inp del_input_col req_bag_col" />
        </div>
        <div class="bag_rows">
            <label>E-mail</label>
            <input type="email" name="email" class="email_bag_inp del_input_col req_bag_col" />
        </div>

        <div class="bag_rows">
            <label>Адрес</label>
            <input type="text" name="address" class="address_bag_inp del_input_col req_bag_col" />
        </div>
        <div class="bag_rows">
            <div class="bag_row_check">
                <div>
                    <span>В пределах МКАД</span>
                    <input type="radio" name="place" class="ride_inps" data-ride="1" />
                </div>
                <div>
                    <span>За пределами МКАД-а</span>
                    <input type="radio" name="place" class="ride_inps" data-ride="2" />
                </div>
            </div>
        </div>
        <div class="bag_rows del_for_km " id="del_for">
            <label>Расстояние от МКАД (км)</label>
            <input type="number" name="kilomethers"  class="rast_do_mkad " />
        </div>
        <div class="bag_rows">
            <label>Дополнительно</label>
            <input type="text" name="addititonal" class="aditional_b"/>
        </div>
        <div class="bag_rows">
            <label>Код купона</label>
            <input type="text" name="kupon" class="cupon_b"/>
        </div>
        <div class="bag_rows">
            <div class="bag_row_check">
                <div>
                    <span>Оплата на месте</span>
                    <input type="radio" name="payment" data-pay="1" class="type_inps"/>
                </div>
                <div>
                    <span>Оплата онлайн</span>
                    <input type="radio" name="payment" data-pay="2"  class="type_inps"/>
                </div>
                <div>
                    <span>Самовывоз</span>
                    <input type="radio" name="payment" data-pay="3"  class="type_inps"/>
                </div>
            </div>
        </div>
        <div class="bag_rows">
            <label>Итоговая Сумма</label>
            <input type="text" disabled="disabled" class="cart_itog"/>
        </div>
        <div class="verify_payment">
            Продолжить
        </div>
    </div>
</div>
<script>
    readPrices();
    function readPrices() {
        var resulrRubli;
        var prodCount = $('.trBag').length;
        // console.log(prodCount);
        var resultPrice = 0;
        for(var i = 0; i<prodCount; i++){
            var onePrice = $('.one_pr_'+i).text();
            var Count = $('.op_count_'+i).text();
           // console.log(Count);
            var Oneresult = parseInt(onePrice) * parseInt(Count);
            $('.op_pr_'+i).text(Oneresult);
            resultPrice += Oneresult;
        }
        $('._result_priceing').text(resultPrice);
        resulrRubli = parseFloat(resultPrice);
        return resulrRubli;
    }

    $('.count_arrow').click(function () {
        var nshan = $(this).attr('data-arrow');
        var myInts = parseInt($(this).parent('.prod_count').find('.count_int').text());
        if(nshan == '-'){
            if(myInts == 1)
                return false;
            var myInt = myInts -= 1;
            $(this).parent('.prod_count').find('.count_int').text(myInts);
        }
        if(nshan == '+'){
            var myInts = myInts += 1;
            $(this).parent('.prod_count').find('.count_int').text(myInts);
        }
        var id = $(this).attr('data-back');
        var count = myInts;
        var url = base+"/bag/update/";
        var body = "id="+id+"&count="+count+"";
        requestPost(url, body, function () {
            if(this.readyState == 4){
                readPrices();
            }
        })
    });
    $('.bagRemove').click(function () {
        var self = $(this);
        var id= $(this).attr('data-prid');
        var url = base+"/bag/delete/";
        var body = "id="+id+"";

        requestPost(url, body, function () {
            if(this.readyState == 4){
                var result = JSON.parse(this.responseText);
                if(result.error){
                    self.parent('.blockInfoProBag ').parent('.trBag_on').parent('.trBag').fadeOut();
                    var hCount = $('.count_kr').text();
                    $('.count_kr').text(parseInt(hCount) - 1);
                    $('.counts_in_bag').text(parseInt(hCount) - 1);
                    readPrices();
                }else{

                }
            }
        })
    });

    $('.SumBtnBag').click(function () {
        $('.SumBtnBag').removeClass('activeBag');
        $(this).toggleClass('activeBag');
    });
    $('.btnSEnd').click(function () {
        $('.deverly_main').addClass('deverlyActive');
        $('.cart_itog').val(readPrices());
    });
    function rideAndPay() {
        var allD = $('.del_input_col');
        for(var i = 0; i < allD.length; i++){
            //console.log(i,$(allD[i]).val())
            if($(allD[i]).val() === ''){
                $(allD[i]).addClass('error_allD')
            }
        }
        //console.log(allD);
    }
    $('.verify_payment').click(function () {
        var ride = $('.ride_inps:checked').attr('data-ride');
        var type = $('.type_inps:checked').attr('data-pay');
        if(typeof ride === 'undefined'){
            alert('Уточните место отправки.')
        }else{
            if(ride === '2'){
                $('#del_for').removeClass('del_for_km');
                $('.rast_do_mkad').addClass('req_bag_col');
                $('.rast_do_mkad').change(function () {
                    var kms = $(this).val();
                    var sPrice = $('.cart_itog').val();
                    var result = (30 * parseFloat(kms))+ parseFloat(sPrice);
                    $('.cart_itog').val(result);
                });
            }else{
                $('#del_for').addClass('del_for_km');
                $('.rast_do_mkad').removeClass('req_bag_col');
                rideAndPay();
            }

        }

        if(typeof type === 'undefined'){
            alert('Выберите метод платежа')
        }else{
            // var x = $('.del_input_col');
            // console.log('aaaa', x)
        }
        if(typeof type !== 'undefined' && typeof ride !== 'undefined'){
            var requires = $('.req_bag_col');
            var t = 0;
            for(var i = 0; i < requires.length; i++){
                if($(requires[i]).val() == ''){
                    $(requires[i]).addClass('error_allD');
                    t++;
                }
            }
            if(t > 0){
                return false
            }else{
                if(ride == '1'){
                    var mile = 500;
                }
                if(ride == '2'){
                    var mile =   $('.rast_do_mkad').val() * 30;
                }
                var url = base+"/bag/checkout/";
                var body = '';
                body += 'guest='+getCookie('gc');
                body += '&name='+$('.name_bag_inp').val();
                body += '&phone='+$('.phone_bag_inp').val();
                body += '&email='+$('.email_bag_inp').val();
                body += '&address='+$('.address_bag_inp').val();
                body += '&ride='+ride;
                body += '&mile='+mile;
                body += '&type='+type;
                body += '&cupone='+$('.cupon_b').val();
                body += '&aditional='+$('.aditional_b').val();
                console.log(body)
                requestPost(url,body, function () {

                })
            }
        }
    })


    $('.ride_inps').change(function () {
        var ride = $(this).attr('data-ride');;
        if(ride == '2'){
            $('#del_for').removeClass('del_for_km');
        }else{
            $('#del_for').addClass('del_for_km');
        }
    })
    $('.req_bag_col').change(function () {
        $(this).removeClass('error_allD');
    })
    $('.close_sels').click(function () {
        $('.deverly_main').removeClass('deverlyActive');
    })
</script>