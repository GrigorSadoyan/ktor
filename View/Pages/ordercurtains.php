<div class="col-lg-12 nopadding">
    <div class="cover">
        <img src="<?=$baseurl?>/assets/images/content/<?=$params['cover_info']['image']?>">
        <div class="cover_white"></div>
        <div class=" text_block">
            <p class="cover_title"><?=$params['cover_info']['text_1']?></p>
            <p class="cover_text"><?=$params['cover_info']['text_2']?></p>
            <p class="cover_text"><span><?=$params['cover_info']['text_3']?></span></p>
            <p class=""><span class="btn_cover" data-toggle="modal" data-target="#myModal">Получить Дизайн-проект</span></p>
        </div>
    </div>
</div>
<div class="col-lg-12 nopadding clear">
    <div class="podb_v">Как мы работаем</div>
    <div class="content">
        <?php  for($i = 0; $i < count($params['result']); $i++){  ?>
            <div class="oc_main clear">
                <div class="oc_left">
                    <p class="oc_title"><?=$params['result'][$i]['title']?></p>
                    <div class="oc_text">
                        <?=$params['result'][$i]['text']?>
                    </div>
<!--                    <p class="oc_text">В нашей компании для обустройства Вашего дома можно использовать ткани со всего мира: Италия, Испания, Бельгия, Германия, США, Индия, Китай, Турция. ​</p>-->
<!--                    <p class="oc_text">Наш специалист поможет Вам подобрать материалы максимально подходящие Вам по дизайну и качеству. ​</p>-->
<!--                    <p class="oc_text">При большом ассортименте всегда можно найти уникальное решение именно для Вас! ​</p>-->
                    <?php if($i == 0){ ?>
                        <p class="oc_button">позвоните нам​</p>
                    <?php } ?>
                </div>
                <div class="oc_right">
                    <div class="oc_imgs">
                        <img src="<?=$baseurl?>/assets/images/content/OrderCurtains/<?=$params['result'][$i]['image']?>" alt="<?=$params['result'][$i]['title']?>"/>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
