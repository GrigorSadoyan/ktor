<div class="podb_v">избранное</div>

<div class="izb_main mobnone">
    <div class="content clear">
        <div class="col-lg-12 clear nopadding">
            <?php foreach ($params['result'] as $items) { ?>
                <div class="col-lg-3  how_we_z">
                    <div class="pod_img">
                        <div class="pod_image" style="background-image: url('../../assets/images/product/<?=$items['product']['image']?>')"></div>
                        <a class="pod_link" href="<?=$baseurl?>/product/<?=$items['product']['type_id']?>/<?=$items['product']['id']?>"></a>
<!--                        <div class="pod_korz">-->
<!--                            <div class="pod_dirq">-->
<!--                                <div class="pod_knok_likeIn pod_knoks">-->
<!--                                    <p>в корзину</p>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="del_likes" data-del="<?=$items['id']?>">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="swiper-container izbrni-slider pcnone">
    <div class="swiper-wrapper">
        <div class="swiper-slide  slider-items slider_img  ">
           <div class="imgMobIzbrni">
               <img src="http://ktor.hk/assets/images/content/ktor3.jpg" alt="ktor1">
           </div>
            <div class="mobtextizbrni">
            <p class="pod_img_text">Ткань Хлопковая принтованная с рисунком Cozyland Zigzag Italian Brown</p>
            <p class="pod_img_text"><span>2300</span> руб.</p>
            </div>
        </div>
        <div class="swiper-slide  slider-items slider_img  ">
            <div class="imgMobIzbrni">
                <img src="http://ktor.hk/assets/images/content/ktor2.jpg" alt="ktor1">
            </div>
            <div class="mobtextizbrni">
                <p class="pod_img_text">Ткань Хлопковая принтованная с рисунком Cozyland Zigzag Italian Brown</p>
                <p class="pod_img_text"><span>2300</span> руб.</p>
            </div>
        </div>
        <div class="swiper-slide  slider-items slider_img  ">
            <div class="imgMobIzbrni">
                <img src="http://ktor.hk/assets/images/content/ktor1.jpg" alt="ktor1">
            </div>
            <div class="mobtextizbrni">
                <p class="pod_img_text">Ткань Хлопковая принтованная с рисунком Cozyland Zigzag Italian Brown</p>
                <p class="pod_img_text"><span>2300</span> руб.</p>
            </div>
        </div>



    </div>
</div>
<script>
    var izbrni = new Swiper('.izbrni-slider', {
        loop: false,
        slidesPerView: 2,
        navigation: {
            nextEl: '.swiper-button-nexts-work',
            prevEl: '.swiper-button-prevs-work',
        },
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        breakpoints:{
            480:{         slidesPerView: 1.4,
                spaceBetween: 1,
                slidesOffsetAfter: -20,
                slidesOffsetBefore:-20,


            },

        }
    });
    $('.del_likes').click(function () {
        var self = $(this);
        var id = $(this).attr('data-del');
        var body = 'id='+id;
        var url = base+'/like/delete/';
        requestPost(url, body, function () {
            if(this.readyState == 4){
                var result = JSON.parse(this.responseText);
                if(!result.error){
                    self.parent('.pod_img').parent('.how_we_z').fadeOut();
                    var countss = parseInt($('.count_li').text());
                    countss --;
                    $('.count_li').text(countss);
                }else{

                }
            }
        })
    })
</script>