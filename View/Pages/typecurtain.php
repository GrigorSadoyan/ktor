<div class="col-lg-12 nopadding">
    <div class="cover">
        <img src="<?=$baseurl?>/assets/images/content/<?=$params['cover_info']['image']?>">
        <div class="cover_white"></div>
        <div class=" text_block">
            <p class="cover_title"><?=$params['cover_info']['text_1']?></p>
            <p class="cover_text"><?=$params['cover_info']['text_2']?></p>
            <p class="cover_text"><span><?=$params['cover_info']['text_3']?></span></p>
        </div>
    </div>
</div>

<?php foreach ($params['result'] as $param) { ?>
    

<div class="col-lg-12 nopadding clear ">
    <div class="podb_v"><?=$param['name']?></div>
    <div class="swiper-container shtor-slider pcnone">
        <div class="swiper-wrapper">
            <div class="swiper-slide  slider-items slider_img type_img ">
                <div class="col-lg-3 type_items">
                    <div class="type_img">
                        <img src="<?=$baseurl?>/assets/images/content/ktor3.jpg" alt="ktor1">
                        <div class="type_img_wh"></div>
                    </div>
                    <div class="type_img_tMain">
                        <p class="type_name">Тюль</p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide  slider-items slider_img type_img ">
                <div class="col-lg-3 type_items">
                    <div class="type_img">
                        <img src="<?=$baseurl?>/assets/images/content/ktor2.jpg" alt="ktor1">
                        <div class="type_img_wh"></div>
                    </div>
                    <div class="type_img_tMain">
                        <p class="type_name">Тюль</p>
                    </div>
                </div>
            </div>
        </div>

    </div>








    <div class="content mobnone">
        <div class="col-lg-12 nopadding oc_main clear">
            <?php foreach ($param['items'] as $item) { ?>
            <div class="col-lg-3 type_items">
                <a href="<?=$baseurl?>/typecurtain/<?=$item['id']?>/">
                    <div class="type_img">
                        <img src="<?=$baseurl?>/assets/images/content/curtaincat/<?=$item['image']?>" alt="ktor1">
                        <div class="type_img_wh"></div>
                    </div>
                    <div class="type_img_tMain">
                       <p class="type_name"><?=$item['name']?></p>
                    </div>
                </a>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<?php } ?>



<!--<div class="col-lg-12 nopadding clear">-->
<!--    <div class="podb_v">Виды помещений</div>-->
<!--    <div class="swiper-container shtor2-slider pcnone">-->
<!--        <div class="swiper-wrapper">-->
<!--            <div class="swiper-slide  slider-items slider_img type_img">-->
<!--                <div class="col-lg-3 type_items">-->
<!--                    <div class="type_img">-->
<!--                        <img src="--><?//=$baseurl?><!--/assets/images/content/ktor1.jpg" alt="ktor1">-->
<!--                        <div class="type_img_wh"></div>-->
<!--                    </div>-->
<!--                    <div class="type_img_tMain">-->
<!--                        <p class="type_name">Тюль</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="swiper-slide  slider-items slider_img type_img">-->
<!--                <div class="col-lg-3 type_items">-->
<!--                    <div class="type_img">-->
<!--                        <img src="--><?//=$baseurl?><!--/assets/images/content/ktor4.jpg" alt="ktor1">-->
<!--                        <div class="type_img_wh"></div>-->
<!--                    </div>-->
<!--                    <div class="type_img_tMain">-->
<!--                        <p class="type_name">Тюль</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--    </div>-->
<!---->
<!---->
<!---->
<!---->
<!---->
<!--    <div class="content mobnone">-->
<!--        <div class="col-lg-12 nopadding oc_main clear">-->
<!---->
<!--            <div class="col-lg-3 type_items">-->
<!--                <div class="type_img">-->
<!--                    <img src="--><?//=$baseurl?><!--/assets/images/content/ktor3.jpg" alt="ktor1">-->
<!--                    <div class="type_img_wh"></div>-->
<!--                </div>-->
<!--                <div class="type_img_tMain">-->
<!--                    <p class="type_name">Гостиная</p>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<script>
    var pagesslider = new Swiper('.shtor-slider', {
        loop: true,
        slidesPerView: 1.4,
        navigation: {
            nextEl: '.swiper-button-nexts-work',
            prevEl: '.swiper-button-prevs-work',
        },
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        breakpoints:{
            480:{ slidesPerView: 1.4},
            768:{ slidesPerView: 2},
            1024:{slidesPerView: 3}
        }
    });
    var pagesslider2 = new Swiper('.shtor2-slider', {
        loop: true,
        slidesPerView: 1.4,
        navigation: {
            nextEl: '.swiper-button-nexts-work',
            prevEl: '.swiper-button-prevs-work',
        },
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        breakpoints:{
            480:{ slidesPerView: 1.4},
            768:{ slidesPerView: 2},
            1024:{slidesPerView: 3}
        }
    });
</script>