<link rel="stylesheet" href="<?= $baseurl ?>/assets/fancybox/source/jquery.fancybox.css?v=2.1.5">
<link rel="stylesheet" href="<?= $baseurl ?>/assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5">
<link rel="stylesheet" href="<?= $baseurl ?>/assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7">

<script src="<?= $baseurl ?>/assets/fancybox/lib/jquery.mousewheel.pack.js?v=3.1.3"></script>
<script src="<?= $baseurl ?>/assets/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<script src="<?= $baseurl ?>/assets/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script src="<?= $baseurl ?>/assets/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script src="<?= $baseurl ?>/assets/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<div class="col-lg-12 nopadding">
    <div class="cover">
        <img src="<?=$baseurl?>/assets/images/content/<?=$params['cover_info']['image']?>">
        <div class="cover_white"></div>
        <div class=" text_block">
            <p class="cover_title"><?=$params['cover_info']['text_1']?></p>
            <p class="cover_text"><?=$params['cover_info']['text_2']?></p>
            <p class="cover_text"><span><?=$params['cover_info']['text_3']?></span></p>
            <p class=""><span class="btn_cover" data-toggle="modal" data-target="#myModal">Получить Дизайн-проект</span></p>
            <p><a href="<?=$baseurl?>/typecurtain/"><span class="more_cover ">Узнать больше о шторах</span></a></p>
        </div>
    </div>
</div>

<div class="col-lg-12 nopadding ">
    <div class="content clear ">
        <p class="slider_title">Добро пожаловать</p>
        <p class="col-lg-4 center textlider">omm - перевоплощение ткани в источник Ваших самых приятных ощущений уюта и Дома</p>

        <div class="swiper-container product-slider">
            <div class="swiper-wrapper">
                <?php foreach ($params['m_gallery'] as $gals) { ?>
                    <div class="swiper-slide  slider-items slider_img ">
                        <img src="<?=$baseurl?>/assets/images/content/glav_slider/<?=$gals['image']?>">
                        <div class="info_block">
                            <p class="pro_slide_title">Эскиз в подарок!</p>
                            <p><span class="btn_slider " data-toggle="modal" data-target="#myModal">Получить</span></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="swiper-button-nexts"><i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i></div>
            <div class="swiper-button-prevs"><i class="fa fa-chevron-left fa-2x" aria-hidden="true"></i></div>

        </div>
        <div class="col-lg-12 nopadding">
            <div class="how_we_work_g">Как мы работаем</div>
            <?php foreach ($params['how_we_work'] as $how_we_work){ ?>
                <div class="col-lg-3">
                    <div class="how_we_img">
                        <img src="<?=$baseurl?>/assets/images/content/icons/<?=$how_we_work['image']?>" />
                    </div>
                    <div class="how_we_title">
                        <span><?=$how_we_work['title']?></span>
                    </div>
                    <div class="how_we_text">
                        <span>
                            <?=$how_we_work['text']?>
                        </span>
                    </div>
                </div>
            <?php  } ?>
            <div class="col-lg-12 how_we_more">
                <a href="<?=$baseurl?>/about"><span>Подробнее</span></a>
            </div>
        </div>

        <div class="how_we_work_g">Портфолио</div>
        <div class="swiper-container portpholio-slider pcnone">
            <div class="swiper-wrapper">
                <?php foreach ($params['m_gallery'] as $gals) { ?>
                    <div class="swiper-slide  slider-items slider_img   ">
                        <a href="<?=$baseurl?>/cattkan/1/">
                            <div class="col-lg-4 mopadd zitems_mains">

                                <div class="zitems_g">
                                    <div class="leyer"></div>
                                    <img src="<?=$baseurl?>/assets/images/content/glav_slider/<?=$gals['image']?>" />
                                </div>
                                <p class="zItemsBl_p">
                                <span class="zItemsBl_title">
                                    Шторы на заказ в гостиную
                                </span></br>
                                    <span class="zItemsBl_text">
                                    Проект текстильного оформления студии в стиле лофт. Окно в зоне гостиной оформлено однотонной органзой и  портьерами под лен. В зоне кухни - непрозрачные римские шторы. Зона отдыха декорирована органзой используемой на окне в гостиной
                                </span>
                                </p>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
            <div class="col-lg-12 col-xs-12 how_we_more">
                <span>показать больше</span>
            </div>
        </div>

        <div class="col-lg-12 nopadding mobnone">

<!--            <a href="--><?//=$baseurl?><!--/cattkan/1/">-->
<!--                <div class="col-lg-4 zitems_mains">-->
<!--                    <div class="zitems_g">-->
<!--                        <img src="--><?//=$baseurl?><!--/assets/images/content/room1.jpg" />-->
<!--                        <div class="zItemsBl">-->
<!--                            <p class="zItemsBl_p">-->
<!--                                <span class="zItemsBl_title">-->
<!--                                    Шторы на заказ в гостиную-->
<!--                                </span></br>-->
<!--                                <span class="zItemsBl_text">-->
<!--                                    Проект текстильного оформления студии в стиле лофт. Окно в зоне гостиной оформлено однотонной органзой и  портьерами под лен. В зоне кухни - непрозрачные римские шторы. Зона отдыха декорирована органзой используемой на окне в гостиной-->
<!--                                </span>-->
<!--                            </p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </a>-->
<!--            <div class="col-lg-4">-->
<!--                <div class="zitems_g zitems_mains">-->
<!--                    <img src="--><?//=$baseurl?><!--/assets/images/content/room2.jpg" />-->
<!--                    <div class="zItemsBl">-->
<!--                        <p class="zItemsBl_p">-->
<!--                            <span class="zItemsBl_title">-->
<!--                                Шторы на заказ в гостиную-->
<!--                            </span></br>-->
<!--                            <span class="zItemsBl_text">-->
<!--                                Проект текстильного оформления студии в стиле лофт. Окно в зоне гостиной оформлено однотонной органзой и  портьерами под лен. В зоне кухни - непрозрачные римские шторы. Зона отдыха декорирована органзой используемой на окне в гостиной-->
<!--                            </span>-->
<!--                        </p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-lg-4">-->
<!--                <div class="zitems_g zitems_mains">-->
<!--                    <img src="--><?//=$baseurl?><!--/assets/images/content/room3.jpg" />-->
<!--                    <div class="zItemsBl">-->
<!--                        <p class="zItemsBl_p">-->
<!--                            <span class="zItemsBl_title">-->
<!--                                Шторы на заказ в гостиную-->
<!--                            </span></br>-->
<!--                            <span class="zItemsBl_text">-->
<!--                                Проект текстильного оформления студии в стиле лофт. Окно в зоне гостиной оформлено однотонной органзой и  портьерами под лен. В зоне кухни - непрозрачные римские шторы. Зона отдыха декорирована органзой используемой на окне в гостиной-->
<!--                            </span>-->
<!--                        </p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-lg-4">-->
<!--                <div class="zitems_g zitems_mains">-->
<!--                    <img src="--><?//=$baseurl?><!--/assets/images/content/room1.jpg" />-->
<!--                    <div class="zItemsBl">-->
<!--                        <p class="zItemsBl_p">-->
<!--                            <span class="zItemsBl_title">-->
<!--                                Шторы на заказ в гостиную-->
<!--                            </span></br>-->
<!--                            <span class="zItemsBl_text">-->
<!--                                Проект текстильного оформления студии в стиле лофт. Окно в зоне гостиной оформлено однотонной органзой и  портьерами под лен. В зоне кухни - непрозрачные римские шторы. Зона отдыха декорирована органзой используемой на окне в гостиной-->
<!--                            </span>-->
<!--                        </p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-lg-4">-->
<!--                <div class="zitems_g zitems_mains">-->
<!--                    <img src="--><?//=$baseurl?><!--/assets/images/content/room2.jpg" />-->
<!--                    <div class="zItemsBl">-->
<!--                        <p class="zItemsBl_p">-->
<!--                            <span class="zItemsBl_title">-->
<!--                                Шторы на заказ в гостиную-->
<!--                            </span></br>-->
<!--                            <span class="zItemsBl_text">-->
<!--                                Проект текстильного оформления студии в стиле лофт. Окно в зоне гостиной оформлено однотонной органзой и  портьерами под лен. В зоне кухни - непрозрачные римские шторы. Зона отдыха декорирована органзой используемой на окне в гостиной-->
<!--                            </span>-->
<!--                        </p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-lg-4">-->
<!--                <div class="zitems_g zitems_mains">-->
<!--                    <img src="--><?//=$baseurl?><!--/assets/images/content/room3.jpg" />-->
<!--                    <div class="zItemsBl">-->
<!--                        <p class="zItemsBl_p">-->
<!--                            <span class="zItemsBl_title">-->
<!--                                Шторы на заказ в гостиную-->
<!--                            </span></br>-->
<!--                            <span class="zItemsBl_text">-->
<!--                                Проект текстильного оформления студии в стиле лофт. Окно в зоне гостиной оформлено однотонной органзой и  портьерами под лен. В зоне кухни - непрозрачные римские шторы. Зона отдыха декорирована органзой используемой на окне в гостиной-->
<!--                            </span>-->
<!--                        </p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <?php foreach ($params['portfolio'] as $valp) { ?>
                <div class="col-lg-4 col-sm-6 port_item port_slide_open" data-ids="1">
                    <div class="port_img">
                        <a rel="example_group_<?=$valp['id']?>" title="<?=$valp['gallery'][0]['title']?>" href="<?=$baseurl?>/assets/images/content/portfolio/<?=$valp['gallery'][0]['image']?>">
                            <img alt="<?=$valp['name']?>" src="<?=$baseurl?>/assets/images/content/portfolio/<?=$valp['gallery'][0]['image']?>">
                        </a>
                        <?php for($i=1;$i<count($valp['gallery']);$i++) { ?>
                            <a rel="example_group_<?=$valp['id']?>" title="<?=$valp['gallery'][$i]['title']?>" href="<?=$baseurl?>/assets/images/content/portfolio/<?=$valp['gallery'][$i]['image']?>" style="display:none;"></a>
                        <?php } ?>
<!--                        <div class="type_img_wh"></div>-->
                    </div>
                    <div class="port_name">
                        <p><?=$valp['name']?> </p>
                    </div>
                </div>
            <?php } ?>
            <div class="col-lg-12  zitems_more">
                <a href="<?=$baseurl?>/portfolio/"><span>Подробнее</span></a>
            </div>
        </div>
    </div>



    <div class="main_gr_row">
        <div class="content">
            <div  class="gr_row_title">
                <span>Заказать БЕСПЛАТНЫЙ выезд дизайнера с образцами тканей </span>
            </div>
            <div class="gr_forms clear">
                <div class="gr_name gr_rows">
                    <input type="text" class="s_req_name" placeholder="УКАЖИТЕ ВАШЕ ИМЯ"/>
                    <div class="gr_row_abs">
                        <i class="fa fa-user"></i>
                    </div>
                </div>
                <div class="gr_phone gr_rows">
                    <input type="text"  class="s_req_phone" placeholder="УКАЖИТЕ ВАШ ТЕЛЕФОН"/>
                    <div class="gr_row_abs">
                        <i class="fa fa-phone"></i>
                    </div>
                </div>
                <div class="gr_email gr_rows">
                    <input type="text" class="s_req_email" placeholder="УКАЖИТЕ ВАШ EMAIL"/>
                    <div class="gr_row_abs">
                        <i class="fa fa-envelope"></i>
                    </div>
                </div>
                <div class="gr_call_butt gr_rows">
                    <p class="send_sButton">заказать звонок</p>
                </div>
            </div>
        </div>
    </div>



<!--        HAYKO   -->

    <div class="content">
        <div class="how_we_work_g">
            ПОДБЕРЕМ ДЛЯ ВАС
            <a href="<?=$baseurl?>/cattkan/">
                <span class="pod_tk">ТКАНИ</span>
            </a>
        </div>
        <div class="swiper-container portpholio-slider pcnone">
            <div class="swiper-wrapper">
                <div class="swiper-slide  slider-items slider_img podbermimg  ">
                    <div class="pod_img">
                        <img src="<?=$baseurl?>/assets/images/content/ktor1.jpg" alt="ktor1">
                            <div class="pod_dirqs">
                                <div class="pod_knok_izb pod_knoks">
                                    <p>избранное</p>
                                </div>
                                <div class="pod_knok_korz pod_knoks">
                                    <p>в корзину</p>
                                </div>
                            </div>
                    </div>
                    <div class="pod_img_tMain">
                        <p class="pod_img_text">Ткань Хлопковая принтованная с рисунком Cozyland Zigzag Italian Brown</p>
                        <p class="pod_img_text"><span>2300</span> руб.</p>
                    </div>
                </div>
                <div class="swiper-slide  slider-items slider_img podbermimg  ">
                    <div class="pod_img">
                        <img src="<?=$baseurl?>/assets/images/content/ktor1.jpg" alt="ktor1">7
                        <div class="pod_dirqs">
                            <div class="pod_knok_izb pod_knoks">
                                <p>избранное</p>
                            </div>
                            <div class="pod_knok_korz pod_knoks">
                                <p>в корзину</p>
                            </div>
                        </div>
                    </div>
                    <div class="pod_img_tMain">
                        <p class="pod_img_text">Ткань Хлопковая принтованная с рисунком Cozyland Zigzag Italian Brown</p>
                        <p class="pod_img_text"><span>2300</span> руб.</p>
                    </div>
                </div>
                <div class="swiper-slide  slider-items slider_img podbermimg  ">
                    <div class="pod_img">
                        <img src="<?=$baseurl?>/assets/images/content/ktor1.jpg" alt="ktor1">7
                        <div class="pod_dirqs">
                            <div class="pod_knok_izb pod_knoks">
                                <p>избранное</p>
                            </div>
                            <div class="pod_knok_korz pod_knoks">
                                <p>в корзину</p>
                            </div>
                        </div>
                    </div>
                    <div class="pod_img_tMain">
                        <p class="pod_img_text">Ткань Хлопковая принтованная с рисунком Cozyland Zigzag Italian Brown</p>
                        <p class="pod_img_text"><span>2300</span> руб.</p>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-xs-12 how_we_more">
                <span>показать больше</span>
            </div>
        </div>


<!--       / HAYKO   -->


        <div class="col-lg-12 nopadding clear mobnone">
            <?php foreach ($params['tkani'] as $items) { ?>
            <div class="col-lg-3  how_we_z">
                <div class="pod_img">
                    <div class="pod_image" style="background-image: url('../../assets/images/product/<?=$items['image']?>')"></div>
                    <a class="pod_link" href="<?=$baseurl?>/product/<?=$items['type_id']?>/<?=$items['url']?>/<?=$items['id']?>"></a>
                    <div class="pod_korz">
                        <div class="pod_dirq">
                            <div class="pod_knok_izb pod_knoks">
                                <p class="__addlike" data-like-id="<?=$items['id']?>" >избранное</p>
                            </div>
                            <div class="pod_knok_korz pod_knoks">
                                <a  href="<?=$baseurl?>/product/<?=$items['type_id']?>/<?=$items['id']?>">
                                    <p>в корзину</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pod_img_tMain">
                    <p class="pod_img_text"><?=$items['name']?></p>
                    <p class="pod_img_text"><span>
                            <?=
                            is_null($items['prices'][0]['sale_price'])
                            || $items['prices'][0]['sale_price'] == ''
                            || $items['prices'][0]['sale_price'] == 0
                                ? $items['prices'][0]['price']
                                : '<del>'.$items['prices'][0]['price'].' руб.</del>'.$items['prices'][0]['sale_price']
                            ?>
                        </span> руб.</p>
                </div>
            </div>
            <?php } ?>
            <div class="col-lg-12">
                <a href="<?=$baseurl?>/catalog/ткань/1"><p class="pod_more">показать больше</p></a>
            </div>
        </div>


        <!--       HAYKO   -->


        <div class="how_we_work_g">ПОДБЕРЕМ ДЛЯ ВАС <span class="pod_tk">КАРНИЗЫ</span></div>
        <div class="swiper-container portpholio-slider pcnone">
            <div class="swiper-wrapper">
                <div class="swiper-slide  slider-items slider_img podbermimg  ">
                    <div class="pod_img">
                        <img src="<?=$baseurl?>/assets/images/content/ktor1.jpg" alt="ktor1">7
                        <div class="pod_dirqs">
                            <div class="pod_knok_izb pod_knoks">
                                <p>избранное</p>
                            </div>
                            <div class="pod_knok_korz pod_knoks">
                                <p>в корзину</p>
                            </div>
                        </div>
                    </div>
                    <div class="pod_img_tMain">
                        <p class="pod_img_text">Ткань Хлопковая принтованная с рисунком Cozyland Zigzag Italian Brown</p>
                        <p class="pod_img_text"><span>2300</span> руб.</p>
                    </div>
                </div>
                <div class="swiper-slide  slider-items slider_img podbermimg  ">
                    <div class="pod_img">
                        <img src="<?=$baseurl?>/assets/images/content/ktor1.jpg" alt="ktor1">7
                        <div class="pod_dirqs">
                            <div class="pod_knok_izb pod_knoks">
                                <p>избранное</p>
                            </div>
                            <div class="pod_knok_korz pod_knoks">
                                <p>в корзину</p>
                            </div>
                        </div>
                    </div>
                    <div class="pod_img_tMain">
                        <p class="pod_img_text">Ткань Хлопковая принтованная с рисунком Cozyland Zigzag Italian Brown</p>
                        <p class="pod_img_text"><span>2300</span> руб.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-xs-12 how_we_more">
                <span>показать больше</span>
            </div>
        </div>
        <div class="col-lg-12 nopadding clear mobnone">


            <!--       / HAYKO   -->

            <?php foreach ($params['shtori'] as $val) { ?>
            <div class="col-lg-3 how_we_z">
                <div class="pod_img">
                    <div class="pod_image" style="background-image: url('../../assets/images/product/<?=$val['image']?>')"></div>
                    <a class="pod_link" href="<?=$baseurl?>/product/<?=$val['type_id']?>/<?=$val['url']?>/<?=$val['id']?>"></a>
                    <div class="pod_korz">
                        <div class="pod_dirq">
                            <div class="pod_knok_izb pod_knoks">
                                <p class="__addlike" data-like-id="<?=$val['id']?>">избранное</p>
                            </div>
                            <div class="pod_knok_korz pod_knoks">
                                <a  href="<?=$baseurl?>/product/<?=$val['type_id']?>/<?=$val['id']?>">
                                    <p>в корзину</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pod_img_tMain">
                    <p class="pod_img_text"><?=$val['name']?></p>
                    <p class="pod_img_text"><span><?=$val['prices'][0]['dlina']?> см. /
                            <?=
                            is_null($val['prices'][0]['sale_price'])
                            || $val['prices'][0]['sale_price'] == ''
                            || $val['prices'][0]['sale_price'] == 0
                                ? $items['prices'][0]['price']
                                : '<del>'.$val['prices'][0]['price'].' руб.</del>'.$val['prices'][0]['sale_price']
                            ?>
                        </span> руб.</p>
                </div>
            </div>
            <?php  } ?>
            <div class="col-lg-12">
                <a href="<?=$baseurl?>/catalog/2"><p class="pod_more">показать больше</p></a>
            </div>
        </div>


<!--        ПОДБЕРЕМ ДЛЯ ВАС СОЛНЦЕЗАШИТНУЮ СИСТЕМУ   --- ВЕРСТКА ---->

<!--        <div class="how_we_work_g">ПОДБЕРЕМ ДЛЯ ВАС <span class="pod_tk">СОЛНЦЕЗАШИТНУЮ СИСТЕМУ</span></div>-->
<!--        <div class="swiper-container portpholio-slider pcnone">-->
<!--            <div class="swiper-wrapper">-->
<!--                <div class="swiper-slide  slider-items slider_img podbermimg  ">-->
<!--                    <div class="pod_img">-->
<!--                        <img src="--><?//=$baseurl?><!--/assets/images/content/ktor3.jpg" alt="ktor1">7-->
<!--                        <div class="pod_dirqs">-->
<!--                            <div class="pod_knok_izb pod_knoks">-->
<!--                                <p>избранное</p>-->
<!--                            </div>-->
<!--                            <div class="pod_knok_korz pod_knoks">-->
<!--                                <p>в корзину</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="pod_img_tMain">-->
<!--                        <p class="pod_img_text">Ткань Хлопковая принтованная с рисунком Cozyland Zigzag Italian Brown</p>-->
<!--                        <p class="pod_img_text"><span>2300</span> руб.</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="swiper-slide  slider-items slider_img podbermimg  ">-->
<!--                    <div class="pod_img">-->
<!--                        <img src="--><?//=$baseurl?><!--/assets/images/content/ktor2.jpg" alt="ktor1">7-->
<!--                        <div class="pod_dirqs">-->
<!--                            <div class="pod_knok_izb pod_knoks">-->
<!--                                <p>избранное</p>-->
<!--                            </div>-->
<!--                            <div class="pod_knok_korz pod_knoks">-->
<!--                                <p>в корзину</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="pod_img_tMain">-->
<!--                        <p class="pod_img_text">Ткань Хлопковая принтованная с рисунком Cozyland Zigzag Italian Brown</p>-->
<!--                        <p class="pod_img_text"><span>2300</span> руб.</p>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!---->
<!---->
<!--            </div>-->
<!--            <div class="col-lg-12 col-xs-12 how_we_more">-->
<!--                <span>показать больше</span>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-lg-12 nopadding clear mobnone">-->
<!--            <div class="col-lg-3 how_we_z">-->
<!--                <div class="pod_img">-->
<!--                    <img src="--><?//=$baseurl?><!--/assets/images/content/ktor1.jpg" alt="ktor1">7-->
<!--                    <div class="pod_korz">-->
<!--                        <div class="pod_dirq">-->
<!--                            <div class="pod_knok_izb pod_knoks">-->
<!--                                <p>избранное</p>-->
<!--                            </div>-->
<!--                            <div class="pod_knok_korz pod_knoks">-->
<!--                                <p>в корзину</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="pod_img_tMain">-->
<!--                    <p class="pod_img_text">Ткань Хлопковая принтованная с рисунком Cozyland Zigzag Italian Brown</p>-->
<!--                    <p class="pod_img_text"><span>2300</span> руб.</p>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-lg-12">-->
<!--                <p class="pod_more">показать больше</p>-->
<!--            </div>-->
<!--        </div>-->

        <!--     /   ПОДБЕРЕМ ДЛЯ ВАС СОЛНЦЕЗАШИТНУЮ СИСТЕМУ   --- ВЕРСТКА ---->


        <div class="col-lg-12 nopadding clear">
            <div class="how_we_work_g">ОТЗЫВЫ</div>
            <div class="col-lg-1">

            </div>
            <div class="col-lg-10 nopadding center">
                <?php foreach ($params['feedback'] as $feedback){  ?>
                    <div class="otz_main">
                        <div class="otz_img_main">
                            <img src="<?=$baseurl?>/assets/images/content/feedback/<?=$feedback['image']?>" alt="img1"/>
                        </div>
                        <div class="otz_textM">
                            <p class="otz_t_title"><?=$feedback['name']?></p>
                            <p  class="otz_text">
                                <?=$feedback['description']?>
                            </p>
                        </div>
                    </div>
                <?php  } ?>
            </div>
            <div class="col-lg-1">

            </div>
        </div>
    </div>
</div>
<div class="success_like" style="top: -10000px;">
    Продукт добавлен
</div>
<div class="success_like_error" style="top: -10000px;">
    Продукт уже добавлен
</div>
<script>
    var ids = <?=$params['portfolio_id']?>;
    $(document).ready(function () {
        for(var i=0;i<ids.length;i++) {
            $("a[rel=example_group_"+ids[i]+"]").fancybox();
        }
    })
        $('.center').slick({
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 3,
            prevArrow: '<div class="prew_sl">\n' +
            '            <i class="fa fa-chevron-left " aria-hidden="true"></i>\n' +
            '        </div>',
            nextArrow: '<div class="next_sl">\n' +
            '            <i class="fa fa-chevron-right" aria-hidden="true"></i>\n' +
            '        </div>',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                }
            ]
        });

    var izbrni = new Swiper('.portpholio-slider', {
        loop: true,
        slidesPerView: 2,
        navigation: {
            nextEl: '.swiper-button-nexts-work',
            prevEl: '.swiper-button-prevs-work',
        },
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        breakpoints:{
            480:{
                slidesPerView: 1.4,
                spaceBetween: 5,
            },

        }
    });

    // $('.center').slick();
    $('.port_slide_open').click(function () {
        var blId = $(this).attr('data-ids');
        $('.port_to_op_'+blId).css({
            display:'block'
        })
    })

        $('.send_sButton').click(function () {
            $('.send_loading').css({'display':'block'})
            var name = $('.s_req_name').val();
            var phone = $('.s_req_phone').val();
            var email = $('.s_req_email').val();
            console.log('phone',phone)
            if(ValidateEmail(email)){
                if(ValidatePhoneNumber(phone)){
                    if(name.length > 0){
                        var body = 'name='+name+'&phone='+phone+'&email='+email+'&type=1'
                        var url = base+'/msg/invite/';
                        requestPost(url, body, function () {
                            if(this.readyState == 4){
                                var result = JSON.parse(this.responseText);
                                if(result.error){
                                    $('.send_loading').css({'display':'none'});
                                    $('.success_error').css({
                                        'top':'50px'
                                    });
                                    setTimeout(function () {
                                        $('.success_error').css({
                                            'top':'-10000px'
                                        });
                                    },2000);
                                }else{
                                    $('.send_loading').css({'display':'none'});
                                    $('.success_send').css({
                                        'top':'50px'
                                    });
                                    setTimeout(function () {
                                        $('.success_send').css({
                                            'top':'-10000px'
                                        });
                                    },2000);
                                }
                            }
                        })
                    }else{
                        var errorText = 'Заполните все поля';
                        closeError(errorText);
                    }
                }else{
                    var errorText = 'Неправильный телефонный номер, введите без знака "+"';
                    closeError(errorText);
                }
            }else{
                var errorText = 'Неправильный Емейл Адрес';
                closeError(errorText);
            }

        })

    $('.__addlike').click(function () {
        $('.success_like_error').css({'top':'-10000px'});
        $('.success_like').css({'top':'-10000px'});
        var id = $(this).attr('data-like-id');
        var body = 'pid='+id;
        var url = base+'/like/add/';
        requestPost(url, body, function () {
            if(this.readyState == 4){
                var result = JSON.parse(this.responseText);
                if(!result.error){
                    $('.success_like').css({'top':'50px'});
                    setTimeout(function () {
                        $('.success_like').css({
                            'top':'-10000px'
                        });
                    },2000);
                    var countss = parseInt($('.count_li').text());
                    countss ++;
                    $('.count_li').text(countss);
                }else{
                    $('.success_like_error').css({'top':'50px'});
                    setTimeout(function () {
                        $('.success_like_error').css({
                            'top':'-10000px'
                        });
                    },2000);
                }
            }
        })
    })
</script>





