<div class="col-lg-12 nopadding clear">
    <div class="podb_v">О бренде</div>
    <div class="content clear">
        <div class="col-lg-12 nopadding clear">
            <div class="col-lg-5 ab_logo_main">
                <div class="col-lg-12">
                    <div class="ab_logo">
                        <img src="<?=$baseurl?>/assets/images/content/logo_design_house.png"  width="100%"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="col-lg-12">
                    <p class="ab_title"><?=$params['result']['title_one']?></p>
                    <div class="ab_te_ma">
                        <?= $params['result']['text_one'] ?>
                    </div>
                    <p class="ab_title"><?=$params['result']['title_two']?></p>
                    <div class="ab_te_ma">
                        <?= $params['result']['text_two'] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12 nopadding clear">
    <div class="podb_v">Мы в цифрах</div>
    <div class="col-lg-12 nopadding clear ab_back">
        <div class="cover_white">
        </div>
        <img class="cover_white_img" src="<?=$baseurl?>/assets/images/content/portfolio_back.jpeg" alt="About">

        <div class="ab_back_main">
            <div class="content clear">
                <div class="clear ab_back_main">
                    <?php foreach ($params['cifr'] as $vals){ ?>
                        <div class="col-lg-4 col-sm-6 clear">
                            <div class="col-lg-12 clear ab_box">
                                <div class="col-lg-9 nopadding center">
                                    <div class="ab_int">
                                        <?=$vals['ints']?>
                                    </div>
                                    <div class="ab_row"></div>
                                    <div class="ab_row_text">
                                        <?=$vals['text']?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php  } ?>
                </div>
            </div>
        </div>
    </div>
</div>