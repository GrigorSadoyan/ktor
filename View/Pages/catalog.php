<?php
$urls = trim($_SERVER['REQUEST_URI'], '/\\');
$url= explode("/", $urls);
$CatName =  urldecode($url[1]);

?>
<div class="col-lg-12 nopadding">
    <div class="cover coverportfolio">
        <img src="<?=$baseurl?>/assets/images/content/<?=$params['cover_info']['image']?>">
        <div class="cover_white"></div>
        <div class=" text_block">
            <p class="cover_title"><?=$params['cover_info']['text_1']?></p>
            <p class="cover_text"><?=$params['cover_info']['text_2']?></p>
            <p class="cover_text"><span><?=$params['cover_info']['text_3']?></span></p>
            <?php  if($CatName != 'солнцезащитная-система'){ ?>
            <p class=""><span class="btn_cover" data-toggle="modal" data-target="#myModal">Получить Дизайн-проект</span></p>
            <?php } ?>
        </div>
    </div>
</div>
<?php if($CatName != 'солнцезащитная-система'){ ?>
<div class="col-lg-12 nopadding clear">
    <div class="load_gifs">
        <img src="<?=$baseurl?>/assets/images/content/load.gif" />
    </div>
    <div class="content">
        <div class="ca_main clear">
            <div class="ca_left">
                <div class="ca_title">
                    Фильтры
                </div>
                <div class="ca_filters">
                    <?php for($i = 0; $i<count($params['filtrs_result']); $i++){ ?>
                        <div class="ca_filtr_item">
                            <p class="filtr_item_main clear">
                                <span class="filtr_item_name"><?=$params['filtrs_result'][$i]['name']?></span>
                                <span class="filtr_item_icon filtr_item_icon_close "><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                <span class="filtr_item_icon filtr_item_icon_open f_ic_none"><i class="fa fa-chevron-up" aria-hidden="true"></i></span>
                            </p>

                            <div class="filtr_vals">
                                <?php foreach ($params['filtrs_result'][$i]['filtrs'] as $items){ ?>
                                    <?php if($params['filtrs_result'][$i]['view_type'] == '0'){ ?>
                                            <p class="filtr_val_main">
                                                <span class="filter_val_check">
                                                    <input  onclick="handleClick(this)" class="filter_change_but" data-chid="<?=$items['filtrs_origin']['id']?>" id="sss_<?=$items['filtrs_origin']['id']?>" type="checkbox"/>
                                                </span>
                                                <label for="sss_<?=$items['filtrs_origin']['id']?>">
                                                    <span class="filter_val_name">
                                                        <?= $items['filtrs_origin']['name'] ?>
                                                    </span>
                                                </label>
                                            </p>
                                        <?php  }elseif($params['filtrs_result'][$i]['view_type'] == '1'){ ?>
                                            <div class="filtr_vals_one">
                                                <label for="cvet_<?=$items['filtrs_origin']['id']?>">
                                                    <span style="background: #<?=$items['filtrs_origin']['name'] ?>" class="fil_val_col_bl"></span>
                                                </label>
                                                <input type="checkbox" onclick="handleClick(this)" class="filter_vals_colors_checks filter_change_but" data-chid="<?=$items['filtrs_origin']['id']?>" id="cvet_<?=$items['filtrs_origin']['id']?>" value="cvet_1">
                                            </div>
                                        <?php  } ?>
                                <?php  } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="ca_right ">
                <div class=" col-lg-12 ca_search clear nopadding">
                    <div class="col-lg-8">
                        <div class="ca_search_icon">
                            <i class="fa fa-search"></i>
                        </div>
                        <input type="text" placeholder="ПОИСК В РАЗДЕЛЕ" />
                    </div>
                    <div class="col-lg-4 col-xs-6 filtr">
                        <p class="sort pcnone">Фильтры</p>
                        <span class="sort_close mobnone"><i class="fa fa-angle-down"></i></span>
                    </div>
                    <div class="col-lg-4 col-xs-6 filtr">
                        <p class="sort __sort_chechText">сортировать</p>
                        <span class="sort_close mobnone __sort_op"><i class="fa fa-angle-down"></i></span>
                        <div class="sort_wind  __sort_winds sort_wind_none">
                            <div class="sort_wind_items" data-sort="minmax">
                                <span>Цена По Возрастанию</span>
                            </div>
                            <div class="sort_wind_items" data-sort="maxmin">
                                <span>Цена По Уменьшению</span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
//                    echo '<pre>';
//                    var_dump($params['products']);die;

                ?>
                <div class="col-lg-12 nopadding clear mobnone prods_main">
                    <?php foreach($params['products'] as $val){  ?>
                        <div class="col-lg-4  ca_mains">
                            <div class="pod_img">
                                <div class="pod_image" style="background-image: url('../../../assets/images/product/<?=$val['image']?>')"></div>
                                <a class="pod_link" href="<?=$baseurl?>/product/<?=$val['type_id']?>/<?=$val['url']?>/<?=$val['id']?>"></a>
                                <div class="pod_korz">
                                    <div class="pod_dirq">
<!--                                        <div class="pod_knok_izb pod_knoks">-->
<!--                                            <a href="--><?//=$baseurl?><!--/product/--><?//=$val['type_id']?><!--/--><?//=$val['id']?><!--">-->
<!--                                                <p data-like-id="--><?//=$val['id']?><!--">избранное</p>-->
<!--                                            </a>-->
<!--                                        </div>-->
                                        <div class="pod_knok_likeIn pod_knoks">
                                            <a href="<?=$baseurl?>/product/<?=$val['type_id']?>/<?=$val['url']?>/<?=$val['id']?>">
                                                <p>в корзину</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pod_img_tMain">
                                <p class="pod_img_text"><?=$val['name']?></p>
<!--                                <p class="pod_img_text"><span>--><?//=is_null($val['price'][0]['sale_price']) || $val['price'][0]['sale_price'] == ''|| $val['price'][0]['sale_price'] == '0' ? $val['price'][0]['price'] : $val['price'][0]['sale_price']?><!--</span> руб.</p>-->


                                <p class="pod_img_text">
                                    <span>
                                        <?=
                                        is_null($val['price'][0]['sale_price'])
                                        || $val['price'][0]['sale_price'] == ''
                                        || $val['price'][0]['sale_price'] == 0
                                            ? $val['price'][0]['price'].' руб.'
                                            : '<del>'.$val['price'][0]['price'].' руб.</del>'.$val['price'][0]['sale_price'].' руб.'
                                        ?>
                                    </span>
                                </p>
                            </div>
                        </div>
                    <?php  } ?>

<!--                    <div class="col-lg-4  ca_mains">-->
<!--                        <div class="pod_img col-xs-5">-->
<!--                            <img src="--><?//=$baseurl?><!--/assets/images/content/ktor4.jpg" alt="ktor1">-->
<!--                            <div class="pod_korz">-->
<!--                                <div class="pod_dirq">-->
<!--                                    <div class="pod_knok_izb pod_knoks">-->
<!--                                        <p>избранное</p>-->
<!--                                    </div>-->
<!--                                    <div class="pod_knok_korz pod_knoks">-->
<!--                                        <p>в корзину</p>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="pod_img_tMain">-->
<!--                            <p class="pod_img_text">Ткань Хлопковая принтованная с рисунком Cozyland Zigzag Italian Brown</p>-->
<!--                            <p class="pod_img_text"><span>2300</span> руб.</p>-->
<!--                        </div>-->
<!--                    </div>-->

                    <div class="col-lg-12">
                        <p class="pod_more">показать больше</p>
                    </div>
                </div>
<!--                **************mobail*****************-->
                <div class="col-xs-12  minBlock_pro pcnone">
                    <div class="col-xs-5 mobImg ">
                        <img src="<?=$baseurl?>/assets/images/content/ktor4.jpg" alt="ktor1">
                    </div>
                    <div class="col-xs-7  contentinfo">
                        <div class="pod_img_tMain">
                            <p class="pod_img_text">Ткань Хлопковая принтованная с рисунком Cozyland Zigzag Italian Brown</p>
                            <p class="pod_img_text"><span>2300</span> руб.</p>
                        </div>
                        <div class="pod_dirq">
                            <div class="pod_knok_izb pod_knoks">
                                <p>избранное</p>
                            </div>
                            <div class="pod_knok_korz pod_knoks">
                                <p>в корзину</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12  minBlock_pro pcnone">
                    <div class="col-xs-5 mobImg ">
                        <img src="<?=$baseurl?>/assets/images/content/ktor4.jpg" alt="ktor1">
                    </div>
                    <div class="col-xs-7  contentinfo">
                        <div class="pod_img_tMain">
                            <p class="pod_img_text">Ткань Хлопковая принтованная с рисунком Cozyland Zigzag Italian Brown</p>
                            <p class="pod_img_text"><span>2300</span> руб.</p>
                        </div>
                        <div class="pod_dirq">
                            <div class="pod_knok_izb pod_knoks">
                                <p>избранное</p>
                            </div>
                            <div class="pod_knok_korz pod_knoks">
                                <p>в корзину</p>
                            </div>
                        </div>
                    </div>
                </div>
<!--                ****************/mobile***************-->
            </div>
        </div>
    </div>
</div>
<?php  }else{ ?>
    <div class="in_construction col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <i class="fa fa-cog fa-2x fa-spin fa-fw"></i>
        <span>В разработке</span>
    </div>
<?php  } ?>
<script>
    $(document).ready(function () {
        var heightWin = ($(window).height());
        $('.load_gifs').height(heightWin);
    })

    $('.filtr_item_main').click(function () {
        var arrows = $(this).children('.filtr_item_icon');
        for(var i = 0; i<arrows.length;i++){
            if($(arrows[i]).hasClass('f_ic_none')){
                $(arrows[i]).removeClass('f_ic_none');
            }else{
                $(arrows[i]).addClass('f_ic_none');
            }
        }
        $(this).next('.filtr_vals').slideToggle(200);
    })
    $('.fil_val_col_bl').click(function () {
        var col_bls =  $('.fil_val_col_bl');
        if($(this).hasClass('fil_val_col_bl_sels')){
            $(this).removeClass('fil_val_col_bl_sels');
            $(this).parent('label').next('.filter_vals_colors_checks').cheked = false;
        }else{
            $(this).addClass('fil_val_col_bl_sels');
        }
        $(this).parent('label').next('.filter_vals_colors_checks').cheked = true;
    })
    $('.__sort_op').click(function () {
        var openSub = $(this).next('.__sort_winds');
        if($(openSub).hasClass('sort_wind_none')) {
            $(openSub).removeClass('sort_wind_none');
        }else{
            $(openSub).addClass('sort_wind_none');
        }
    })
    $('.sort_wind_items').click(function () {
        var typeSortText  = $(this).children('span').text();
        var typeSort  = $(this).attr('data-sort');
        $(this).parent('.__sort_winds').prev('.sort_close ').prev('.__sort_chechText').text(typeSortText);
        $(this).parent('.__sort_winds').addClass('sort_wind_none');
    })
    function handleClick(e) {
        $('.load_gifs').css({
            'display':'block'
        });
        var allBox = document.getElementsByClassName('filter_change_but');
        var postBoxes = [];
        for(var i= 0; i<allBox.length; i++ ){
            if(allBox[i].checked){
                var ids = allBox[i].getAttribute('data-chid');
                postBoxes.push(ids);
            }
        }
        var types = <?php echo json_encode( $this->CatsArray) ?>;
        var typeName = '<?=$CatName?>';
        for(var j = 0; j <types.length; j++) {
            if(types[j].url === typeName){
                var typeId = types[j].cat_id;
                break
            }
        }
        if(postBoxes.length > 0) {
            var self = $(this);
            var url = base + "/catalog/filtr/change/";
            var body = "id=" + postBoxes + "&type_id=" + typeId + "";
            requestPost(url, body, function () {
                if (this.readyState == 4) {
                    var result = JSON.parse(this.responseText);
                    $('.prods_main').html('');
                    var perepend = '';
                    for (var i = 0; i < result.length; i++) {
                        var ccc = '';
                        if (result[i]['price'][0]['sale_price'] === '0') {
                            ccc = result[i]['price'][0]['price']
                        } else {
                            ccc = '<del>' + result[i]['price'][0]['price'] + 'РУБ.' + '</del>' + result[i]['price'][0]['sale_price'] + 'РУБ.';
                        }
                        perepend +=
                            '<div class="col-lg-4  ca_mains">'
                            + '<a href="<?=$baseurl?>/product/' + typeId + '/' + result[i]['url'] + '/'+ result[i]['id']+'">'
                            + '<div class="pod_img ">'
                            + '<img src="<?=$baseurl?>/assets/images/product/' + result[i]['image'] + '" alt="ktor1" />'
                            + '<div class="pod_korz">'
                            + '<div class="pod_dirq">'
                            + '<div class="pod_knok_izb pod_knoks">'
                            + '<p>избранное</p>'
                            + '</div>'
                            + '<div class="pod_knok_korz pod_knoks">'
                            + '<p>в корзину</p>'
                            + '</div>'
                            + '</div>'
                            + '</div>'
                            + '</div>'
                            + '</a>'
                            + '<div class="pod_img_tMain">'
                            + '<p class="pod_img_text">' + result[i]['name'] + '</p>'
                            + '<p class="pod_img_text"><span>' + ccc + '</span></p>'
                            + '</div>'
                            + '</div>'

                    }
                    $('.prods_main').append(perepend);
                    $('.load_gifs').css({
                        'display': 'none'
                    })
                }
            })
        }else{
            window.location.reload()
        }
    }

</script>