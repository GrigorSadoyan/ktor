<link rel="stylesheet" href="<?= $baseurl ?>/assets/fancybox/source/jquery.fancybox.css?v=2.1.5">
<link rel="stylesheet" href="<?= $baseurl ?>/assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5">
<link rel="stylesheet" href="<?= $baseurl ?>/assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7">

<script src="<?= $baseurl ?>/assets/fancybox/lib/jquery.mousewheel.pack.js?v=3.1.3"></script>
<script src="<?= $baseurl ?>/assets/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<script src="<?= $baseurl ?>/assets/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script src="<?= $baseurl ?>/assets/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script src="<?= $baseurl ?>/assets/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<div class="col-lg-12 nopadding">
    <div class="cover coverportfolio">
        <img src="<?=$baseurl?>/assets/images/content/<?=$params['cover_info']['image']?>">
        <div class="cover_white"></div>
        <div class=" text_block">
            <p class="cover_title"><?=$params['cover_info']['text_1']?></p>
            <p class="cover_text"><?=$params['cover_info']['text_2']?></p>
            <p class="cover_text"><span><?=$params['cover_info']['text_3']?></span></p>
            <p class=""><span class="btn_cover" data-toggle="modal" data-target="#myModal">Получить Дизайн-проект</span></p>
        </div>
    </div>
</div>
<div class="col-lg-12 nopadding clear">
    <div class="content">
        <div class="col-lg-12 clear nopadding">
            <?php foreach ($params['portfolio'] as $valp) { ?>
                <div class="col-lg-4 col-sm-6 port_item port_slide_open" data-ids="1">
                    <div class="port_img">
                        <a rel="example_group_<?=$valp['id']?>" title="<?=$valp['gallery'][0]['title']?>" href="<?=$baseurl?>/assets/images/content/portfolio/<?=$valp['gallery'][0]['image']?>">
                            <img alt="<?=$valp['name']?>" src="<?=$baseurl?>/assets/images/content/portfolio/<?=$valp['gallery'][0]['image']?>">
                        </a>
                        <?php for($i=1;$i<count($valp['gallery']);$i++) { ?>
                            <a rel="example_group_<?=$valp['id']?>" title="<?=$valp['gallery'][$i]['title']?>" href="<?=$baseurl?>/assets/images/content/portfolio/<?=$valp['gallery'][$i]['image']?>" style="display:none;"></a>
                        <?php } ?>
                        <!--                        <div class="type_img_wh"></div>-->
                    </div>
                    <div class="port_name">
                        <p><?=$valp['name']?> </p>
                    </div>
                </div>
            <?php } ?>

        </div>
        <div class="col-lg-12">
            <p class="pod_more">показать больше</p>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.center').slick({
        });
        $('.port_slide_open').click(function () {
            var blId = $(this).attr('data-ids');
            $('.port_to_op_'+blId).css({
                display:'block'
            })
        })
        var ids = <?=$params['portfolio_id']?>;
        $(document).ready(function () {
            for(var i=0;i<ids.length;i++) {
                $("a[rel=example_group_"+ids[i]+"]").fancybox();
            }
        })
    });
</script>
