<div class="col-lg-12 nopadding clear">
    <div class="podb_v">Контакты</div>
    <div class="content clear">
        <div class="col-lg-12 nopadding clear">
            <div class="col-lg-6">
                <div class="cont_main">
                    <p class="cont_title">
                        Адрес шоу-рума:​
                    </p>
                    <p class="cont_text"><?=$params['result']['address']?>​</p>
                    <p class="cont_text">Телефон: <?=$params['result']['phone']?></p>
                    <p class="cont_text">e-mail: <?=$params['result']['email']?></p>
                    <p class="cont_text">сайт: <?=$params['result']['site']?></p>
                    <p class="cont_title">
                        Режим работы:​
                    </p>
                    <p class="cont_text"><?=$params['result']['time_one']?>​</p>
                    <p class="cont_text"><?=$params['result']['time_two']?>​</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="map_main">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!--<script>-->
    <!--    function initMap() {-->
    <!--        var icon = {-->
    <!--            icon:base + "/assets/images/content/icons/marker.png"-->
    <!--        };-->
    <!--        var uluru = {lat:55.7074751, lng:37.6856698};-->
    <!--        var map = new google.maps.Map(document.getElementById('map'), {-->
    <!--            zoom: 15,-->
    <!--            center: uluru-->
    <!--        });-->
    <!--        var marker = new google.maps.Marker({-->
    <!--            position: uluru,-->
    <!--            map: map,-->
    <!--            icon: icon.icon,-->
    <!--        });-->
    <!--    }-->
    <!--</script>-->
    <!--<script async defer-->
    <!--        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwm4OGBDFjTuh26cU-ptsEpuLNsbeqP1E&callback=initMap">-->
    <!--</script>-->
<script src="https://api-maps.yandex.ru/2.1/?apikey=d809a125-3426-4131-ab5d-a300da991472&lang=ru_RU" type="text/javascript">
</script>
<script type="text/javascript">
    ymaps.ready(init);
    function init(){
        var myMap = new ymaps.Map("map", {
            center: [55.701526, 37.422180],
            zoom: 15
        });
        var myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            balloonContentBody: [
                '<address>',
                '<strong>Офис в Москве</strong>',
                '<br/>',
                'Адрес: 121471, г Москва, улица Рябиновая, дом 41 КОРПУС 1 СТР 6',
                '<br/>',
                'Подробнее: <a href="http://domdizaina.com">domdizaina.com</a>',
                '</address>'
            ].join('')
        }, {
            preset: 'islands#redDotIcon'
        });
        myMap.geoObjects.add(myPlacemark);
    }
</script>