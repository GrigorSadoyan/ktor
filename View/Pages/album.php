<div class="col-lg-12 clear WrapperAlbom nopadding mobnone">
    <div class="content">
        <div class="col-lg-12 nopadding clear">
            <div class="col-lg-8 center clear change_row">
                <div class="col-lg-4 nopadding ch_main">
                    <p class="ch_news">
                        <span class="" data-tab="1">
                            <img src="<?=$baseurl?>/assets/images/content/icons/telegram.png" />
                            Новости компании
                        </span>
                    </p>
                </div>
                <div class="col-lg-4 nopadding ch_main">
                    <p class="ch_news ">
                        <span class="active_ch_tab" data-tab="2">
                             <img src="<?=$baseurl?>/assets/images/content/icons/book.png" />
                             Полезные статьи
                        </span>
                    </p>
                </div>
                <div class="col-lg-4 nopadding ch_main">
                    <p class="ch_news">
                        <span class="" data-tab="3">
                            <img src="<?=$baseurl?>/assets/images/content/icons/iii.png" />
                            Все о текстиле
                        </span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-lg-12 clear nopadding port_M pol_news">
            <?php foreach ($params['result'][0] as $news){ ?>
                <div class="col-lg-4 port_item">
                    <a href="<?=$baseurl?>/typecurtain/typec/">
                        <div class="port_img">
                            <img src="<?=$baseurl?>/assets/images/content/album/<?=$news['image']?>" />
                            <div class="type_img_wh"></div>
                        </div>
                        <div class="port_name">
                            <p><?=$news['title']?> </p>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>

        <div class="col-lg-12 clear nopadding port_M pol_stats active_ch">
            <?php foreach ($params['result'][1] as $stat){ ?>
            <div class="col-lg-4 port_item">
                <a href="<?=$baseurl?>/album/<?=$stat['id']?>/">
                    <div class="port_img">
                        <img src="<?=$baseurl?>/assets/images/content/album/<?=$stat['image']?>" />
                        <div class="type_img_wh"></div>
                    </div>
                    <div class="port_name">
                        <p><?=$stat['title']?> </p>
                    </div>
                </a>
            </div>
            <?php  } ?>
        </div>
        <div class="col-lg-12 clear nopadding port_M pol_all">
            <?php foreach ($params['result'][2] as $all){ ?>
                <div class="col-lg-4 port_item">
                    <a href="<?=$baseurl?>/album/<?=$all['id']?>/">
                        <div class="port_img">
                            <img src="<?=$baseurl?>/assets/images/content/album/<?=$all['image']?>" />
                            <div class="type_img_wh"></div>
                        </div>
                        <div class="port_name">
                            <p><?=$all['title']?> </p>
                        </div>
                    </a>
                </div>
            <?php  } ?>
        </div>
    </div>
</div>

<!--***********************mobile*****************************-->
<div class="col-xs-12 pcnone WrapperAlbom">
    <p class="ch_news">
        <img src="<?=$baseurl?>/assets/images/content/icons/telegram.png" />Новости компании
    </p>
    <div class="swiper-container albom1-slider">
        <div class="swiper-wrapper">
            <div class="swiper-slide  slider-items slider_img ">
                <div class="col-lg-3 type_items">
                    <div class="type_img">
                        <img src="<?=$baseurl?>/assets/images/content/port1.jpg" alt="ktor1">
                    </div>
                    <div class="albob_conten">
                        <p class="title_al">Виды текстиля</p>
                        <p class="text_al"> Компания HOMM Textile расскажет Вам про все этапы производства и на что стоит обратить внимание при пошиве штор</p>
                        
                    </div>
                </div>
            </div>
            <div class="swiper-slide  slider-items slider_img ">
                <div class="col-lg-3 type_items">
                    <div class="type_img">
                        <img src="<?=$baseurl?>/assets/images/content/port2.jpg" alt="ktor1">
                    </div>
                    <p class="title_al">Виды текстиля</p>
                    <p class="text_al"> Компания HOMM Textile расскажет Вам про все этапы производства и на что стоит обратить внимание при пошиве штор</p>

                </div>
            </div>
        </div>

    </div>
</div>
<div class="col-xs-12 pcnone">
    <p class="ch_news">
        <img src="<?=$baseurl?>/assets/images/content/icons/book.png" />
        Полезные статьи
    </p>
    <div class="swiper-container albom2-slider">
        <div class="swiper-wrapper">
            <div class="swiper-slide  slider-items slider_img ">
                <div class="col-lg-3 type_items">
                    <div class="type_img">
                        <img src="<?=$baseurl?>/assets/images/content/port1.jpg" alt="ktor1">
                    </div>
                    <div class="albob_conten">
                        <p class="title_al">Виды текстиля</p>
                        <p class="text_al"> Компания HOMM Textile расскажет Вам про все этапы производства и на что стоит обратить внимание при пошиве штор</p>

                    </div>
                </div>
            </div>
            <div class="swiper-slide  slider-items slider_img ">
                <div class="col-lg-3 type_items">
                    <div class="type_img">
                        <img src="<?=$baseurl?>/assets/images/content/port2.jpg" alt="ktor1">
                    </div>
                    <p class="title_al">Виды текстиля</p>
                    <p class="text_al"> Компания HOMM Textile расскажет Вам про все этапы производства и на что стоит обратить внимание при пошиве штор</p>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12 pcnone">
    <p class="ch_news">
        <img src="<?=$baseurl?>/assets/images/content/icons/iii.png" />
        Все о текстиле
    </p>
    <div class="swiper-container albom3-slider">
        <div class="swiper-wrapper">
            <div class="swiper-slide  slider-items slider_img ">
                <div class="col-lg-3 type_items">
                    <div class="type_img">
                        <img src="<?=$baseurl?>/assets/images/content/port1.jpg" alt="ktor1">
                    </div>
                    <div class="albob_conten">
                        <p class="title_al">Виды текстиля</p>
                        <p class="text_al"> Компания HOMM Textile расскажет Вам про все этапы производства и на что стоит обратить внимание при пошиве штор</p>

                    </div>
                </div>
            </div>
            <div class="swiper-slide  slider-items slider_img ">
                <div class="col-lg-3 type_items">
                    <div class="type_img">
                        <img src="<?=$baseurl?>/assets/images/content/port2.jpg" alt="ktor1">
                    </div>
                    <p class="title_al">Виды текстиля</p>
                    <p class="text_al"> Компания HOMM Textile расскажет Вам про все этапы производства и на что стоит обратить внимание при пошиве штор</p>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="clear"></div>
<!--***********************mobile*****************************-->
<script>
    $('.ch_news span').click(function () {
        var ttt = $(this).attr('data-tab');
        $('.port_M').removeClass('active_ch');
        $('.ch_news span').removeClass('active_ch_tab');
        $(this).addClass('active_ch_tab');
        if(ttt == '1'){
            $('.pol_news').addClass('active_ch');
        }
        if(ttt == '2'){
            $('.pol_stats').addClass('active_ch');
        }
        if(ttt == '3'){
            $('.pol_all').addClass('active_ch');
        }
    })
    var albom1 = new Swiper('.albom1-slider', {
        loop: true,
        slidesPerView: 1.4,
        navigation: {
            nextEl: '.swiper-button-nexts-work',
            prevEl: '.swiper-button-prevs-work',
        },
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        breakpoints:{
            480:{ slidesPerView: 1.4},
            768:{ slidesPerView: 2},
            1024:{slidesPerView: 3}
        }
    });
    var albom2 = new Swiper('.albom2-slider', {
        loop: true,
        slidesPerView: 1.4,
        navigation: {
            nextEl: '.swiper-button-nexts-work',
            prevEl: '.swiper-button-prevs-work',
        },
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        breakpoints:{
            480:{ slidesPerView: 1.4},
            768:{ slidesPerView: 2},
            1024:{slidesPerView: 3}
        }
    });
    var albom3 = new Swiper('.albom3-slider', {
        loop: true,
        slidesPerView: 1.4,
        navigation: {
            nextEl: '.swiper-button-nexts-work',
            prevEl: '.swiper-button-prevs-work',
        },
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        breakpoints:{
            480:{ slidesPerView: 1.4},
            768:{ slidesPerView: 2},
            1024:{slidesPerView: 3}
        }
    });
</script>