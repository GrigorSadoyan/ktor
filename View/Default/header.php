<?php
$urls = trim($_SERVER['REQUEST_URI'], '/\\');
$url= explode("/", $urls);
if(count($url) > 1){
    $CatName =  urldecode($url[1]);
}else{
    $CatName = '';
}

?>
<!doctype html>
<html lang="en">
	<head>
        <title><?=$seo['title']?></title>
        <meta name="description" content="<?=$seo['desc']?>">
        <meta name="keywords" content="<?=$seo['key']?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="manifest" href="<?= $baseurl ?>/assets/fav/manifest.json">
        <meta name="msapplication-TileImage" content="<?= $baseurl ?>/assets/fav/ms-icon-144x144.png">

		<link rel="stylesheet" href="<?= $baseurl ?>/assets/css/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" href="<?= $baseurl ?>/assets/css/bootstrap/css/bootstrap-theme.css">
		<script src="<?= $baseurl ?>/assets/javascript/jquery2.2.4.min.js"></script>
		<script src="<?= $baseurl ?>/assets/css/bootstrap/js/bootstrap.js"></script>

		<link rel="stylesheet" file="text/css" href="<?= $baseurl ?>/assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= $baseurl ?>/assets/css/swiper.css" media="all" />

		<script type="text/javascript"><?php echo "var base = '".$baseurl."';"; ?></script>

		<link rel="stylesheet" type="text/css" href="<?= $baseurl ?>/assets/css/slick/slick.css">
		<link rel="stylesheet" type="text/css" href="<?= $baseurl ?>/assets/css/slick/slick-theme.css">
        <link rel="stylesheet" href="<?= $baseurl ?>/assets/css/style.css" media="all" />
        <link rel="stylesheet" href="<?= $baseurl ?>/assets/css/style480.css" media="all" />
        <link rel="stylesheet" href="<?= $baseurl ?>/assets/css/style768.css" media="all" />
		<script src="<?= $baseurl ?>/assets/css/slick/slick.min.js"></script>
		<script src="<?= $baseurl ?>/assets/javascript/swiper.js"></script>
		<script src="<?= $baseurl ?>/assets/javascript/script.js"></script>
		<script src="<?= $baseurl ?>/assets/javascript/main.js"></script>
	</head>
	<body>
	<header>
        <div class="headers">
        <div class="content">
            <div class="head_main clear ">
                <div class="col-lg-5 col-xs-3   nopadding">
                    <div class="logo_main">
                        <a href="<?=$baseurl?>/" class="logo_main_head">
                            <img src="<?=$baseurl?>/assets/images/content/logo_design_house.png" />
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 mobnone nopadding">
                    <ul class="head_socials">
                        <a href="https://www.facebook.com" target="_blank">
                            <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                        </a>
                        <a href="https://www.instagram.com" target="_blank">
                            <li><i class="fa fa-instagram" aria-hidden="true"></i></li>
                        </a>
                        <a href="https://www.vk.com" target="_blank">
                            <li><i class="fa fa-vk" aria-hidden="true"></i></li>
                        </a>
                        <a href="https://www.youtube.com" target="_blank">
                            <li><i class="fa fa-youtube-play" aria-hidden="true"></i></li>
                        </a>
                    </ul>
                </div>
                <div class="col-lg-5 col-xs-9   nopadding">
                    <ul class="head_infos">
                        <li>
                            <span class="head_phone"> <a href="tel:79266064278">+7 926-606-42-78</a></span>
                        </li>
                        <li>
                            <span class="head_res_but" data-toggle="modal" data-target="#myModal">Заказать шторы</span>
                        </li>
                        <li >
                            <ul class="groupIcon mobnone">
                                <li class="">
                                    <a href="<?=$baseurl?>/like/">
                                        <i class="fa fa-heart fa-2x" aria-hidden="true"></i>
                                        <span class="count count_li"><?=$this->likeCount?></span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="<?=$baseurl?>/bag/">
                                    <i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>
                                    <span class="count count_kr"><?=$this->bagCount?></span>
                                    </a>
                                </li>
                            </ul>
                            <span class="OpenMenu"><i class="fa fa-bars " aria-hidden="true"></i></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        </div>
            <nav>
                <div class="content">
                <ul class="nav">
                    <ul class="groupIcon pcnone">
                        <li class="">
                            <a href="<?=$baseurl?>/izbrannie/">
                                <i class="fa fa-heart fa-2x" aria-hidden="true"></i>
                                <span class="count">0</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="<?=$baseurl?>/bag/">
                                <i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>
                                <span class="count"><?=$this->bagCount?></span>
                            </a>
                        </li>
                    </ul>
                    <span class="closeMenu pcnone"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
                    <li class="<?=$url[0]==''? 'active_menu' : ''?>"><a href="<?=$baseurl?>/">главная</a></li>
                    <li class="<?=$url[0]=='ordercurtains'? 'active_menu' : ''?>"><a href="<?=$baseurl?>/ordercurtains/"> ЗАКАЗ ШТОР</a></li>
                    <li class="<?=$url[0]=='price'? 'active_menu' : ''?>"><a href="<?=$baseurl?>/price/">ЦЕНЫ</a></li>
                    <li class="<?=$url[0]=='typecurtain'? 'active_menu' : ''?>"><a href="<?=$baseurl?>/typecurtain/">ВИДЫ ШТОР</a></li>
                    <li class="<?=$url[0]=='portfolio'? 'active_menu' : ''?>"><a href="<?=$baseurl?>/portfolio/">ПОРТФОЛИО</a></li>
                    <li class="<?=$CatName=='ткань'? 'active_menu' : ''?>"><a href="<?=$baseurl?>/catalog/<?=$this->CatsArray[0]['url']?>/1">КАТАЛОГ ТКАНЕЙ</a></li>
                    <li class="<?=$CatName=='карниз'? 'active_menu' : ''?>"><a href="<?=$baseurl?>/catalog/<?=$this->CatsArray[1]['url']?>/1">карнизы</a></li>
                    <li class="<?=$CatName=='солнцезащитная-система'? 'active_menu' : ''?>"><a href="<?=$baseurl?>/catalog/<?=$this->CatsArray[2]['url']?>/1">солнцезащитная система</a></li>
                    <li class="<?=$url[0]=='album'? 'active_menu' : ''?>"><a href="<?=$baseurl?>/album/">ЖУРНАЛ</a></li>
                    <li class="<?=$url[0]=='about'? 'active_menu' : ''?>"><a href="<?=$baseurl?>/about/">О НАС</a></li>
                    <li class="<?=$url[0]=='contact'? 'active_menu' : ''?>"><a href="<?=$baseurl?>/contact/">КОНТАКТЫ</a></li>
                    <ul class="head_socials pcnone">
                        <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                        <li><i class="fa fa-instagram" aria-hidden="true"></i></li>
                        <li><i class="fa fa-vk" aria-hidden="true"></i></li>
                        <li><i class="fa fa-youtube-play" aria-hidden="true"></i></li>
                    </ul>
                </ul>
                </div>
            </nav>

    </header>
