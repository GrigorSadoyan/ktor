<footer>

    <div class="content clear">
        <div class="col-lg-12 clear nopadding foot_main">
            <div class="col-lg-4 clear footerBloock">
                <div class="col-lg-12 clear foot_items">

                    <div class="fItems_title">
                        <p class="fItems_title_ps">ПОЗВОНИТЕ НАМ</p>
                        <p class="fItems_title_ss"></p>
                    </div>
                    <div class="col-lg-12 foot_items_main">
                        <p class="foot_items_main_texts">
                            БЕЗ ВЫХОДНЫХ</br>
                            09:00-21:00
                        </p>
                        <p class="foot_items_main_texts">
                            ЕСТЬ ВОПРОСЫ?</br>
                            НЕОБХОДИМА КОНСУЛЬТАЦИЯ?
                        </p>
                        <p class="foot_items_main_texts">
                            <b>МЫ ЖДЕМ!</b></br>
                            7-926-606-42-78
                        </p>
                    </div>
                    <div class="col-lg-12 nopadding">
                        <a href="tel:79266064278">
                            <p class="call_us_fButton">
                                позвонить
                            </p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 clear footerBloock">
                <div class="col-lg-12 clear foot_items">
                    <div class="fItems_title">
                        <p class="fItems_title_ps">ПРИГЛАСИТЕ НАС</p>
                        <p class="fItems_title_ss">
                            ПРИГЛАСИТЕ ДИЗАЙНЕРА С ПОДБОРКОЙ
                            ТКАНЕЙ ПОД ВАШ ИНТЕРЬЕР
                        </p>
                    </div>
                    <div class="col-lg-12 clear foot_items_main">
                        <div class="foot_items_main_texts">
                            <div class="gr_rows">
                                <input type="text" class="b_req_name" placeholder="УКАЖИТЕ ВАШЕ ИМЯ"/>
                                <div class="gr_row_abs">
                                    <i class="fa fa-user"></i>
                                </div>
                            </div>
                        </div>
                        <div class="foot_items_main_texts">
                            <div class=" gr_rows">
                                <input type="text" class="b_req_phone" placeholder="УКАЖИТЕ ВАШ ТЕЛЕФОН"/>
                                <div class="gr_row_abs">
                                    <i class="fa fa-phone"></i>
                                </div>
                            </div>
                        </div>
                        <div class="foot_items_main_texts">
                            <div class="gr_rows">
                                <input type="text" class="b_req_email" placeholder="УКАЖИТЕ ВАШ EMAIL"/>
                                <div class="gr_row_abs">
                                    <i class="fa fa-envelope"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 nopadding">
                        <p class="send_fButton">
                            отправить
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 clear footerBloock ">
                <div class="col-lg-12 foot_items">
                    <div class="fItems_title">
                        <p class="fItems_title_ps">НАПИШИТЕ НАМ</p>
                        <p class="fItems_title_ss"></p>
                    </div>
                    <div class="col-lg-12 foot_items_main">
                        <p class="foot_items_main_textt">
                            ОТПРАВЬТЕ НАМ ВАШИ КРИТЕРИИ,</br>
                            МЫ РАЗРАБОТАЕМ КОНЦЕПЦИЮ ДИЗАЙНА И</br>
                            ПРИВЕЗЕМ ВАМ УНИКАЛЬНУЮ ПОДБОРКУ ТКАНЕЙ
                        </p>
                        <p class="foot_items_main_textt">
                            e-mail: d.dizaina@yandex.ru</br>
                            whatsapp: 7-926-606-42-78
                        </p>
                    </div>
                    <div class="col-lg-12 nopadding">
                        <p class="write_fButton" data-toggle="modal" data-target="#myModal">
                            написать
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="foot_second mobnone">
        <div class="content clear">
            <div class="col-lg-12 foot_second_main clear">
                <div class="col-lg-3 clear">
                    <div class="foot_logo">
                        <img src="<?=$baseurl?>/assets/images/content/logo_design_house.png">
                    </div>
                    <div class="col-lg-7 nopadding socials">
                        <div class="col-lg-4">
                            <div class="col-lg-12 soc_items">
                                <i class="fa fa-facebook-f"></i>
                            </div>
                        </div>
                        <div class="col-lg-4 ">
                            <div class="col-lg-12 soc_items">
                                <i class="fa fa-instagram"></i>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="col-lg-12 soc_items">
                                <i class="fa fa-vk"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <ul class="fs_ul">
                        <li>ЗАКАЗ ШТОР</li>
                        <li>ЦЕНЫ</li>
                        <li>ВИДЫ ШТОР</li>
                        <li>ПОРТФОЛИО</li>
                        <li>КАТАЛОГ ТКАНЕЙ</li>
                    </ul>
                </div>
                <div class="col-lg-3">
                    <ul class="fs_ul">
                        <li>карнизы</li>
                        <li>солнцезащитная  система </li>
                        <li>ЖУРНАЛ</li>
                        <li>О НАС</li>
                        <li>КОНТАКТЫ</li>
                    </ul>
                </div>
                <div class="col-lg-3">
                    <ul class="fs_ul">
                        <li>тел: +7-926-606-42-78</li>
                        <li>e-mail: D.DIZAINA@YANDEX.RU​</li>
                        <li>121471, Г МОСКВА, УЛИЦА РЯБИНОВАЯ, </li>
                        <li>ДОМ 41 КОРПУС 1 СТР 6, ЭТАЖ 2 ПОМ 27​</li>
                        <li><a href="<?=$baseurl?>/privacy">Политика конфиденциальности</a> </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="foot_th">
        <p>© 2018 DESIGNED BY UBILOPER LLC</p>
    </div>


    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog my_mod col-xs-12 center  clear">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="col-lg-12 col-xs-12">
                <div class="mod_title">
                    <div class="mod_img">
                        <img src="<?=$baseurl?>/assets/images/content/logo_design_house.png" />
                    </div>
                    <div class="mod_t_p">
                        ПРИГЛАСИТЕ ДИЗАЙНЕРА
                    </div>
                    <div class="mod_t_ph">
                        Tel: +7 926-606-42-78  |  D.DIZAINA@YANDEX.RU​
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-xs-12 clear mod_mains">
                <div class="col-lg-10 col-xs-10 clear center nopadding">
                    <div class="col-lg-6 col-xs-12">
                        <div class="mod_rows">
                            <input type="text" placeholder="УКАЖИТЕ ВАШЕ ИМЯ" />
                            <div class="ca_mod_icon">
                                <i class="fa fa-user"></i>
                            </div>
                        </div>
                        <div class="mod_rows">
                            <input type="text" placeholder="УКАЖИТЕ ВАШЕ ТЕЛЕФОН" />
                            <div class="ca_mod_icon">
                                <i class="fa fa-phone"></i>
                            </div>
                        </div>
                        <div class="mod_rows">
                            <input type="text" placeholder="УКАЖИТЕ ВАШЕ EMAIL" />
                            <div class="ca_mod_icon">
                                <i class="fa fa-envelope"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="mod_rows">
                            <textarea type="text" placeholder="СООБШЕНИЕ"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-xs-12 clear mod_main_send">
                <span class="mod_span">отправить</span>
            </div>
        </div>
    </div>

    <div class="send_loading">
        <img src="<?=$baseurl?>/assets/images/content/load.gif" />
    </div>
    <div class="success_send" style="top: -10000px;">
        Сообщение отправлено успешно
    </div>
    <div class="success_error" style="top: -10000px;">
        Сообщение не отправлено
    </div>
    <div class="send_error_vse" style="top: -10000px;">

    </div>
</footer>


<script>
    $('.send_fButton').click(function () {
        $('.send_loading').css({'display':'block'})
        var name = $('.b_req_name').val();
        var phone = $('.b_req_phone').val();
        var email = $('.b_req_email').val();
        if(ValidateEmail(email)){
            if(ValidatePhoneNumber(phone)){
                if(name.length > 0){
                    var body = 'name='+name+'&phone='+phone+'&email='+email+'&type=1'
                    var url = base+'/msg/invite/';
                    requestPost(url, body, function () {
                        if(this.readyState == 4){
                            var result = JSON.parse(this.responseText);
                            if(result.error){
                                $('.send_loading').css({'display':'none'});
                                $('.success_error').css({
                                    'top':'50px'
                                });
                                setTimeout(function () {
                                    $('.success_error').css({
                                        'top':'-10000px'
                                    });
                                },2000);
                            }else{
                                $('.send_loading').css({'display':'none'});
                                $('.success_send').css({
                                    'top':'50px'
                                });
                                setTimeout(function () {
                                    $('.success_send').css({
                                        'top':'-10000px'
                                    });
                                },2000);
                            }
                        }
                    })
                }else{
                    var errorText = 'Заполните все поля';
                    closeError(errorText);
                }
            }else{
                var errorText = 'Неправильный телефонный номер, введите без знака "+"';
                closeError(errorText);
            }
        }else{
            var errorText = 'Неправильный Емейл Адрес';
            closeError(errorText);
        }

    })
</script>