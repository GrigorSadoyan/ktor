<div id='content'>
    <form id='main_form' action='' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title"><?= isset($params['result']['name']) ? $params['result']['name'] : '' ?></h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box_edit box_ck">
                <div class="box_edit box_ck">
                    <h2>Контент</h2>
                    <div class="form_input">
                        <label>Название </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='name' placeholder="Название" value='<?= isset($params['result']['name']) ? $params['result']['name'] : '' ?>'>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="form_input a_form_butt">
            <div class="input_group clen">
                <button class='save' for='main_form'>Save</button>
            </div>
        </div>
    </form>
</div>
<script>

    $(document).ready(function () {
        $('#ckeditor1').ckeditor();
    })

</script>