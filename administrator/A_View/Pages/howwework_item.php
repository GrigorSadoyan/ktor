<?php
$urls = trim($_SERVER['REQUEST_URI'], '/\\');
$url= explode("/", $urls);

?>
<div id='content'>
    <form id='main_form' action='' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">
                    ОТЗЫВЫ
                </h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box_edit select_b_ed box_ck">
                <h2>Контент</h2>
                <div class="box_edit box_ck">
                    <div class="form_input">
                        <p class="text_desc">Title </p>
                        <input name="title" class="input_text" value="<?= isset($params['result']['title']) ? $params['result']['title'] : '' ?>" />
                    </div>
                    <div class="form_input">
                        <p class="text_desc">Text </p>
                        <input name="text" class="input_text" value="<?= isset($params['result']['text']) ? $params['result']['text'] : '' ?>" />
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form_input a_form_butt">
                    <div class="input_group clen">
                        <!--                <div class="input_img forsave"><i class="fa fa-floppy-o"></i></div>-->
                        <button class='save' for='main_form'>Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>