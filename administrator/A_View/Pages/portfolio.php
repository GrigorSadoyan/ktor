<div id='content'>

    <div id='table_div'>
        <div class="table_head_main">
            <div class="table_main_head clen">
                <div class='table_head'>
                    <h3></h3>
                </div>
                <div class='table_head_name'>
                    <h1>ПОРТФОЛИО </h1>
                </div>
                <div class='table_head fnone'>
                    <div class="form_input">
                        <div class="input_group add_project">
                            <a href='<?= $baseurl ?>/portfolio/add/' class='save'>Add </a>
                        </div>
                        <div class="input_group add_project">
                            <a href='<?= $baseurl ?>/page/5' class='save'>Edit Page </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ex_filtrs_col clen"></div>


        <table>
            <thead>
            <tr>
                <th class='table_num'>#</th>
                <th class='w_10'>Name</th>
                <th class='table_action last_th'>Action</th>
            </tr>
            </thead>
            <tbody id="main_tbody" data-table=''>
            <?php
            $numbered = 0;
            if (isset($params['result'])) {

                foreach ($params['result'] as $val) {
                    $numbered++
                    ?>
                    <tr id='m_<?= $val['id'] ?>'>
                        <td>
                            <span><?= $numbered ?></span>
                        </td>
                        <td>
                            <a href='<?= $baseurl ?>/portfolio/<?= $val['id'] ?>/'>
                                <span class="table_imgae"><?= $val['name'] ?></span>
                            </a>
                        </td>
                        <td class='last_td'>
                            <a href='<?= $baseurl ?>/portfolio/<?= $val['id'] ?>/'>
                                <span class='action_td'>
                                    <img src="<?= $baseurl ?>/a_assets/images/icons/edit.png" alt="">
                                </span>
                            </a>
                            <span class='action_td action_delete' data-id="<?= $val['id'] ?>" data-get='portfolio'>
                                <img src="<?= $baseurl ?>/a_assets/images/icons/delete_icon.png" alt="">
                            </span>
                            <span class='action_td sorttable'>
                                <img src="<?= $baseurl ?>/a_assets/images/icons/rearrange_icon_dark.png" alt="">
                            </span>
                        </td>
                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#main_tbody').sortable({
            axis: "y",
            handle: '.sorttable',
            update: function () {
                ords = $(this).sortable("toArray");
                console.log($(this).sortable("toArray"))
                var url = base+"/portfolio/sort/";
                var body = "ord="+ords+"";
                requestPost(url,body,function(){
                    if(this.readyState == 4){
                        var result = JSON.parse(this.responseText);
                        if(result.error){
                            self.parent('td').parent('tr').fadeOut();
                        }else{

                        }
                    }
                })
            }
        });
    });
</script>