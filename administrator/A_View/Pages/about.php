<div id='content'>
    <form id='main_form' action='' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">О нас</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
                <div class='table_head fnone'>
                    <div class="form_input">
                        <div class="input_group add_project">
                            <a href='<?= $baseurl ?>/about/items/' class='save'>Колонки </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box_edit box_ck">
                <div class="box_edit box_ck">
                    <h2>SEO Инструменты</h2>
                    <div class="form_input">
                        <label>SEO Title </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text"  name='seo_title' placeholder="SEO Title" value='<?= isset($params['result']['seo_title']) ? $params['result']['seo_title'] : '' ?>'>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>SEO Description </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text"  name='seo_desc' placeholder="SEO Description" value='<?= isset($params['result']['seo_desc']) ? $params['result']['seo_desc'] : '' ?>'>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>SEO Text </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text"  name='seo_text' placeholder="SEO Text" value='<?= isset($params['result']['seo_text']) ? $params['result']['seo_text'] : '' ?>'>
                        </div>
                    </div>
                    <hr/>
                    <h2>Контент</h2>
                    <div class="form_input">
                        <label>Название 1</label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='title_one' placeholder="Название 1" value='<?= isset($params['result']['title_one']) ? $params['result']['title_one'] : '' ?>'>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>Текст 1</label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <textarea class="area_tx" required name='text_one' id="ckeditor8" placeholder="Текст 1"><?= isset($params['result']['text_one']) ? $params['result']['text_one'] : '' ?></textarea>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>Название 2</label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='title_two' placeholder="Название 2" value='<?= isset($params['result']['title_two']) ? $params['result']['title_two'] : '' ?>'>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>Текст 2</label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <textarea class="area_tx" required name='text_two' id="ckeditor9" placeholder="Текст 2"><?= isset($params['result']['text_two']) ? $params['result']['text_two'] : '' ?></textarea>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="form_input a_form_butt">
                <div class="input_group clen">
                    <button class='save' for='main_form'>Save</button>
                </div>
            </div>
    </form>
</div>
<script>

    $(document).ready(function () {
        $('#ckeditor8').ckeditor();
        $('#ckeditor9').ckeditor();
    })

</script>
