<?php
$urls = trim($_SERVER['REQUEST_URI'], '/\\');
$url= explode("/", $urls);

?>
<div id='content'>
    <form id='main_form' action='' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">
                    ОТЗЫВЫ
                </h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box_edit select_b_ed box_ck">
                <h2>Контент</h2>
                <div class="box_edit box_ck">
                    <div class="form_input">
                        <label>Имя</label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" name='name' placeholder="Имя" value='<?= isset($params['result']['name']) ? $params['result']['name'] : '' ?>'>
                        </div>
                    </div>

                    <div class="form_input">
                        <p class="text_desc">Description </p>
                        <textarea name="description" class="area_tx" id="ckeditor7"><?= isset($params['result']['description']) ? $params['result']['description'] : '' ?></textarea>
                    </div>
                    <div class="box_edit box_ck">
                        <div class="form_input">
                            <div class="form_input">
                                <div class="input_group clen">
                                    <div class="chose_file _chose_file">Выбрать Фото</div>
                                    <div class='foto_block forempty child_ab'>
                                        <img src='<?= isset($params['result']['image']) && $params['result']['image'] != '' ? $params['own']['images_path'].$params['result']['image'] : $baseurl."/a_assets/images/front/no-image.svg" ?>' />
                                        <input style="display: none" type="file" name="image" class="img_file ab_img_f" >
                                        <!--                                    <div class='empty_foto'><i class="fa fa-picture-o"></i></div>-->
                                        <div class='full_foto'></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="upimg" value="<?= isset($params['result']['image']) && $params['result']['image'] != '' ? $params['result']['image'] : ''?>" >
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form_input a_form_butt">
                    <div class="input_group clen">
                        <!--                <div class="input_img forsave"><i class="fa fa-floppy-o"></i></div>-->
                        <button class='save' for='main_form'>Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function () {
        $('#ckeditor7').ckeditor();
        $('.box_prices_item_del').click(function () {
            if(!confirm("Are you sure delete this price?")){return false;}
            var self = $(this);
            var url = base+"/product/shtorprice/delete/";
            var id = $(this).data('prid');
            var body = "id="+id+"";
            requestPost(url,body,function(){
                if(this.readyState == 4){
                    var result = JSON.parse(this.responseText);
                    if(result.error){
                        self.parent('.kk_'+id).fadeOut();
                    }else{

                    }
                }
            })
        })
</script>