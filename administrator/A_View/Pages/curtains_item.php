<div id='content'>
    <form id='main_form' action='' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title"><?= isset($params['result']['name']) ? $params['result']['name'] : '' ?></h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box_edit box_ck">
                <div class="box_edit box_ck">
                    <h2>SEO Инструменты</h2>
                    <div class="form_input">
                        <label>SEO Title </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='seo_title' placeholder="SEO Title" value='<?= isset($params['result']['seo_title']) ? $params['result']['seo_title'] : '' ?>'>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>SEO Description </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='seo_desc' placeholder="SEO Description" value='<?= isset($params['result']['seo_desc']) ? $params['result']['seo_desc'] : '' ?>'>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>SEO Text </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='seo_text' placeholder="SEO Text" value='<?= isset($params['result']['seo_text']) ? $params['result']['seo_text'] : '' ?>'>
                        </div>
                    </div>
                    <hr/>
                    <h2>Контент</h2>
                    <div class="form_input">
                        <label>Название </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='name' placeholder="Название" value='<?= isset($params['result']['name']) ? $params['result']['name'] : '' ?>'>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>Текст </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <textarea id="ckeditor1" class="input_text " required name='text' placeholder="Текст" ><?= isset($params['result']['text']) ? $params['result']['text'] : '' ?></textarea>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>Привязание к категории </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <select name="cur_id" class="curid_s">
                                <?php foreach ($params['cur_cats'] as $val) { ?>
                                    <?php if(isset($params['result'])){ ?>
                                    <option value="<?=$val['id']?>"
                                        <?= $val['id'] == $params['result']['cur_id'] ? 'selected' : '' ?>
                                    ><?=$val['name']?></option>
                                        <?php  }else{ ?>
                                        <option value="<?=$val['id']?>"><?=$val['name']?></option>
                                    <?php }?>
                                <?php }  ?>

                            </select>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="box_edit box_ck">
                <div class="form_input">
                    <div class="form_input">
                        <div class="input_group clen">
                            <div class="chose_file _chose_file">Выберите Файл</div>
                            <div class='foto_block forempty child_ab'>
                                <img src='<?= isset($params['result']['image']) && $params['result']['image'] != '' ? $params['own']['images_path'].$params['result']['image'] : $baseurl."/a_assets/images/front/no-image.svg" ?>' />
                                <input style="display: none" type="file" name="image" class="img_file ab_img_f" >
                                <!--                                    <div class='empty_foto'><i class="fa fa-picture-o"></i></div>-->
                                <div class='full_foto'></div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="upimg" value="<?= isset($params['result']['image']) && $params['result']['image'] != '' ? $params['result']['image'] : ''?>" >
            </div>
        </div>
        <div class="form_input a_form_butt">
            <div class="input_group clen">
                <button class='save' for='main_form'>Save</button>
            </div>
        </div>
    </form>
</div>
<script>

    $(document).ready(function () {
        $('#ckeditor1').ckeditor();
    })

</script>