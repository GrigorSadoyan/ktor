<div id='content'>
    <form id='main_form' action='' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">Загрузить Фотографии Галереи (1920 X 1080) , (1366 X 768) </h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class='table_head fnone'>
                <div class="form_input">
                    <div class="input_group add_project">
                        <a href='<?= $baseurl ?>/page/1' class='save'>Edit Page </a>
                    </div>
                </div>
            </div>
            <div class="box_edit box_ck">
                <div class="form_input">
                    <div class="form_input">
                        <div class="input_group clen">
                            <div class="chose_file _chose_file">Выберите Файл</div>
                            <div class='foto_block forempty child_ab'>
                                <img src='../../a_assets/images/front/no-image.svg' />
                                <input style="display: none" type="file" name="image" class="img_file ab_img_f" >
<!--                                <div class='empty_foto'><i class="fa fa-picture-o"></i></div>-->
                                <div class='full_foto'></div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="upimg" value="<?= isset($params['result']['image']) && $params['result']['image'] != '' ? $params['result']['image'] : ''?>" >
            </div>
            <div class="form_input a_form_butt">
                <div class="input_group clen">
                    <button class='save' for='main_form'>Save</button>
                </div>
            </div>
        </div>

    </form>

    <div class='box'>
        <div class='box_header'>
            <h3 class="box-title">Загруженные Фотографии</h3>
            <div class="box-tools">
                <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box_edit box_ck clen" id="main_tbody">
            <?php foreach ($params['result'] as $val) { ?>

                <div class="gal_add_g kk_<?=$val['id']?>" id="m_<?= $val['id'] ?>">
                    <div class="gal_add_dels" data-galid="<?=$val['id']?>">X</div>
                    <div class="gal_add_sort sorttable"><i class="fa fa-sort" aria-hidden="true"></i></div>
                    <img src="<?=$baseurlM?>/assets/images/content/glav_slider/<?=$val['image']?>">
                </div>
            <?php     }?>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        $('#ckeditor1').ckeditor();

        $('#main_tbody').sortable({
            handle: '.sorttable',
            update: function () {
                ords = $(this).sortable("toArray");
                console.log($(this).sortable("toArray"))
                var url = base+"/slidemain/sort/";
                var body = "ord="+ords+"";
                requestPost(url,body,function(){
                    // if(this.readyState == 4){
                    //     var result = JSON.parse(this.responseText);
                    //     if(result.error){
                    //         self.parent('td').parent('tr').fadeOut();
                    //     }else{
                    //
                    //     }
                    // }
                })
            }
        });

        $('.gal_add_dels').click(function () {
            if(!confirm("Are you sure delete this image?")){return false;}
            var self = $(this);
            var url = base+"/slidemain/delete/";
            var id = $(this).data('galid');
            var body = "id="+id+"";
            requestPost(url,body,function(){
                if(this.readyState == 4){
                    var result = JSON.parse(this.responseText);
                    if(result.error){
                        self.parent('.kk_'+id).fadeOut();
                    }else{

                    }
                }
            })
        })
    })

</script>