<div id='content'>
    <form id='main_form' action='' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title"><a href="<?=$baseurl?>/about">О нас</a>  -> Колонки</h3>
            </div>
            <div class="box_edit box_ck">
                <div class="box_edit box_ck">
                    <div class="form_input">
                        <label>Цифра</label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='ints' placeholder="Цифра">
                        </div>
                    </div>
                    <div class="form_input">
                        <label>Текст</label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='text' placeholder="Текст">
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="form_input a_form_butt">
                <div class="input_group clen">
                    <button class='save' for='main_form'>Save</button>
                </div>
            </div>
        </div>
    </form>
    <div id="main_tbody">
        <?php foreach ($params['result'] as $val){ ?>
            <div class="box_edit box_ck ard_added" id="m_<?= $val['id'] ?>">
                <form method="post" action="<?=$baseurl?>/about/<?=$val['id']?>/">
                    <div class="box_edit box_ck">
                        <div class="form_input">
                            <label>Цифра</label>
                            <div class="input_group">
                                <div class="input_img"><i class="fa fa-pencil"></i></div>
                                <input type="text" class="input_text" required name='ints' placeholder="Цифра" value="<?=$val['ints']?>">
                            </div>
                        </div>
                        <div class="form_input">
                            <label>Текст</label>
                            <div class="input_group">
                                <div class="input_img"><i class="fa fa-pencil"></i></div>
                                <input type="text" class="input_text" required name='text' placeholder="Текст" value="<?=$val['text']?>">
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="form_input a_form_butt">
                        <div class="input_group clen">
                            <span class='save  action_delete' data-id="<?= $val['id'] ?>" data-get='about'>Delete</span>
                            <button class='save'>Update</button>
                            <div class="about_add_sort sorttable ui-sortable-handle"><i class="fa fa-sort" aria-hidden="true"></i></div>
                        </div>
                    </div>
                </form>
            </div>

        <?php  } ?>
    </div>
</div>
<script>

    $(document).ready(function () {
        $('#ckeditor8').ckeditor();
        $('#ckeditor9').ckeditor();

        $('#main_tbody').sortable({
            handle: '.sorttable',
            update: function () {
                ords = $(this).sortable("toArray");
                console.log($(this).sortable("toArray"))
                var url = base+"/about/sort/";
                var body = "ord="+ords+"";
                requestPost(url,body,function(){
                    // if(this.readyState == 4){
                    //     var result = JSON.parse(this.responseText);
                    //     if(result.error){
                    //         self.parent('td').parent('tr').fadeOut();
                    //     }else{
                    //
                    //     }
                    // }
                })
            }
        });
    })

</script>
<style>
    .ard_added{
        background: #ffffff;
        position: relative;
        border-bottom: 1px solid ;
    }
</style>