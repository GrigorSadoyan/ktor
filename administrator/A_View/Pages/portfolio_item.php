<div id='content'>
    <form id='main_form' action='' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">ЗАКАЗ ШТОР</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box_edit box_ck">
                <div class="box_edit box_ck">
                    <h2>SEO Инструменты</h2>
                    <div class="form_input">
                        <label>SEO Title </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='seo_title' placeholder="SEO Title" value='<?= isset($params['result']['seo_title']) ? $params['result']['seo_title'] : '' ?>'>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>SEO Description </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='seo_desc' placeholder="SEO Description" value='<?= isset($params['result']['seo_desc']) ? $params['result']['seo_desc'] : '' ?>'>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>SEO Text </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='seo_text' placeholder="SEO Text" value='<?= isset($params['result']['seo_text']) ? $params['result']['seo_text'] : '' ?>'>
                        </div>
                    </div>
                    <hr/>
                    <h2>Контент</h2>
                    <div class="form_input">
                        <label>Название </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='name' placeholder="Название" value='<?= isset($params['result']['name']) ? $params['result']['name'] : '' ?>'>
                        </div>
                    </div>
                    <hr/>
                    <h2>Галлерея</h2>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="box_edit box_ck">
                <div class="form_input">
                    <div class="form_input">
                        <div class="input_group clen">
<!--                            <div class="chose_file _chose_file">Выберите Файл</div>-->
                            <div class='foto_block forempty child_ab'>
                                <img src='<?= isset($params['result']['image']) && $params['result']['image'] != '' ? $params['own']['images_path'].$params['result']['image'] : $baseurl."/a_assets/images/front/no-image.svg" ?>' />
                                <input style="display: none" type="file" name="image" class="img_file ab_img_f">
                                <!--                                    <div class='empty_foto'><i class="fa fa-picture-o"></i></div>-->
                                <div class='full_foto'></div>
                            </div>
                            <div class="foto_attrs">
                                <div>
                                    <p>Image title</p>
                                    <input type="text" name="title">
                                </div>
                                <div>
                                    <p>Image Alt</p>
                                    <input type="text" name="alt">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="upimg" value="<?= isset($params['result']['image']) && $params['result']['image'] != '' ? $params['result']['image'] : ''?>" >
            </div>
        </div>
        <div class="form_input a_form_butt">
            <div class="input_group clen">
                <button class='save' for='main_form'>Save</button>
            </div>
        </div>
    </form>
    <div id="main_tbody">
    <?php if(isset($params['gallery'])){

        foreach ($params['gallery'] as $val){
        ?>
            <div  id='m_<?= $val['id'] ?>'>
                <form id='main_form' action='<?=$baseurl?>/portfolio/gallery/<?=$val['id']?>/' method='post' enctype="multipart/form-data">
                    <div class="form_gal_pics " >
                        <div class="form_input">
                            <div class="form_input">
                                <div class="input_group clen">
                                    <!--title-->
                                    <div class='foto_block forempty child_ab'>
                                        <img src='<?= isset($val['image']) && $val['image'] != '' ? $params['own']['images_path'].$val['image'] : $baseurl."/a_assets/images/front/no-image.svg" ?>' />
                                        <input style="display: none" type="file" name="image" class="img_file ab_img_f">
                                        <!--                                    <div class='empty_foto'><i class="fa fa-picture-o"></i></div>-->
                                        <div class='full_foto'></div>
                                    </div>
                                    <div class="foto_attrs">
                                        <div>
                                            <p>Image title</p>
                                            <input type="text" name="title" value="<?= isset($val['title']) ? $val['title'] : '' ?>">
                                        </div>
                                        <div>
                                            <p>Image Alt</p>
                                            <input type="text" name="alt" value="<?= isset($val['alt']) ? $val['alt'] : '' ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="del_img_gall action_delete" data-id="<?= $val['id'] ?>" data-get='portfolio/gallery'>
                            X
                        </div>
                        <div class="sort_img_gall sorttable" data-id="<?= $val['id'] ?>" data-get='portfolio/gallery'>
                            <img src="<?=$baseurl?>/a_assets/images/icons/rearrange_icon_dark.png" alt="">
                        </div>
                        <div class="form_input a_form_butt">
                            <div class="input_group clen">
                                <button class='save' for='main_form'>Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
    <?php }
    }  ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#main_tbody').sortable({
            axis: "y",
            handle: '.sorttable',
            update: function () {
                ords = $(this).sortable("toArray");
                console.log($(this).sortable("toArray"))
                var url = base+"/portfolio/gallery/sort/";
                var body = "ord="+ords+"";
                requestPost(url,body,function(){
                    if(this.readyState == 4){
                        var result = JSON.parse(this.responseText);
                        if(result.error){
                            self.parent('td').parent('tr').fadeOut();
                        }else{

                        }
                    }
                })
            }
        });
    });
</script>
<style>
    .foto_attrs{
        float: left;
        width: 75%;
    }
    .foto_attrs p{
        margin: 0;
        margin-top: -5px;
        margin-left: 15px;
        background: #f9f9f9;
        width: 96px;
        text-align: center;
        border-radius: 3px;
    }
    .foto_attrs input{
        margin-left: 15px;
        width: 100%;
        border: none;
        border-bottom: 1px solid #e6e7eb;
        outline: none;
        padding: 1px 5px;
    }
    .foto_attrs div{
        margin-bottom: 8px;
    }
    .form_gal_pics{
        border: 1px solid;
        background-color: white;
        position: relative;
    }
    .del_img_gall{
        position: absolute;
        font-size: 18px;
        background-color: #2a62bc;
        color: #ffffff;
        border-radius: 25px;
        padding: 3px 10px;
        cursor: pointer;
        top: 15px;
        right: 15px;
    }
    .del_img_gall:hover{
        background-color: #bc1a15;
    }
    .sort_img_gall{
        position: absolute;
        font-size: 24px;
        color: #818181;
        border-radius: 25px;
        padding: 5px;
        cursor: pointer;
        top: 60px;
        right: 15px;
    }
</style>