<div id='content'>
    <?php
    $urls = trim($_SERVER['REQUEST_URI'], '/\\');
    $url= explode("/", $urls);
    ?>
    <div id='table_div'>
        <div class="table_head_main">
            <div class="table_main_head clen">
                <div class='table_head'>
                    <h3></h3>
                </div>
                <div class='table_head_name'>
                    <?php foreach ($this->CatsArray as $item) { ?>
                        <?php if($item['cat_id'] == $url[3]){ ?>
                        <h1><?=$item['cat_name']?> </h1>
                    <?php  } } ?>
                </div>
                <div class='table_head fnone'>
                    <div class="form_input">
                        <div class="input_group add_project">
                            <a href='<?= $baseurl ?>/product/<?=$url[3]?>/add/' class='save'>Добавить </a>
                        </div>
                        <div class="input_group add_project">
                            <a href="<?= $baseurl ?>/page/<?=$this->CatsArray[$url[3]-1]["go_page_id"]?>" class='save'>Edit Page </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ex_filtrs_col clen"></div>


        <table>
            <thead>
            <tr>
                <th class='table_num'>#</th>
                <th class='w_10'>Имя</th>
                <th class='w_10'>Категория</th>
                <th class='table_action last_th'>Действие</th>
            </tr>
            </thead>
            <tbody id="main_tbody" data-table=''>
            <?php
            $numbered = 0;

            if (isset($params['result'])) {

                foreach ($params['result'] as $val) {
                    $numbered++
                    ?>
                    <tr id='m_<?= $val['id'] ?>'>
                        <td>
                            <span><?= $numbered ?></span>
                        </td>
                        <td>
                            <a href='<?= $baseurl ?>/product/<?= $val['id'] ?>/'>
                                <span class="table_imgae"><?= $val['name'] ?></span>
                            </a>
                        </td>
                        <td>
                            <a href='<?= $baseurl ?>/product/<?= $val['id'] ?>/'>
                                <span class="table_imgae">
                                    <?php
                                        if($url[3] == '1') echo 'Ткань';
                                        if($url[3] == '2') echo 'Карниз';
                                        if($url[3] == '3') echo 'Солнцезащитная Система ';
                                    ?>
                                </span>
                            </a>
                        </td>
                        <td class='last_td'>
                            <a href='<?= $baseurl ?>/product/<?= $val['id'] ?>/'>
                                <span class='action_td'>
                                    <img src="<?= $baseurl ?>/a_assets/images/icons/edit.png" alt="">
                                </span>
                            </a>
                            <span class='action_td action_delete' data-id="<?= $val['id'] ?>" data-get='product'>
                                <img src="<?= $baseurl ?>/a_assets/images/icons/delete_icon.png" alt="">
                            </span>
                        </td>
                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>

    </div>
</div>