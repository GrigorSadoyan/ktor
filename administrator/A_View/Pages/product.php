<div id='content'>

    <div id='table_div'>

        <div class="ex_filtrs_col clen"></div>


        <table>
            <thead>
            <tr>
                <th class='table_num'>#</th>
                <th class='w_10'>Имя</th>
            </tr>
            </thead>
            <tbody id="main_tbody" data-table=''>
            <?php
            $numbered = 0;

            if (isset($params['result'])) {
//                var_dump($params['result']);
                foreach ($params['result'] as $val) {
                    $numbered++
                    ?>
                    <tr id='m_<?= $val['cat_id'] ?>'>
                        <td>
                            <span><?= $numbered ?></span>
                        </td>
                        <td>
                            <a href='<?= $baseurl ?>/product/select/<?= $val['cat_id'] ?>/'>
                                <span class="table_imgae"><?= $val['cat_name'] ?></span>
                            </a>
                        </td>
                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>

    </div>
</div>