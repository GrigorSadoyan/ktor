<div id='content'>
    <form id='main_form' action='<?=$baseurl?>/<?=$params['own']['url']?>/<?= isset($params['update']) && is_numeric($params['update']) ? $params['update']."/" : 'add/' ?>' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">Categories</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box_edit box_ck">
                <div class="box_edit box_ck">
                    <div class="form_input">
                        <label>Name </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='name' placeholder="Name" value='<?= isset($params['result']['name']) ? $params['result']['name'] : '' ?>'>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="box_edit box_ck">
                <div class="form_input">
                    <div class="form_input">
                        <div class="input_group clen">
                            <div class="chose_file _chose_file">CHOOSE FILE</div>
                            <div class='foto_block forempty child_ab'>
                                <img src='<?= isset($params['result']['img']) && $params['result']['img'] != '' ? $params['own']['images_path'].$params['result']['img'] : $baseurl."/a_assets/images/front/no-image.svg" ?>' />
                                <input style="display: none" type="file" name="image" class="img_file ab_img_f" >
                                <!--                                    <div class='empty_foto'><i class="fa fa-picture-o"></i></div>-->
                                <div class='full_foto'></div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="upimg" value="<?= isset($params['result']['img']) && $params['result']['img'] != '' ? $params['result']['img'] : ''?>" >
            </div>
        </div>
        <div class="form_input a_form_butt">
            <div class="input_group clen">
                <button class='save' for='main_form'>Save</button>
            </div>
        </div>
    </form>
</div>