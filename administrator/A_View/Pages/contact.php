<div id='content'>
    <form id='main_form' action='' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">ЗАКАЗ ШТОР</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box_edit box_ck">
                <div class="box_edit box_ck">
                    <h2>SEO Инструменты</h2>
                    <div class="form_input">
                        <label>SEO Title </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='seo_title' placeholder="SEO Title" value='<?= isset($params['result']['seo_title']) ? $params['result']['seo_title'] : '' ?>'>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>SEO Description </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='seo_desc' placeholder="SEO Description" value='<?= isset($params['result']['seo_desc']) ? $params['result']['seo_desc'] : '' ?>'>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>SEO Text </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" required name='seo_text' placeholder="SEO Text" value='<?= isset($params['result']['seo_text']) ? $params['result']['seo_text'] : '' ?>'>
                        </div>
                    </div>
                    <hr/>
                    <h2>Контент</h2>
                </div>
            </div>
            <div class="form_input">
                <label>Адресс </label>
                <div class="input_group">
                    <div class="input_img"><i class="fa fa-pencil"></i></div>
                    <input type="text" class="input_text" required name='address' placeholder="" value='<?= isset($params['result']['address']) ? $params['result']['address'] : '' ?>'>
                </div>
            </div>
            <div class="form_input">
                <label>Телефон </label>
                <div class="input_group">
                    <div class="input_img"><i class="fa fa-pencil"></i></div>
                    <input type="text" class="input_text" required name='phone' placeholder="" value='<?= isset($params['result']['phone']) ? $params['result']['phone'] : '' ?>'>
                </div>
            </div>
            <div class="form_input">
                <label>E-Mail </label>
                <div class="input_group">
                    <div class="input_img"><i class="fa fa-pencil"></i></div>
                    <input type="text" class="input_text" required name='email' placeholder="" value='<?= isset($params['result']['email']) ? $params['result']['email'] : '' ?>'>
                </div>
            </div>
            <div class="form_input">
                <label>Сайт </label>
                <div class="input_group">
                    <div class="input_img"><i class="fa fa-pencil"></i></div>
                    <input type="text" class="input_text" required name='site' placeholder="" value='<?= isset($params['result']['site']) ? $params['result']['site'] : '' ?>'>
                </div>
            </div>
            <div class="form_input">
                <label>Час 1</label>
                <div class="input_group">
                    <div class="input_img"><i class="fa fa-pencil"></i></div>
                    <input type="text" class="input_text" required name='time_one' placeholder="" value='<?= isset($params['result']['time_one']) ? $params['result']['time_one'] : '' ?>'>
                </div>
            </div>
            <div class="form_input">
                <label>Час 2</label>
                <div class="input_group">
                    <div class="input_img"><i class="fa fa-pencil"></i></div>
                    <input type="text" class="input_text" required name='time_two' placeholder="" value='<?= isset($params['result']['time_two']) ? $params['result']['time_two'] : '' ?>'>
                </div>
            </div>
        </div>
        <div class="form_input a_form_butt">
            <div class="input_group clen">
                <button class='save' for='main_form'>Save</button>
            </div>
        </div>
    </form>
</div>
<script>

    $(document).ready(function () {
        $('#ckeditor1').ckeditor();
    })

</script>