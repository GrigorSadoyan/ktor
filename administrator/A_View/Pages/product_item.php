<?php
$urls = trim($_SERVER['REQUEST_URI'], '/\\');
$url= explode("/", $urls);

?>
<div id='content'>
    <form id='main_form' action='<?=$baseurl?>/<?=$params['own']['url']?>/<?= isset($params['update']) && is_numeric($params['update']) ? $params['update']."/" : 'add/' ?>' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">
                    <?php if($url[2] == '1') echo 'Добавить Ткань'?>
                    <?php if($url[2] == '2') echo 'Добавить Шторы'?>
                    <?php if($url[2] == '3') echo 'Добавить Солнцезащитную Систему '?>
                </h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box_edit select_b_ed box_ck">
<!--                <div class="clen">-->
<!--                    <div class="slect_filtr_bl slect_filtr_bl_bottom">-->
<!--                        <p class="select_lable">Categories </p>-->
<!--                        <select class="chosen-select" name="sub_sub_categories_id" data-placeholder="Выбрать тип категорию" required="required">-->
<!--                            <option value="0">Select categorie</option>-->
<!--                            --><?php //foreach($params['categories'] as $val){ ?>
<!--                                --><?php //if(isset($params['result']['categories_id']) && $params['result']['categories_id'] == $val['id']){ ?>
<!--                                    <option value="--><?//=$val['id']?><!--" selected>--><?//=$val['name']?><!--</option>-->
<!--                                --><?php //}else{ ?>
<!--                                    <option value="--><?//=$val['id']?><!--">--><?//=$val['name']?><!--</option>-->
<!--                                --><?php //} ?>
<!--                            --><?php //} ?>
<!--                        </select>-->
<!--                    </div>-->
<!--                </div>-->

                <div class="box_edit box_ck">
                    <h2>SEO Инструменты</h2>
                    <div class="form_input">
                        <label>SEO Title </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text"  name='seo_title' placeholder="SEO Title" value='<?= isset($params['result']['seo_title']) ? $params['result']['seo_title'] : '' ?>'>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>SEO Description </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text"  name='seo_desc' placeholder="SEO Description" value='<?= isset($params['result']['seo_desc']) ? $params['result']['seo_desc'] : '' ?>'>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>SEO Text </label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text"  name='seo_text' placeholder="SEO Text" value='<?= isset($params['result']['seo_text']) ? $params['result']['seo_text'] : '' ?>'>
                        </div>
                    </div>
                    <hr/>
                    <h2>Контент</h2>
                </div>
                <div class="box_edit box_ck">
                    <div class="form_input">
                        <p>В имени продукта не должны быть знаки  "/" и "-" </p>
                        <label>Имя</label>
                        <div class="input_group">

                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" name='name' placeholder="Имя" value='<?= isset($params['result']['name']) ? $params['result']['name'] : '' ?>'>
                        </div>
                    </div>
                    <div class="form_input">
                        <label>Код Продукта</label>
                        <div class="input_group">
                            <div class="input_img"><i class="fa fa-pencil"></i></div>
                            <input type="text" class="input_text" name='code' placeholder="Код" value='<?= isset($params['result']['code']) ? $params['result']['code'] : '' ?>'>
                        </div>
                    </div>

                    <input type="hidden" class="input_text" name='type_id' placeholder="port" value='<?=$url[2]?>'>

                    <div class="form_input">
                        <p class="text_desc">Description </p>
                        <textarea name="description" class="area_tx" id="ckeditor7"><?= isset($params['result']['description']) ? $params['result']['description'] : '' ?></textarea>
                    </div>
                    <div class="box_edit box_ck">
                        <div class="form_input">
                            <div class="form_input">
                                <div class="input_group clen">
                                    <div class="chose_file _chose_file">Выбрать Фото</div>
                                    <div class='foto_block forempty child_ab'>
                                        <img src='<?= isset($params['result']['image']) && $params['result']['image'] != '' ? $params['own']['images_path'].$params['result']['image'] : $baseurl."/a_assets/images/front/no-image.svg" ?>' />
                                        <input style="display: none" type="file" name="image" class="img_file ab_img_f" >
                                        <!--                                    <div class='empty_foto'><i class="fa fa-picture-o"></i></div>-->
                                        <div class='full_foto'></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="upimg" value="<?= isset($params['result']['image']) && $params['result']['image'] != '' ? $params['result']['image'] : ''?>" >
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form_input a_form_butt">
                    <div class="input_group clen">
                        <!--                <div class="input_img forsave"><i class="fa fa-floppy-o"></i></div>-->
                        <button class='save' for='main_form'>Сохранить</button>
                    </div>
                </div>
            </div>

        </div>



    </form>

    <?php if(isset($params['update']) && is_numeric($params['update'])){ ?>
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">Пподключить к филтрам</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box_edit box_ck box_no_hiden">
                <div class="big_add_filtr_col clen">
                    <div class="slect_filtr_bl slect_filtr_bl_bottom add_filtr_name">
                        <p class="select_lable">Добавить Фильтры</p>
                        <select id="filtr_name" class="chosen-select" name="filtr_name_id[]" multiple data-placeholder="Выбрать категорию">
                            <?php foreach($params['filtr_name'] as $val){ ?>
                                <?php if(in_array($val['id'],$params['filtr_name_id'])){ ?>
                                    <option value="<?=$val['id']?>" data-name="<?=$val['name']?>" selected><?=$val['name']?></option>
                                <?php }else{ ?>
                                    <option value="<?=$val['id']?>" data-name="<?=$val['name']?>"><?=$val['name']?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div id="all_pro_filtr" class="clen">
                    <?php foreach($params['filtr_name_filtrs'] as $value){ ?>
                        <div class='filtrs_colum'>
                            <p class='filtrs_title'><?=$value['name']?></p>
                            <?php foreach($value['filtrs'] as $val){ ?>
                                <ul class='filtr_ul'>
                                    <li class='filtr_li'>
                                        <input id='<?=$val['id']?>'
                                               class='filtr_change'
                                               data-pid='<?=$value['id']?>'
                                               value="<?=$val['id']?>"
                                               type='checkbox'
                                                <?= $val['selected'] ? 'checked' : '' ?>
                                        >
                                    </li>
                                    <li class='filtr_li'>
                                        <label style="background-color: #<?=$val['name']?>" for='<?=$val['id']?>'><?=$val['name']?></label>
                                    </li>
                                </ul>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>


        <div class="box">
            <div class='box_header'>
                <h3 class="box-title">Добавить цены</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box_edit box_ck box_no_hiden">
                <div class="box_prices_main">
                    <?php  if($params['result']['type_id'] == '2'){ ?>
                    <form method="post" action="<?=$baseurl?>/product/prodprice/<?=$url[2]?>/">
                        <?php
//                            var_dump($params['result']['type_id']);
                            foreach ($params['shtor_ryad'] as $val){
                        ?>
                        <div class="box_prices_item clen">
                            <div class="box_prices_i">
                                <span><?=$val['ryads']?> Ряд ...........................</span>
                            </div>
                            <div class="box_prices_i">
                                <p>Длина Штора</p>
                                <input type="text" class="" name='dlina[]' placeholder="Длина">
                            </div>
                            <div class="box_prices_i">
                                <p>Цена</p>
                                <input type="text" class="" name='price[]' placeholder="Цена">
                            </div>
                            <div class="box_prices_i">
                                <p>Цена со Скидкой</p>
                                <input type="text" class="" name='sale_price[]' placeholder="Цена со Скидкой">
                            </div>
                        </div>

                    <?php   }  ?>

                        <div class="form_input a_form_butt">
                            <div class="input_group clen">
                                <!--                <div class="input_img forsave"><i class="fa fa-floppy-o"></i></div>-->
                                <button class='save' for='main_form'>Сохранить</button>
                            </div>
                        </div>
                    </form>
                    <?php  }elseif($params['result']['type_id'] == '1'){ ?>
                        <form method="post" action="<?=$baseurl?>/product/prodprice/<?=$url[2]?>/">
                            <div class="box_prices_item clen">
                                <div class="box_prices_i">
                                    <p>Цена Ткани</p>
                                    <input type="text" class="" name='price' placeholder="Цена" value="<?= isset($params['tkan_price'][0]['price']) ? $params['tkan_price'][0]['price'] : '' ?>">
                                </div>
                                <div class="box_prices_i">
                                    <p>Цена Ткани со Скидкой</p>
                                    <input type="text" class="" name='sale_price' placeholder="Цена со Скидкой" value="<?= isset($params['tkan_price'][0]['sale_price']) ? $params['tkan_price'][0]['sale_price'] : '' ?>">
                                </div>
                            </div>
                            <div class="form_input a_form_butt">
                                <div class="input_group clen">
                                    <!--                <div class="input_img forsave"><i class="fa fa-floppy-o"></i></div>-->
                                    <button class='save' for='main_form'>Сохранить</button>
                                </div>
                            </div>
                        </form>
                    <?php }elseif($params['result']['type_id'] == '3'){ ?>
                        Будут Характеристики Цен Для Солнцезашитной Системы
                    <?php  } ?>
                </div>
            </div>
        </div>













<!--    --><?php // if(isset($params['shtor_ryad']) && count($params['shtor_price']) != 0){
       ?>
    <?php if($params['result']['type_id'] == '2'){ ?>
        <div class="box">
            <div class='box_header'>
                <h3 class="box-title">Добавленные цены</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box_edit box_ck box_no_hiden">
                <div class="box_prices_main">

                       <?php foreach ($params['shtor_price'] as $val) {
                            $i = 2;
                            foreach ($val as $vals) {
                                ?>
                                    <div class="box_prices_item clen kk_<?=$vals['id']?>">
                                        <div class="box_prices_item_del" data-prid="<?=$vals['id']?>">X</div>
                                        <div class="box_prices_i">
                                            <span><?=$vals['shtor_ryad_id']?> Ряд ............ <span class="box_prices_i_spa"><?=$vals['dlina']?></span> см.</span>
                                        </div>
<!--                                        <div class="box_prices_i">-->
<!--                                            <p>Длина Штора</p>-->
<!--                                            <input type="text" disabled class="" value="">-->
<!--                                        </div>-->
                                        <div class="box_prices_i">
                                            <p>Цена</p>
                                            <input type="text" disabled class="" value="<?=$vals['price']?>">
                                        </div>
                                        <div class="box_prices_i">
                                            <p>Цена со Скидкой</p>
                                            <input type="text" disabled class="" value="<?=$vals['sale_price']?>">
                                        </div>
                                    </div>
                                <?php
                                $i++;
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php }  ?>
<!--    --><?php //} ?>

</div>
<script>

    $(document).ready(function () {
        $('#ckeditor7').ckeditor();
        $('.box_prices_item_del').click(function () {
            if(!confirm("Are you sure delete this price?")){return false;}
            var self = $(this);
            var url = base+"/product/shtorprice/delete/";
            var id = $(this).data('prid');
            var body = "id="+id+"";
            requestPost(url,body,function(){
                if(this.readyState == 4){
                    var result = JSON.parse(this.responseText);
                    if(result.error){
                        self.parent('.kk_'+id).fadeOut();
                    }else{

                    }
                }
            })
        })
        /* multy preview */
        $( "._foto_block_multy" ).click(function() {
            $(this).children(".img_file_multy" )[0].click();
        });


        $('#filtr_name').on('change', function(evt, params) {
            console.log('evt',$(this).data('name'));
            console.log('evt',evt);
            console.log('params',params);
            if(params.selected != 'undefined'){
                var url = base+"/product/filtr/"+params.selected+"/";
                requestGet(url,function () {
                    if (this.readyState === 4) {
                        var result = JSON.parse(this.responseText);
                        addFiltrs(result)
                    }
                })
            }else if(params.deselected != 'undefined'){

            }
        });

        function addFiltrs(param) {
            if(param.data.length > 0) {
                var perepend = '';
                perepend += "<div class='filtrs_colum'><p class='filtrs_title'>" + param.filtr_name['name'] + "</p>";
                for (var i = 0; i < param.data.length; i++) {
                    perepend +=   "<ul class='filtr_ul'>" +
                                        "<li class='filtr_li'>" +
                                            "<input id='" + param.data[i]['id'] + "' class='filtr_change' data-pid='"+param.filtr_name['id']+"' value='" + param.data[i]['id'] + "' type='checkbox'>" +
                                        "</li>" +
                                        "<li class='filtr_li'>" +
                                            "<label style=background-color:#"+ param.data[i]['name'] +" for='" + param.data[i]['id'] + "'>" + param.data[i]['name'] + "</label>" +
                                        "</li>" +
                                   "</ul>";

                }
                perepend += "</div>";
                $("#all_pro_filtr").prepend(perepend);
            }
        }

        $(document).on("change",".filtr_change",function() {
            var addOrRe = '';
            if($(this).prop("checked")){
                addOrRe = 'add';
            }else{
                addOrRe = 'delete';
            }
            var filtr_id = $(this).val();
            var filtr_name_id = $(this).data('pid');
            var url = base+"/product/filtr/"+addOrRe+"/<?= isset($params['update']) && is_numeric($params['update']) ? $params['update'] : '0' ?>/";
            var body = "filtr_id="+filtr_id+"&filtr_name_id="+filtr_name_id+"";
            requestPost(url,body,function () {
                if (this.readyState === 4) {
                    //var result = JSON.parse(this.responseText);
                }
            })
        })

        function renderImage(file,x){
            var reader = new FileReader();
            reader.onload = function(event){
                the_url = event.target.result;
                appendBody = "<div class='gal_img_col sprite_col add_new_sp'>" +
                    "<img src='"+the_url+"' alt=''>" +
                    "</div>";
                $("#sort_sprite").append(appendBody);
            }
            reader.readAsDataURL(file);
        }

        $( ".img_file_multy" ).change(function() {
            $(".add_new_sp").remove();
            for (var i = 0; i < this.files.length; i++) {
                renderImage(this.files[i], this)
            }
        });
        /* multy preview */

        $(".filt_select").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });

        var config = {
            '.chosen-select'           : {no_results_text: "Oops, nothing found!", width: "100%"},
            '.chosen-select-deselect'  : { allow_single_deselect: true },
            '.chosen-select-no-single' : { disable_search_threshold: 10 },
            '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
            '.chosen-select-rtl'       : { rtl: true },
            '.chosen-select-width'     : { width: '95%' }
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }


        $('.delete_img_gall').click(function () {
            if(!confirm("Are you sure delete this image?")){return false;}
            var self = $(this);

            var contr = $(this).data('get');
            var url = base+"/product/sprite/delete/";

            var id = $(this).data('id');
            var body = "id="+$(this).data('id')+"";
            requestPost(url,body,function(){
                if(this.readyState == 4){
                    var result = JSON.parse(this.responseText);
                    if(result.error){
                        self.parent('.gal_img_col').fadeOut();
                    }else{

                    }
                }
            })
        })
    })

</script>