<div id="menu">	
<!--    <div class='user_panel'>-->
<!--        <div class="pull-left image">-->
<!--            <img src="--><?//= $baseurl ?><!--/assets/images/user/1.jpg" class="img-circle" alt="User Image">-->
<!--        </div>-->
<!--        <div class="pull-left info">-->
<!--            <p>GYM</p>-->
<!--            <a href="#"><i class="fa fa-circle"></i> Online</a>-->
<!--        </div>-->
<!--    </div>-->
    <ul class='sidebar-menu'>
        <li class='header'>Add Filtr</li>
        <a href='<?= $baseurl ?>/filtrname/'>
            <li class="menuli">
                <i class="inline_menu fa fa-filter <?= $page == 'filtr' ? "active_i" : "" ?>" aria-hidden="true"></i>
                <span class="menu_text inline_menu <?= $page == 'filtr' ? "active_span_menu" : "" ?>">Filtrs </span>
            </li>
        </a>
        <li class='header'>Filtrs</li>
        <?php if(count($params['menu_filtr_name']) > 0){ ?>
            <?php foreach ($params['menu_filtr_name'] as $val){ ?>
                <a href='<?= $baseurl ?>/filtrs/<?=$val['id']?>'>
                    <li class="menuli">
                        <i class="inline_menu fa fa-filter <?= $page == 'categories' ? "active_i" : "" ?>" aria-hidden="true"></i>
                        <span class="menu_text inline_menu <?= $page == 'categories' ? "active_span_menu" : "" ?>"><?=$val['name']?> </span>
                    </li>
                </a>
            <?php } ?>
        <?php } ?>
        <li class='header'>Main Menu</li>
        <a href='<?= $baseurl ?>/slidemain/'>
            <li class="menuli">
                <i class="inline_menu fa fa-picture-o <?= $page == 'slidemain' ? "active_i" : "" ?>" aria-hidden="true"></i>
                <span class="menu_text inline_menu <?= $page == 'slidemain' ? "active_span_menu" : "" ?>">Слайд Главной Страницы</span>
            </li>
        </a>
<!--        <a href='--><?//= $baseurl ?><!--/categories/'>-->
<!--            <li class="menuli">-->
<!--                <i class="inline_menu fa fa-bars --><?//= $page == 'categories' ? "active_i" : "" ?><!--" aria-hidden="true"></i>-->
<!--                <span class="menu_text inline_menu --><?//= $page == 'categories' ? "active_span_menu" : "" ?><!--">Categories </span>-->
<!--            </li>-->
<!--        </a>-->
        <a href='<?= $baseurl ?>/product/'>
            <li class="menuli">
                <i class="inline_menu fa fa-shopping-cart <?= $page == 'product' ? "active_i" : "" ?>" aria-hidden="true"></i>
                <span class="menu_text inline_menu <?= $page == 'product' ? "active_span_menu" : "" ?>">Продукты </span>
            </li>
        </a>
<!--        <li class='header'>Главный контент</li>-->
<!--        <a href='--><?//= $baseurl ?><!--/slide/'>-->
<!--            <li class="menuli">-->
<!--                <i class="inline_menu fa fa-picture-o --><?//= $page == 'slide' ? "active_i" : "" ?><!--" aria-hidden="true"></i>-->
<!--                <span class="menu_text inline_menu --><?//= $page == 'slide' ? "active_span_menu" : "" ?><!--">Слайд </span>-->
<!--            </li>-->
<!--        </a>-->
<!--        <a href='--><?//= $baseurl ?><!--/about/1/'>-->
<!--            <li class="menuli">-->
<!--                <i class="inline_menu fa fa-industry --><?//= $page == 'about' ? "active_i" : "" ?><!--" aria-hidden="true"></i>-->
<!--                <span class="menu_text inline_menu --><?//= $page == 'about' ? "active_span_menu" : "" ?><!--">О нас </span>-->
<!--            </li>-->
<!--        </a>-->
        <a href='<?= $baseurl ?>/ordercurtains/'>
            <li class="menuli">
                <i class="inline_menu fa fa-shopping-cart <?= $page == 'ordercurtains' ? "active_i" : "" ?>" aria-hidden="true"></i>
                <span class="menu_text inline_menu <?= $page == 'ordercurtains' ? "active_span_menu" : "" ?>">ЗАКАЗ ШТОР </span>
            </li>
        </a>
        <a href='<?= $baseurl ?>/ceni/'>
            <li class="menuli">
                <i class="inline_menu fa fa-shopping-cart <?= $page == 'ceni' ? "active_i" : "" ?>" aria-hidden="true"></i>
                <span class="menu_text inline_menu <?= $page == 'ceni' ? "active_span_menu" : "" ?>">ЦЕНЫ</span>
            </li>
        </a>
        <a href='<?= $baseurl ?>/curtaincat/'>
            <li class="menuli">
                <i class="inline_menu fa fa-shopping-cart <?= $page == 'curtaincat' ? "active_i" : "" ?>" aria-hidden="true"></i>
                <span class="menu_text inline_menu <?= $page == 'curtaincat' ? "active_span_menu" : "" ?>">ВИДЫ ШТОР - Категории</span>
            </li>
        </a>
        <a href='<?= $baseurl ?>/curtains/'>
            <li class="menuli">
                <i class="inline_menu fa fa-shopping-cart <?= $page == 'curtains' ? "active_i" : "" ?>" aria-hidden="true"></i>
                <span class="menu_text inline_menu <?= $page == 'curtains' ? "active_span_menu" : "" ?>">ВИДЫ ШТОР - Колонки</span>
            </li>
        </a>
        <a href='<?= $baseurl ?>/portfolio/'>
            <li class="menuli">
                <i class="inline_menu fa fa-shopping-cart <?= $page == 'portfolio' ? "active_i" : "" ?>" aria-hidden="true"></i>
                <span class="menu_text inline_menu <?= $page == 'portfolio' ? "active_span_menu" : "" ?>">ПОРТФОЛИО</span>
            </li>
        </a>
        <a href='<?= $baseurl ?>/album/'>
            <li class="menuli">
                <i class="inline_menu fa fa-shopping-cart <?= $page == 'album' ? "active_i" : "" ?>" aria-hidden="true"></i>
                <span class="menu_text inline_menu <?= $page == 'album' ? "active_span_menu" : "" ?>">ЖУРНАЛ</span>
            </li>
        </a>
        <a href='<?= $baseurl ?>/feedback/'>
            <li class="menuli">
                <i class="inline_menu fa fa-commenting-o <?= $page == 'feedback' ? "active_i" : "" ?>" aria-hidden="true"></i>
                <span class="menu_text inline_menu <?= $page == 'feedback' ? "active_span_menu" : "" ?>">ОТЗЫВЫ</span>
            </li>
        </a>
        <a href='<?= $baseurl ?>/howwework/'>
            <li class="menuli">
                <i class="inline_menu fa fa-commenting-o <?= $page == 'howwework' ? "active_i" : "" ?>" aria-hidden="true"></i>
                <span class="menu_text inline_menu <?= $page == 'howwework' ? "active_span_menu" : "" ?>">КАК МЫ РАБОТАЕМ</span>
            </li>
        </a>
        <a href='<?= $baseurl ?>/about/'>
            <li class="menuli">
                <i class="inline_menu fa fa-commenting-o <?= $page == 'about' ? "active_i" : "" ?>" aria-hidden="true"></i>
                <span class="menu_text inline_menu <?= $page == 'about' ? "active_span_menu" : "" ?>">О НАС</span>
            </li>
        </a>
        <a href='<?= $baseurl ?>/contact/'>
            <li class="menuli">
                <i class="inline_menu fa fa-commenting-o <?= $page == 'contact' ? "active_i" : "" ?>" aria-hidden="true"></i>
                <span class="menu_text inline_menu <?= $page == 'contact' ? "active_span_menu" : "" ?>">КОНТАКТЫ</span>
            </li>
        </a>

    </ul>
</div>
