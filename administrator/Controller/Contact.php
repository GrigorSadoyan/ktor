<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
//use Model\Categories as CategoriesModel;
use Model\Contact as mContact;

class Contact extends BaseController
{

    public function __construct($route, $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array(
            //"url"=>"categories",
            //'controller'=>'categories',
            //"table"=>"categories",
//            'images_path' => '../../../../assets/images/content/feedback/',
//            'images_action' => '../assets/images/content/feedback/',
        );


        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'contact') {
                $this->index();
            }
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($countRoute == 1 && $route[0] == 'contact') {
                $this->Update();
            }
        }
    }

    private function index()
    {
        $mContact = new mContact();
        $this->result['result'] = $mContact->findById('1');
        $this->renderView("Pages/contact", "contact", $this->result);
    }

    private function Update()
    {
        $mContact = new mContact();
        $mContact->_put = $_POST;
        $mContact->setId('1');
        $mContact->update();
        $this->headreUrl('contact');
    }
}