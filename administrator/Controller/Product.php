<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\Product as ProductModel;
//use Model\Categories;
use Model\FiltrName;
use Model\Filtrs;
use Model\FiltrProduct;
use Model\KarnizRyad;
use Model\KarnizPrice;
use Model\TkanPrice;

class Product extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array(
            "url"=>"product",
            'controller'=>'product',
            "table"=>"product",
            'images_path'=>'../../../assets/images/product/',
            'images_action'=>'../assets/images/product/',
        );
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'product') {
                $this->index();
            }elseif($countRoute == 3 && $route[0] == 'product' && $route[1] == 'select' && is_numeric($route[2])){
                $this->indexItems($route[2]);
            }elseif($countRoute == 3 && $route[0] == 'product' && is_numeric($route[1]) && $route[2] == 'add'){
                $this->item();
            }elseif($countRoute == 2 && $route[0] == 'product' && is_numeric($route[1])){
                $this->item($route[1]);
            }
            elseif($countRoute == 3 && $route[1] == 'filtr' && is_numeric($route[2])){
                $this->getFiltr($route[2]);
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 2 && $route[0] == 'product' && $route[1]=='add'){
                $this->addItem();
            }elseif($countRoute == 2 && $route[0] == 'product' && is_numeric($route[1])){
                $this->updateItem($route[1]);
            }
            elseif($countRoute == 2 && $route[0] == 'product' && $route[1]=='delete'){
                $this->deleteItem();
            }
//elseif($countRoute == 3 && $route[1] == 'sprite' && $route[2] == 'delete'){
//                $this->deleteSprite();
//            }
            elseif($countRoute == 4 && $route[1] == 'filtr' && $route[2] == 'delete' && is_numeric($route[3])){
                $this->deleteFiltrProduct($route[3]);
            }elseif($countRoute == 4 && $route[1] == 'filtr' && $route[2] == 'add' && is_numeric($route[3])){
                $this->addFiltrProduct($route[3]);
            }elseif($countRoute == 3 && $route[0] == 'product' && $route[1] == 'prodprice' && is_numeric($route[2])){
                $this->addProductPrice($route[2]);
            }elseif($countRoute == 3 && $route[0] == 'product' && $route[1] == 'shtorprice' && $route[2] == 'delete'){
                $this->deleteProductPrice();
            }
        }
    }
    private function deleteProductPrice(){
        $id = $_POST['id'];
        $mShtorPrice = new KarnizPrice();
        $mShtorPrice->delFildName = 'id';
        $mShtorPrice->delValue = $id;
        $mShtorPrice->delete();
        echo json_encode(array('error'=>true));
    }
    private function addProductPrice($id){
//        echo '<pre>';
//        var_dump($_POST);die;
        $mShtorPrice = new KarnizPrice();
        $mProductModel = new ProductModel();
        $aProducts = $mProductModel->findById($id);
        if($aProducts['type_id'] == '2'){
            $oShtorRyad = new KarnizRyad();
            $aShtorRyad = $oShtorRyad->findAll(array());
//            echo '<pre>';
//            var_dump($aShtorRyad);
            $insertData = [];
            if(strlen($_POST['dlina'][0]) != 0 && strlen($_POST['dlina'][1]) != 0){
                for($i = 0; $i<count($aShtorRyad); $i++){
                    $insertData['product_id'] = intval($id);
                    $insertData['shtor_ryad_id'] = intval($aShtorRyad[$i]['ryads']);
                    $insertData['dlina'] = intval($_POST['dlina'][$i]);
                    $insertData['price'] = intval($_POST['price'][$i]);
                    $insertData['sale_price'] = intval($_POST['sale_price'][$i]);
                    $mShtorPrice->_post=$insertData;
                    $mShtorPrice->insert();
                }
            }elseif(strlen($_POST['dlina'][0]) != 0 && strlen($_POST['dlina'][1]) == 0){
                $insertData['product_id'] = intval($id);
                $insertData['shtor_ryad_id'] = intval($aShtorRyad[0]['ryads']);
                $insertData['dlina'] = intval($_POST['dlina'][0]);
                $insertData['price'] = intval($_POST['price'][0]);
                $insertData['sale_price'] = intval($_POST['sale_price'][0]);
                $mShtorPrice->_post=$insertData;
                $mShtorPrice->insert();
            }elseif(strlen($_POST['dlina'][0]) == 0 && strlen($_POST['dlina'][1]) != 0){
                $insertData['product_id'] = intval($id);
                $insertData['shtor_ryad_id'] = intval($aShtorRyad[1]['ryads']);
                $insertData['dlina'] = intval($_POST['dlina'][1]);
                $insertData['price'] = intval($_POST['price'][1]);
                $insertData['sale_price'] = intval($_POST['sale_price'][1]);
                $mShtorPrice->_post=$insertData;
                $mShtorPrice->insert();
            }else{
                exit;
            }
            $url = "product/$id";
            $this->headreUrl($url);
        }elseif($aProducts['type_id'] == '1'){
            $_oTkanPrice = new TkanPrice();
            $aTkanPrice = $_oTkanPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$id));
            if(count($aTkanPrice) != 0){
                $insertData = [];
                $insertData['product_id'] = intval($id);
                $insertData['price'] = intval($_POST['price']);
                $insertData['sale_price'] = intval($_POST['sale_price']);
                $_oTkanPrice->_put = $_POST;
                $_oTkanPrice->setId($aTkanPrice[0]['id']);
                $_oTkanPrice->update();
            }else{
                $insertData = [];
                $insertData['product_id'] = intval($id);
                $insertData['price'] = intval($_POST['price']);
                $insertData['sale_price'] = intval($_POST['sale_price']);
                $_oTkanPrice->_post=$insertData;
                $_oTkanPrice->insert();
            }
            $url = "product/$id";
            $this->headreUrl($url);
        }elseif($aProducts['type_id '] == '3'){
            var_dump('solncezashitnaya post');
        }
    }

    protected function index()
    {
//        $mProductModel = new ProductModel();
//        $aProductModel = $mProductModel->findAll(array('order'=>array('asc'=>'brand')));
//
//        $mCategories = new Categories();
//        foreach ($aProductModel as &$val){
//            if($val['categories_id'] > 0){
//                $val['categories'] = $mCategories->findById($val['categories_id']);
//            }
//        }

//        $this->result['result']=$aProductModel;
        $this->result['result'] = $this->CatsArray;
        $this->renderView("Pages/product","product",$this->result);
    }
    private function indexItems($id){
        $mProductModel = new ProductModel();
        $aProductModel = $mProductModel->findByName(array('fild_name'=>'type_id','fild_val'=>$id,'order'=>array('desc'=>'ord')));
        $this->result['result'] =$aProductModel;
        $this->renderView("Pages/product_info","product_info",$this->result);
    }
    private function item($id=false)
    {

        $mFiltrName = new FiltrName();
        $this->result['filtr_name'] = $mFiltrName->findAll(array());
//
//        $mCategories = new Categories();
//        $this->result['categories'] = $mCategories->findAll(array('order'=>array('asc'=>'ord')));

        $this->result['update'] = $id;
        if($id){

            $this->result['filtr_name_id'] = array();

            $mFiltrProduct = new FiltrProduct();
            $filtr_name_ids = $mFiltrProduct->getSelectedGroup($id);

            $filtrs_selected_ids = array();
            $filtrs_selected = $mFiltrProduct->getSelectedFiltr($id);
            foreach ($filtrs_selected as $val){
                $filtrs_selected_ids[] = $val['filtrs_id'];
            }

            $mFiltrs = new Filtrs();
            foreach ($filtr_name_ids as &$val){
                $this->result['filtr_name_id'][] = $val['id'];
                $dataFlitrs = $mFiltrs->findByMultyName(array(
                    "filtr_name_id"=>$val['id']
                ));
                foreach ($dataFlitrs as &$value){
                    if(in_array($value['id'],$filtrs_selected_ids)){
                        $value['selected'] = true;
                    }else{
                        $value['selected'] = false;
                    }
                }
                $val['filtrs'] = $dataFlitrs;
            }
//            echo "<pre>";
//            var_dump($filtr_name_ids);die;
            $this->result['filtr_name_filtrs'] = $filtr_name_ids;

            $mProductModel = new ProductModel();
            $aProductModel = $mProductModel->findById($id);
            $this->result['result']=$aProductModel;
//            echo "<pre>";
//            var_dump($aProductModel);die;
//            $mProductSlide = new ProductSlide();
//            $this->result['sprite'] = $mProductSlide->findByMultyName(
//                array(
//                    "product_id"=>$id
//                )
//            );

            if($aProductModel['type_id'] == '2'){
                $mShtorPrice = new KarnizPrice();
//            $mids = intval($id);
                $aShtorPrice = $mShtorPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$id));
//            $this->result['shtor_price'] = $aShtorPrice;

                $NewData = array();

                foreach($aShtorPrice as $key => $item)
                {
                    $NewData[$item['dlina']][$key] = $item;
                }

                ksort($NewData, SORT_NUMERIC);
//                     echo "<pre>";
//            var_dump($NewData);die;
                $this->result['shtor_price'] =$NewData;
            }elseif($aProductModel['type_id'] == '1'){
                $oTkanPrice = new TkanPrice();
                $aTkanPrice = $oTkanPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$aProductModel['id']));
                $this->result['tkan_price'] = $aTkanPrice;
//                                     echo "<pre>";
//            var_dump($aTkanPrice);die;
            }

        }


        $oShtorRyad = new KarnizRyad();
        $aShtorRyad = $oShtorRyad->findAll(array());
        $this->result['shtor_ryad'] = $aShtorRyad;



        //var_dump($this->result);die;
        $this->renderView("Pages/product_item","product",$this->result);
    }

    private function addItem()
    {

//        $insertProductData = array(
//            "sub_sub_categories_id"=>$_POST['sub_sub_categories_id'],
//            "name_ru"=>trim($_POST['name_ru']),
//            "name_arm"=>trim($_POST['name_arm']),
//            "text_ru"=>trim($_POST['text_ru']),
//            "text_arm"=>trim($_POST['text_arm']),
//            "img"=>"",
//            "made_in_ru"=>trim($_POST['made_in_ru']),
//            "made_in_arm"=>trim($_POST['made_in_arm']),
//            "price"=>trim($_POST['price']),
//            "qash"=>trim($_POST['qash']),
//            "kod"=>trim($_POST['kod']),
//            "zexch"=>trim($_POST['zexch']),
//            "zexch_exist"=>isset($_POST['zexch']) && $_POST['zexch']>0 ? 1 : 0,
//            "new"=>isset($_POST['new']) ? 1 : 0
//        );

        $_POST['type_id'] = intval($_POST['type_id']);

        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unlink($this->result['own']['images_action'].$_POST['upimg']);
                unset($_POST['upimg']);
            }
        }else{
            $_POST['image'] =$_POST['upimg'];
            unset($_POST['upimg']);
        }
        $_POST['url'] = $this->rus2translit($_POST['name']);
        $mProductModel = new ProductModel();
        $mProductModel->_post=$_POST;
        $lastId = $mProductModel->insert();

//        $mFiltrNameToProduct = new FiltrNameToProduct();
//        if(isset($_POST['filtr_name_id']) && count($_POST['filtr_name_id'])>0){
//            foreach ($_POST['filtr_name_id'] as $val){
//                $mFiltrNameToProduct->_post = array(
//                    "filtr_name_id"=>$val,
//                    "product_id"=>$lastId
//                );
//            }
//        }

        $url = "product/$lastId";
        $this->headreUrl($url);
    }

    private function updateItem($id)
    {
        unset($_POST['type_id']);

        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
//            var_dump(111);die;
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unlink($this->result['own']['images_action'].$_POST['upimg']);
                unset($_POST['upimg']);
            }
        }else{
//            var_dump(222);die;
            $_POST['image'] =$_POST['upimg'];
            unset($_POST['upimg']);
        }
        $_POST['url'] = $this->rus2translit($_POST['name']);
//        echo '<pre>';
//        var_dump($_POST);die;
        $mProductModel = new ProductModel();
        $mProductModel->_put=$_POST;
        $mProductModel->setId($id);
        $mProductModel->update();

        $url = "product/$id";
        $this->headreUrl($url);
    }





























    private function deleteItem()
    {
        $id = $_POST['id'];
        $mProductModel = new ProductModel();

        $aProductModel = $mProductModel->findById($id);
//        var_dump($aProductModel);die;
        if($aProductModel['image'] != ''){
            $imagefilename = $this->result['own']['images_action'].$aProductModel['image'];
            if (file_exists($imagefilename)) {
                unlink($imagefilename);
            }
        }

//        $mProductSlide = new ProductSlide();
//        $aProductSlide = $mProductSlide->findByMultyName(
//            array(
//                "product_id"=>$id
//            )
//        );
//        foreach ($aProductSlide as $val){
//            if($val['img'] != ''){
//                $imagefilename = $this->result['own']['images_action'].$val['img'];
//                if (file_exists($imagefilename)) {
//                    unlink($imagefilename);
//                }
//            }
//        }

        $mProductModel->delFildName = 'id';
        $mProductModel->delValue = $id;
        $mProductModel->delete();
        echo json_encode(array('error'=>true));
    }

    private function deleteSprite()
    {
        $id = $_POST['id'];
        $mProductSlide = new ProductSlide();

        $aProductSlide= $mProductSlide->findById($id);
        if($aProductSlide['img'] != ''){
            $imagefilename = $this->result['own']['images_action'].$aProductSlide['img'];
            if (file_exists($imagefilename)) {
                unlink($imagefilename);
            }
        }

        $mProductSlide->delFildName = 'id';
        $mProductSlide->delValue = $id;
        $mProductSlide->delete();
        echo json_encode(array('error'=>true));
    }

    private function deleteFiltrProduct($productId)
    {
        $mFiltrProduct = new FiltrProduct();
        $filtrs_id = $_POST['filtr_id'];
        $filtr_name_id = $_POST['filtr_name_id'];
        $deleteData = array(
            "`product_id`= $productId",
            "`filtrs_id`= $filtrs_id",
            "`filtr_name_id`= $filtr_name_id"
        );
        $mFiltrProduct->deleteByName($deleteData);
        echo json_encode(array('error'=>false));
    }

    private function addFiltrProduct($productId)
    {
        $mFiltrProduct = new FiltrProduct();
        $mFiltrProduct->_post = array(
            "product_id"=>$productId,
            "filtrs_id"=>$_POST['filtr_id'],
            "filtr_name_id"=>$_POST['filtr_name_id']
        );
        $lastId = $mFiltrProduct->insert();
        if($lastId){
            echo json_encode(array('error'=>false,'id'=>$lastId));
        }else{
            echo json_encode(array('error'=>true));
        }
    }

    private function getFiltr($filtrNameId)
    {
        $mFiltrName = new FiltrName();
        $aFiltrName = $mFiltrName->findById($filtrNameId);

        $mFiltrs = new Filtrs();
        $aFiltr = $mFiltrs->findByMultyName(array(
            "filtr_name_id"=>$filtrNameId
        ));
        echo json_encode(array('error'=>true,'data'=>$aFiltr,'filtr_name'=>$aFiltrName));
    }
}
