<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
//use Model\Categories as CategoriesModel;
use Model\PagesContent;

class Page extends BaseController
{

    public function __construct($route, $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array(
            //"url"=>"categories",
            //'controller'=>'categories',
            //"table"=>"categories",
            'images_path' => '../../../assets/images/content/',
            'images_action' => '../assets/images/content/',
        );


        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 2 && $route[0] == 'page' && is_numeric($route[1])) {
                $this->index($route[1]);
            }
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($countRoute == 2 && $route[0] == 'page' && is_numeric($route[1])) {
                $this->EditPage($route[1]);
            }
        }
    }

    private function index($id){
        $mPagesContent = new PagesContent();
        $aPagesContent = $mPagesContent->findByName(array('fild_name'=>'page_id','fild_val'=>$id));
        $this->result['result'] = $aPagesContent[0];
        $this->renderView("Pages/pagecontent", "pagecontent", $this->result);
    }


    private function EditPage($id)
    {
//        echo '<pre>';
//        var_dump($_FILES);
//        echo '<hr>';
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
//            var_dump($_POST);die;
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unlink($this->result['own']['images_action'].$_POST['upimg']);
                unset($_POST['upimg']);
            }
        }else{
            $_POST['image'] =$_POST['upimg'];
            unset($_POST['upimg']);
        }

        $mPagesContent = new PagesContent();
        $aPagesContent = $mPagesContent->findByName(array('fild_name'=>'page_id','fild_val'=>$id));
        $pid = intval($aPagesContent[0]['id']);
        $mPagesContent->_put=$_POST;
        $mPagesContent->setId($pid);
        $mPagesContent->update();
        $url = "page/$id";
        $this->headreUrl($url);
    }
}