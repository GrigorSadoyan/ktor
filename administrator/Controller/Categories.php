<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\Categories as CategoriesModel;

class Categories extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array(
            "url"=>"categories",
            'controller'=>'categories',
            "table"=>"categories",
            'images_path'=>'../../../assets/images/departments/',
            'images_action'=>'../assets/images/departments/',
        );


        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'categories') {
                $this->index();
            }elseif($countRoute == 2 && $route[0] == 'categories' && $route[1] == 'add'){
                $this->item();
            }elseif($countRoute == 2 && is_numeric($route[1])){
                $this->item($route[1]);
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 2 && $route[0] == 'categories' && $route[1]=='add'){
                $this->addItem();
            }elseif($countRoute == 2 && is_numeric($route[1])){
                $this->updateItem($route[1]);
            }elseif($countRoute == 2 && $route[0] == 'categories' && $route[1]=='delete'){
                $this->deleteItem();
            }elseif($countRoute == 2 && $route[0] == 'categories' && $route[1]=='sort'){
                $this->sortItem();
            }
        }
    }

    private function index()
    {
        $mCategoriesModel = new CategoriesModel();
        $aCategoriesModel = $mCategoriesModel->findAll(array('order'=>array('asc'=>'ord')));
        $this->result['result']=$aCategoriesModel;
        $this->renderView("Pages/categories","categories",$this->result);
    }

    private function item($id=false)
    {
        $this->result['update'] = $id;
        if($id){
            $mCategoriesModel = new CategoriesModel();
            $aCategoriesModel = $mCategoriesModel->findById($id);
            $this->result['result']=$aCategoriesModel;
        }
        //var_dump($this->result);die;
        $this->renderView("Pages/categories_item","categories",$this->result);
    }

    private function addItem()
    {
        $insertData = array(
            "name"=>trim($_POST['name'])
        );


        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $insertData['img'] = $this->uploadData['data']['img'];
            }
        }

        $mCategoriesModel = new CategoriesModel();
        $mCategoriesModel->_post=$insertData;
        $lastId = $mCategoriesModel->insert();

        $url = "categories/$lastId";
        $this->headreUrl($url);
    }

    private function updateItem($id)
    {
        $updateData = array(
            "name"=>trim($_POST['name'])
        );


        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $updateData['img'] = $this->uploadData['data']['img'];
                $imagefilename = $this->result['own']['images_action'].$_POST['upimg'];
                if (file_exists($imagefilename)) {
                    unlink($imagefilename);
                }
            }
        }

        $mCategoriesModel = new CategoriesModel();
        $mCategoriesModel->_put = $updateData;
        $mCategoriesModel->setId($id);
        $mCategoriesModel->update();

        $url = "categories/$id";
        $this->headreUrl($url);
    }

    private function deleteItem()
    {
        $id = $_POST['id'];
        $mCategoriesModel = new CategoriesModel();
        $mCategoriesModel->delFildName = 'id';
        $mCategoriesModel->delValue = $id;
        $mCategoriesModel->delete();
        echo json_encode(array('error'=>true));
    }

    private function sortItem()
    {
        $mCategoriesModel = new CategoriesModel();
        $ords = explode(',',$_POST['ords']);
        foreach ($ords as $key => $value) {
            $idVal = explode('_',$value);
            $mCategoriesModel->_put = array(
                'ord'=>$key
            );
            $mCategoriesModel->setId($idVal[1]);
            $mCategoriesModel->update();
        }
        echo json_encode(array('error'=>true));
    }
}
