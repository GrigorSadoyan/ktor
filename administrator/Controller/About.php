<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
//use Model\Categories as CategoriesModel;
use Model\About as mAbout ;
use Model\AboutItems;

class About extends BaseController
{

    public function __construct($route, $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array(
            //"url"=>"categories",
            //'controller'=>'categories',
            //"table"=>"categories",
//            'images_path' => '../../../../assets/images/content/feedback/',
//            'images_action' => '../assets/images/content/feedback/',
        );


        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'about') {
                $this->index();
            }elseif ($countRoute == 2 && $route[0] == 'about' && $route[1] == 'items' ) {
                $this->items();
            }
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($countRoute == 1 && $route[0] == 'about') {
                $this->Edit();
            }elseif ($countRoute == 2 && $route[0] == 'about' && is_numeric($route[1]) ) {
                $this->itemsUpdate($route[1]);
            }elseif ($countRoute == 2 && $route[0] == 'about' && $route[1] == 'items' ) {
                $this->itemsAdd();
            }elseif ($countRoute == 2 && $route[0] == 'about' && $route[1] == 'delete' ) {
                $this->itemsDelete();
            }elseif ($countRoute == 2 && $route[0] == 'about' && $route[1] == 'sort' ) {
                $this->itemsSort();
            }
        }
    }

    private function index(){
        $mAbout = new mAbout();
        $this->result['result'] = $mAbout->findById('1');
        $this->renderView("Pages/about", "about", $this->result);
    }
    private function Edit(){
//        echo '<pre>';
//        var_dump($_POST);die;
        $mAbout = new mAbout();
        $mAbout->_put=$_POST;
        $mAbout->setId('1');
        $mAbout->update();
        $this->headreUrl('about') ;
    }
    private function items(){
        $mAboutItems = new AboutItems();
        $this->result['result'] = $mAboutItems->findAll(array('order'=>array('asc'=>'ord')));
        $this->renderView("Pages/about_items", "about_items", $this->result);
    }
    private function itemsUpdate($id){
        $mAboutItems = new AboutItems();
        $mAboutItems->_put=$_POST;
        $mAboutItems->setId($id);
        $mAboutItems->update();
        $this->headreUrl('about/items') ;
    }
    private function itemsAdd(){
        $mAboutItems = new AboutItems();
        $mAboutItems->_post = $_POST;
        $mAboutItems->insert();
        $url = "about/items";
        $this->headreUrl($url);
    }
    private function itemsDelete(){
        $id = $_POST['id'];
        $mAboutItems = new AboutItems();
        $mAboutItems->delFildName = 'id';
        $mAboutItems->delValue = $id;
        $mAboutItems->delete();
        echo json_encode(array('error'=>true));
    }
    private function itemsSort(){
        $mAboutItems = new AboutItems();
        $ords = explode(',',$_POST['ord']);

        foreach ($ords as $key => $value) {
            $idVal = explode('_',$value);
//
            $mAboutItems->_put = array(
                'ord'=>$key
            );
            var_dump($idVal);
            $mAboutItems->setId($idVal[1]);
            $mAboutItems->update();
        }
        echo json_encode(array('error'=>true));
    }
}