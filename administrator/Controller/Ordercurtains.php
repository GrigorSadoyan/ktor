<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
//use Model\Categories as CategoriesModel;
use Model\Ordercurtains as OrdercurtainsModel;
use Model\PagesContent;

class Ordercurtains extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array(
            //"url"=>"categories",
            //'controller'=>'categories',
            //"table"=>"categories",
            'images_path'=>'../../../assets/images/content/OrderCurtains/',
            'images_action'=>'../assets/images/content/OrderCurtains/',
        );


        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'ordercurtains') {
                $this->index();
            }elseif ($countRoute == 2 && $route[0] == 'ordercurtains' && is_numeric($route[1])) {
                $this->item($route[1]);
            }elseif ($countRoute == 2 && $route[0] == 'ordercurtains' && $route[1] == 'add') {
                $this->item();
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 2 && $route[0] == 'ordercurtains' && $route[1] == 'add') {
                $this->addItem();
            }elseif ($countRoute == 2 && $route[0] == 'ordercurtains' && is_numeric($route[1])) {
                $this->UpdateItem($route[1]);
            }elseif ($countRoute == 2 && $route[0] == 'ordercurtains' && $route[1] == 'delete') {
                $this->DeleteItem();
            }elseif ($countRoute == 2 && $route[0] == 'ordercurtains' && $route[1] == 'sort') {
                $this->SortItem();
            }
        }
    }
    private function index()
    {
        $mOrdercurtainsModel = new OrdercurtainsModel();
        $aOrdercurtainsModel = $mOrdercurtainsModel->findAll(array('order'=>array('asc'=>'ord')));
        $this->result['result']=$aOrdercurtainsModel;
        $this->renderView("Pages/ordercurtains","ordercurtains",$this->result);
    }

    private function item($id=false)
    {
        //$this->result['update'] = $id;
        if($id){
            $mOrdercurtainsModel = new OrdercurtainsModel();
            $aOrdercurtainsModel = $mOrdercurtainsModel->findById($id);
            $this->result['result']=$aOrdercurtainsModel;
        }
        //var_dump($this->result);die;
        $this->renderView("Pages/ordercurtains_item","ordercurtains_item",$this->result);
    }
    private function addItem(){
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unset($_POST['upimg']);
            }
        }

        //var_dump($_POST);die;
        $mOrdercurtainsModel = new OrdercurtainsModel();
        $mOrdercurtainsModel->_post=$_POST;
        $lastId = $mOrdercurtainsModel->insert();

        $url = "ordercurtains/$lastId";
        $this->headreUrl($url);
    }
    private function UpdateItem($id){
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unlink($this->result['own']['images_action'].$_POST['upimg']);
                unset($_POST['upimg']);
            }
        }else{
            $_POST['image'] =$_POST['upimg'];
            unset($_POST['upimg']);
        }

        $mOrdercurtainsModel = new OrdercurtainsModel();
        $mOrdercurtainsModel->_put=$_POST;
        $mOrdercurtainsModel->setId($id);
        $mOrdercurtainsModel->update();
        $url = "ordercurtains/";
        $this->headreUrl($url);
    }
    private function DeleteItem(){
        $id = $_POST['id'];
        $mOrdercurtainsModel = new OrdercurtainsModel();
        $aOrdercurtainsModel = $mOrdercurtainsModel->findById($id);
        $img = $aOrdercurtainsModel['image'];
        unlink($this->result['own']['images_action'].$img);
        $mOrdercurtainsModel->delFildName = 'id';
        $mOrdercurtainsModel->delValue = $id;
        $mOrdercurtainsModel->delete();
        echo json_encode(array('error'=>true));
    }
    private function sortItem(){
        $mOrdercurtainsModel = new OrdercurtainsModel();
        $ords = explode(',',$_POST['ord']);
        //var_dump($ords);die;
        foreach ($ords as $key => $value) {
            $idVal = explode('_',$value);
            $mOrdercurtainsModel->_put = array(
                'ord'=>$key
            );
            $mOrdercurtainsModel->setId($idVal[1]);
            $mOrdercurtainsModel->update();
        }
        echo json_encode(array('error'=>true));
    }
}