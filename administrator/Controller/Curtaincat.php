<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
//use Model\Categories as CategoriesModel;
use Model\CurtainCat as CurtaincatModel;

class Curtaincat extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array(
            //"url"=>"categories",
            //'controller'=>'categories',
            //"table"=>"categories",
            'images_path'=>'../../../assets/images/content/curtaincat/',
            'images_action'=>'../assets/images/content/curtaincat/',
        );


        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'curtaincat') {
                $this->index();
            }elseif ($countRoute == 2 && $route[0] == 'curtaincat' && is_numeric($route[1])) {
                $this->item($route[1]);
            }elseif ($countRoute == 2 && $route[0] == 'curtaincat' && $route[1] == 'add') {
                $this->item();
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 2 && $route[0] == 'curtaincat' && $route[1] == 'add') {
                $this->addItem();
            }elseif ($countRoute == 2 && $route[0] == 'curtaincat' && is_numeric($route[1])) {
                $this->UpdateItem($route[1]);
            }elseif ($countRoute == 2 && $route[0] == 'curtaincat' && $route[1] == 'delete') {
                $this->DeleteItem();
            }elseif ($countRoute == 2 && $route[0] == 'curtaincat' && $route[1] == 'sort') {
                $this->SortItem();
            }
        }
    }

    private function index()
    {
        $mCurtaincatModel = new CurtaincatModel();
        $aCurtaincatModel = $mCurtaincatModel->findAll(array('order'=>array('asc'=>'ord')));
        $this->result['result']=$aCurtaincatModel;
        $this->renderView("Pages/curtaincat","curtaincat",$this->result);
    }

    private function item($id=false)
    {
        //$this->result['update'] = $id;
        if($id){
            $mCurtaincatModel = new CurtaincatModel();
            $aCurtaincatModel = $mCurtaincatModel->findById($id);
            $this->result['result']=$aCurtaincatModel;
        }
        //var_dump($this->result);die;
        $this->renderView("Pages/curtaincat_item","curtaincat_item",$this->result);
    }
    private function addItem(){
        $mCurtaincatModel = new CurtaincatModel();
        $mCurtaincatModel->_post=$_POST;
        $lastId = $mCurtaincatModel->insert();

        $url = "curtaincat";
        $this->headreUrl($url);
    }
    private function UpdateItem($id){
        $mCurtaincatModel = new CurtaincatModel();
        $mCurtaincatModel->_put=$_POST;
        $mCurtaincatModel->setId($id);
        $mCurtaincatModel->update();
        $url = "curtaincat/";
        $this->headreUrl($url);
    }
    private function DeleteItem(){
        $id = $_POST['id'];
        $mCurtaincatModel = new CurtaincatModel();
        $mCurtaincatModel->delFildName = 'id';
        $mCurtaincatModel->delValue = $id;
        $mCurtaincatModel->delete();
        echo json_encode(array('error'=>true));
    }
    private function sortItem(){
        $mCurtaincatModel = new CurtaincatModel();
        $ords = explode(',',$_POST['ord']);
        //var_dump($ords);die;
        foreach ($ords as $key => $value) {
            $idVal = explode('_',$value);
            $mCurtaincatModel->_put = array(
                'ord'=>$key
            );
            $mCurtaincatModel->setId($idVal[1]);
            $mCurtaincatModel->update();
        }
        echo json_encode(array('error'=>true));
    }
}