<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
//use Model\Categories as CategoriesModel;
use Model\Portfolio as PortfolioModel;
use Model\PortfolioGallery;

class Portfolio extends BaseController
{

    public function __construct($route, $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array(
            //"url"=>"categories",
            //'controller'=>'categories',
            //"table"=>"categories",
            'images_path' => '../../../assets/images/content/portfolio/',
            'images_action' => '../assets/images/content/portfolio/',
        );


        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'portfolio') {
                $this->index();
            } elseif ($countRoute == 2 && $route[0] == 'portfolio' && is_numeric($route[1])) {
                $this->item($route[1]);
            } elseif ($countRoute == 2 && $route[0] == 'portfolio' && $route[1] == 'add') {
                $this->item();
            }
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($countRoute == 2 && $route[0] == 'portfolio' && $route[1] == 'add') {
                $this->addItem();
            } elseif ($countRoute == 2 && $route[0] == 'portfolio' && is_numeric($route[1])) {
                $this->UpdateItem($route[1]);
            } elseif ($countRoute == 2 && $route[0] == 'portfolio' && $route[1] == 'delete') {
                $this->DeleteItem();
            } elseif ($countRoute == 2 && $route[0] == 'portfolio' && $route[1] == 'sort') {
                $this->SortItem();
            } elseif ($countRoute == 3 && $route[0] == 'portfolio' && $route[1] == 'gallery' && is_numeric($route[2])) {
                $this->UpdateGallery($route[2]);
            } elseif ($countRoute == 3 && $route[0] == 'portfolio' && $route[1] == 'gallery' && $route[2] == 'delete') {
                $this->DeleteGallery();
            } elseif ($countRoute == 3 && $route[0] == 'portfolio' && $route[1] == 'gallery' && $route[2] == 'sort') {
                $this->SortGallery();
            }
        }
    }

    private function index(){
        $mPortfolioModel = new PortfolioModel();
        $aPortfolioModel = $mPortfolioModel->findAll(array('order' => array('asc' => 'ord')));
        $this->result['result'] = $aPortfolioModel;
        $this->renderView("Pages/portfolio", "portfolio", $this->result);
    }

    private function item($id = false){
        if ($id) {
            $mPortfolioModel = new PortfolioModel();
            $mPortfolioGallery = new PortfolioGallery();
            $aPortfolio = $mPortfolioModel->findById($id);
            $this->result['result'] = $aPortfolio;
            $aPortfolioGallery = $mPortfolioGallery->findByName(array('fild_name' => 'pid', 'fild_val' => $aPortfolio['id'], 'order' => array('asc' => 'ord')));
            $this->result['gallery'] = $aPortfolioGallery;
        }
        $this->renderView("Pages/portfolio_item", "portfolio_item", $this->result);
    }

    private function addItem(){
        $gallery = [];
//        var_dump('post',$_POST);
//        var_dump('files',$_FILES);die;
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time() + 20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $gallery['image'] = $this->uploadData['data']['img'];
                unset($_POST['upimg']);
            }
        }

        $gallery['alt'] = $_POST['alt'];
        $gallery['title'] = $_POST['title'];
        unset($_POST['alt']);
        unset($_POST['title']);
        $mPortfolioModel = new PortfolioModel();
        $mPortfolioModel->_post = $_POST;
        $lastId = $mPortfolioModel->insert();
        $gallery['pid'] = $lastId;
        $mPortfolioGallery = new PortfolioGallery();
        $mPortfolioGallery->_post = $gallery;
        $mPortfolioGallery->insert();
        $url = "portfolio/$lastId";
        $this->headreUrl($url);
    }

    private function UpdateItem($id){
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time() + 20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $gallery['image'] = $this->uploadData['data']['img'];
                unlink($this->result['own']['images_action'].$_POST['upimg']);
                unset($_POST['upimg']);
            }
        } else {
            $gallery['image'] = $_POST['upimg'];
            unset($_POST['upimg']);
        }


        $gallery['alt'] = $_POST['alt'];
        $gallery['title'] = $_POST['title'];
        unset($_POST['alt']);
        unset($_POST['title']);
        $mPortfolioModel = new PortfolioModel();
        $mPortfolioModel->_put = $_POST;
        $mPortfolioModel->setId($id);
        $mPortfolioModel->update();

        if (strlen($gallery["image"]) != 0) {
            $gallery['pid'] = $id;
            $mPortfolioGallery = new PortfolioGallery();
            $mPortfolioGallery->_post = $gallery;
            $mPortfolioGallery->insert();
        }
        $url = "portfolio/$id";
        $this->headreUrl($url);
    }

    private function UpdateGallery($id){
//        var_dump($_FILES);
//        echo '<hr/>';
//        var_dump($_POST);die;
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time() + 20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unlink($this->result['own']['images_action'] . $_POST['upimg']);
                unset($_POST['upimg']);
            }
        }

        $mPortfolioGallery = new PortfolioGallery();
        $mPortfolioGallery->_put = $_POST;
        $mPortfolioGallery->setId($id);
        $mPortfolioGallery->update();

        $mPortfolioGallery = new PortfolioGallery();
        $aGallery = $mPortfolioGallery->findById($id);
        $hid = $aGallery['pid'];
        $url = "portfolio/$hid";
        $this->headreUrl($url);
    }

    private function DeleteGallery(){
        $id = $_POST['id'];
        $mPortfolioGallery = new PortfolioGallery();
        $aPortfolioModel = $mPortfolioGallery->findById($id);
        $img = $aPortfolioModel['image'];
        unlink($this->result['own']['images_action'] . $img);
        $mPortfolioGallery->delFildName = 'id';
        $mPortfolioGallery->delValue = $id;
        $mPortfolioGallery->delete();
        echo json_encode(array('error' => true));
    }

    private function SortGallery(){
        $mPortfolioGallery = new PortfolioGallery();
        $ords = explode(',', $_POST['ord']);
        //var_dump($ords);die;
        foreach ($ords as $key => $value) {
            $idVal = explode('_', $value);
            $mPortfolioGallery->_put = array(
                'ord' => $key
            );
            $mPortfolioGallery->setId($idVal[1]);
            $mPortfolioGallery->update();
        }
        echo json_encode(array('error' => true));
    }

    private function DeleteItem(){
        $id = $_POST['id'];
        $mPortfolioModel = new PortfolioModel();
        $mPortfolioGallery = new PortfolioGallery();
        $aPortfolioModel = $mPortfolioGallery->findByName(array('fild_name' => 'pid', 'fild_val' => $id));
        foreach ($aPortfolioModel as $val) {
            $img = $val['image'];
            unlink($this->result['own']['images_action'] . $img);
        }
        $mPortfolioModel->delFildName = 'id';
        $mPortfolioModel->delValue = $id;
        $mPortfolioModel->delete();
        echo json_encode(array('error' => true));
    }
    private function SortItem(){
        $mPortfolioModel = new PortfolioModel();
        $ords = explode(',', $_POST['ord']);
        //var_dump($ords);die;
        foreach ($ords as $key => $value) {
            $idVal = explode('_', $value);
            $mPortfolioModel->_put = array(
                'ord' => $key
            );
            $mPortfolioModel->setId($idVal[1]);
            $mPortfolioModel->update();
        }
        echo json_encode(array('error' => true));
    }
}