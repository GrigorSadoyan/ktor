<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
//use Model\Categories as CategoriesModel;
use Model\Album as AlbumModel;

class Album extends BaseController
{

    public function __construct($route, $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array(
            //"url"=>"categories",
            //'controller'=>'categories',
            //"table"=>"categories",
            'images_path' => '../../../../assets/images/content/album/',
            'images_action' => '../assets/images/content/album/',
        );


        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'album') {
                $this->index();
            }elseif ($countRoute == 2 && $route[0] == 'album' && is_numeric($route[1])) {
                $this->AlbumPage($route[1]);
            }elseif ($countRoute == 3 && $route[0] == 'album' && $route[1] == 'add' && is_numeric($route[2])) {
                $this->albumItem($route[2]);
            }elseif ($countRoute == 3 && $route[0] == 'album' && is_numeric($route[1]) && is_numeric($route[2])) {
                $this->albumItemEdit($route[1],$route[2]);
            }
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($countRoute == 3 && $route[0] == 'album' &&$route[1] == 'add' && is_numeric($route[2])) {
                $this->addItem($route[2]);
            }elseif ($countRoute == 3 && $route[0] == 'album' && is_numeric($route[1]) && is_numeric($route[2])) {
                $this->ItemEdit($route[1],$route[2]);
            }elseif ($countRoute == 2 && $route[0] == 'album' && $route[1] == 'sort') {
                $this->sortItems();
            }elseif ($countRoute == 2 && $route[0] == 'album' && $route[1] == 'delete') {
                $this->DeleteItem();
            }
        }
    }

    private function index(){
        $this->renderView("Pages/album", "album", $this->result);
    }
    private function AlbumPage($id){
        if($id == '1'){
            $pageName = 'НОВОСТИ КОМПАНИИ';
        }elseif ($id == '2'){
            $pageName = 'ПОЛЕЗНЫЕ СТАТЬИ';
        }elseif ($id == '3'){
            $pageName = 'ВСЕ О ТЕКСТИЛЕ';
        }
        $mAlbumModel = new AlbumModel();
        $aAlbumModel = $mAlbumModel->findByName(array('fild_name'=>'album_id','fild_val'=>$id,'order'=>array('asc'=>'ord')));
        $this->result['album_id'] = $id;
        $this->result['page_name'] = $pageName;
        $this->result['result'] = $aAlbumModel;
        $this->renderView("Pages/albuminfo", "albuminfo", $this->result);
    }
    private function albumItem($id){
        if($id == '1'){
            $pageName = 'НОВОСТИ КОМПАНИИ';
        }elseif ($id == '2'){
            $pageName = 'ПОЛЕЗНЫЕ СТАТЬИ';
        }elseif ($id == '3'){
            $pageName = 'ВСЕ О ТЕКСТИЛЕ';
        }
        $mAlbumModel = new AlbumModel();
        $aAlbumModel = $mAlbumModel->findByName(array('fild_name'=>'album_id','fild_val'=>$id));
        $this->result['result'] = $aAlbumModel;
        $this->result['page_name'] = $pageName;

        $this->renderView("Pages/albumitem", "albumitem", $this->result);
    }
    private function albumItemEdit($album_id, $id){
        if($album_id == '1'){
            $pageName = 'НОВОСТИ КОМПАНИИ';
        }elseif ($album_id == '2'){
            $pageName = 'ПОЛЕЗНЫЕ СТАТЬИ';
        }elseif ($album_id == '3'){
            $pageName = 'ВСЕ О ТЕКСТИЛЕ';
        }
        $this->result['page_name'] = $pageName;
        $mAlbumModel = new AlbumModel();
        $aAlbumModel = $mAlbumModel->findById($id);
        $this->result['result'] = $aAlbumModel;
        $this->renderView("Pages/albumitem", "albumitem", $this->result);
    }
    private function addItem($album_id)
    {
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time() + 20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unset($_POST['upimg']);
            }
        }
        $_POST['album_id'] = intval($album_id);
        //var_dump($_POST);die;
        $mAlbumModel = new AlbumModel();
        $mAlbumModel->_post = $_POST;
        $mAlbumModel->insert();

        $url = "album/$album_id";
        $this->headreUrl($url);
    }
    private function ItemEdit($album_id, $id)
    {
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unlink($this->result['own']['images_action'].$_POST['upimg']);
                unset($_POST['upimg']);
            }
        }else{
            $_POST['image'] =$_POST['upimg'];
            unset($_POST['upimg']);
        }

        $mAlbumModel = new AlbumModel();
        $mAlbumModel->_put=$_POST;
        $mAlbumModel->setId($id);
        $mAlbumModel->update();
        $url = "album/$album_id";
        $this->headreUrl($url);
    }
    private function sortItems(){
        $mAlbumModel = new AlbumModel();
        $ords = explode(',',$_POST['ord']);
        //var_dump($ords);die;
        foreach ($ords as $key => $value) {
            $idVal = explode('_',$value);
            $mAlbumModel->_put = array(
                'ord'=>$key
            );
            $mAlbumModel->setId($idVal[1]);
            $mAlbumModel->update();
        }
        echo json_encode(array('error'=>true));
    }
    private function DeleteItem(){
        $id = $_POST['id'];
        $mAlbumModel = new AlbumModel();
        $aCeniModel = $mAlbumModel->findById($id);
        $img = $aCeniModel['image'];
        unlink($this->result['own']['images_action'].$img);
        $mAlbumModel->delFildName = 'id';
        $mAlbumModel->delValue = $id;
        $mAlbumModel->delete();
        echo json_encode(array('error'=>true));
    }
}