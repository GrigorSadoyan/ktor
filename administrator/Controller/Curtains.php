<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
//use Model\Categories as CategoriesModel;
use Model\Curtains as CurtainsModel;
use Model\CurtainCat;

class Curtains extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array(
            //"url"=>"categories",
            //'controller'=>'categories',
            //"table"=>"categories",
            'images_path'=>'../../../assets/images/content/curtaincat/',
            'images_action'=>'../assets/images/content/curtaincat/',
        );


        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'curtains') {
                $this->index();
            }elseif ($countRoute == 2 && $route[0] == 'curtains' && is_numeric($route[1])) {
                $this->item($route[1]);
            }elseif ($countRoute == 2 && $route[0] == 'curtains' && $route[1] == 'add') {
                $this->item();
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 2 && $route[0] == 'curtains' && $route[1] == 'add') {
                $this->addItem();
            }elseif ($countRoute == 2 && $route[0] == 'curtains' && is_numeric($route[1])) {
                $this->UpdateItem($route[1]);
            }elseif ($countRoute == 2 && $route[0] == 'curtains' && $route[1] == 'delete') {
                $this->DeleteItem();
            }elseif ($countRoute == 2 && $route[0] == 'curtains' && $route[1] == 'sort') {
                $this->SortItem();
            }
        }
    }

    private function index()
    {
        $mCurtainsModel = new CurtainsModel();
        $aCurtainsModel = $mCurtainsModel->findAll(array('order'=>array('asc'=>'ord')));
        $this->result['result']=$aCurtainsModel;
        $this->renderView("Pages/curtains","curtains",$this->result);
    }

    private function item($id=false)
    {
        //$this->result['update'] = $id;
        if($id){
            $mCurtainsModel = new CurtainsModel();
            $aCurtainsModel = $mCurtainsModel->findById($id);
            $this->result['result']=$aCurtainsModel;
        }
        //var_dump($this->result);die;
        $mCurtainsCat = new CurtainCat();
        $aCurtainsCat = $mCurtainsCat->findAll(array());
        $this->result['cur_cats']=$aCurtainsCat;
        $this->renderView("Pages/curtains_item","curtains_item",$this->result);
    }
    private function addItem(){
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unset($_POST['upimg']);
            }
        }

        //var_dump($_POST);die;
        $mCurtainsModel = new CurtainsModel();
        $mCurtainsModel->_post=$_POST;
        $lastId = $mCurtainsModel->insert();

        $url = "curtains/$lastId";
        $this->headreUrl($url);
    }
    private function UpdateItem($id){
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unlink($this->result['own']['images_action'].$_POST['upimg']);
                unset($_POST['upimg']);
            }
        }else{
            $_POST['image'] =$_POST['upimg'];
            unset($_POST['upimg']);
        }

        $mCurtainsModel = new CurtainsModel();
        $mCurtainsModel->_put=$_POST;
        $mCurtainsModel->setId($id);
        $mCurtainsModel->update();
        $url = "curtains/";
        $this->headreUrl($url);
    }
    private function DeleteItem(){
        $id = $_POST['id'];
        $mCurtainsModel = new CurtainsModel();
        $aCeniModel = $mCurtainsModel->findById($id);
        $img = $aCeniModel['image'];
        unlink($this->result['own']['images_action'].$img);
        $mCurtainsModel->delFildName = 'id';
        $mCurtainsModel->delValue = $id;
        $mCurtainsModel->delete();
        echo json_encode(array('error'=>true));
    }
    private function sortItem(){
        $mCurtainsModel = new CurtainsModel();
        $ords = explode(',',$_POST['ord']);
        //var_dump($ords);die;
        foreach ($ords as $key => $value) {
            $idVal = explode('_',$value);
            $mCurtainsModel->_put = array(
                'ord'=>$key
            );
            $mCurtainsModel->setId($idVal[1]);
            $mCurtainsModel->update();
        }
        echo json_encode(array('error'=>true));
    }
}