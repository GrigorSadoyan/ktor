<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
//use Model\Categories as CategoriesModel;
use Model\Howwework as HowweworkModel;

class Howwework extends BaseController
{

    public function __construct($route, $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array(
            //"url"=>"categories",
            //'controller'=>'categories',
            //"table"=>"categories",
            'images_path' => '../../../assets/images/content/howwework/',
            'images_action' => '../assets/images/content/howwework/',
        );


        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'howwework') {
                $this->index();
            } elseif ($countRoute == 2 && $route[0] == 'howwework' && is_numeric($route[1])) {
                $this->item($route[1]);
            }
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($countRoute == 2 && $route[0] == 'howwework' && is_numeric($route[1])) {
                $this->UpdateItem($route[1]);
            }
        }
    }
    private function index(){
        $mHowweworkModel = new HowweworkModel();
        $this->result['result'] = $mHowweworkModel->findAll(array());
        $this->renderView("Pages/howwework","howwework",$this->result);
    }
    private function item($id){
        $mHowweworkModel = new HowweworkModel();
        $this->result['result'] = $mHowweworkModel->findById($id);
        $this->renderView("Pages/howwework_item","howwework_item",$this->result);
    }
    private function UpdateItem($id){
        $mHowweworkModel = new HowweworkModel();
        $mHowweworkModel->_put=$_POST;
        $mHowweworkModel->setId($id);
        $mHowweworkModel->update();
        $url = "howwework";
        $this->headreUrl($url);
    }
}