<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
//use Model\Categories as CategoriesModel;
use Model\SlideMain as SlideMains;

class Slidemain extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array(
            //"url"=>"categories",
            //'controller'=>'categories',
            //"table"=>"categories",
            'images_path'=>'../../../assets/images/content/glav_slider/',
            'images_action'=>'../assets/images/content/glav_slider/',
        );


        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'slidemain') {
                $this->index();
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 1 && $route[0] == 'slidemain') {
                $this->addItem();
            }elseif ($countRoute == 2 && $route[0] == 'slidemain' && $route[1] == 'delete') {
                $this->DeleteItem();
            }elseif ($countRoute == 2 && $route[0] == 'slidemain' && $route[1] == 'sort') {
                $this->SortItem();
            }
        }
    }

    private function index()
    {
        $mSlideMains = new SlideMains();
        $aSlideMains = $mSlideMains->findAll(array('order'=>array('asc'=>'ord')));
        $this->result['result']=$aSlideMains;
        $this->renderView("Pages/slidemain","slidemain",$this->result);
    }

    private function addItem(){
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unset($_POST['upimg']);
            }
        }

        //var_dump($_POST);die;
        $mSlideMains = new SlideMains();
        $mSlideMains->_post=$_POST;
        $lastId = $mSlideMains->insert();

        $url = "slidemain";
        $this->headreUrl($url);
    }
    private function DeleteItem(){
        $id = $_POST['id'];
        $mSlideMains = new SlideMains();
        $aCeniModel = $mSlideMains->findById($id);
        $img = $aCeniModel['image'];
        unlink($this->result['own']['images_action'].$img);
        $mSlideMains->delFildName = 'id';
        $mSlideMains->delValue = $id;
        $mSlideMains->delete();
        echo json_encode(array('error'=>true));
    }
    private function sortItem(){
        $mSlideMains = new SlideMains();
        $ords = explode(',',$_POST['ord']);
        foreach ($ords as $key => $value) {
            $idVal = explode('_',$value);
            $mSlideMains->_put = array(
                'ord'=>$key
            );
            $mSlideMains->setId($idVal[1]);
            $mSlideMains->update();
        }
        echo json_encode(array('error'=>true));
    }
}