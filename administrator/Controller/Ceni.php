<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
//use Model\Categories as CategoriesModel;
use Model\Ceni as CeniModel;

class Ceni extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array(
            //"url"=>"categories",
            //'controller'=>'categories',
            //"table"=>"categories",
            'images_path'=>'../../../assets/images/content/ceni/',
            'images_action'=>'../assets/images/content/ceni/',
        );


        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'ceni') {
                $this->index();
            }elseif ($countRoute == 2 && $route[0] == 'ceni' && is_numeric($route[1])) {
                $this->item($route[1]);
            }elseif ($countRoute == 2 && $route[0] == 'ceni' && $route[1] == 'add') {
                $this->item();
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 2 && $route[0] == 'ceni' && $route[1] == 'add') {
                $this->addItem();
            }elseif ($countRoute == 2 && $route[0] == 'ceni' && is_numeric($route[1])) {
                $this->UpdateItem($route[1]);
            }elseif ($countRoute == 2 && $route[0] == 'ceni' && $route[1] == 'delete') {
                $this->DeleteItem();
            }elseif ($countRoute == 2 && $route[0] == 'ceni' && $route[1] == 'sort') {
                $this->SortItem();
            }
        }
    }

    private function index()
    {
        $mCeniModel = new CeniModel();
        $aCeniModel = $mCeniModel->findAll(array('order'=>array('asc'=>'ord')));
        $this->result['result']=$aCeniModel;
        $this->renderView("Pages/ceni","ceni",$this->result);
    }

    private function item($id=false)
    {
        //$this->result['update'] = $id;
        if($id){
            $mCeniModel = new CeniModel();
            $aCeniModel = $mCeniModel->findById($id);
            $this->result['result']=$aCeniModel;
        }
        //var_dump($this->result);die;
        $this->renderView("Pages/ceni_item","ceni_item",$this->result);
    }
    private function addItem(){
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unset($_POST['upimg']);
            }
        }

        //var_dump($_POST);die;
        $mCeniModel = new CeniModel();
        $mCeniModel->_post=$_POST;
        $lastId = $mCeniModel->insert();

        $url = "ceni/$lastId";
        $this->headreUrl($url);
    }
    private function UpdateItem($id){
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unlink($this->result['own']['images_action'].$_POST['upimg']);
                unset($_POST['upimg']);
            }
        }else{
            $_POST['image'] =$_POST['upimg'];
            unset($_POST['upimg']);
        }

        $mCeniModel = new CeniModel();
        $mCeniModel->_put=$_POST;
        $mCeniModel->setId($id);
        $mCeniModel->update();
        $url = "ceni/";
        $this->headreUrl($url);
    }
    private function DeleteItem(){
        $id = $_POST['id'];
        $mCeniModel = new CeniModel();
        $aCeniModel = $mCeniModel->findById($id);
        $img = $aCeniModel['image'];
        unlink($this->result['own']['images_action'].$img);
        $mCeniModel->delFildName = 'id';
        $mCeniModel->delValue = $id;
        $mCeniModel->delete();
        echo json_encode(array('error'=>true));
    }
    private function sortItem(){
        $mCeniModel = new CeniModel();
        $ords = explode(',',$_POST['ord']);
        //var_dump($ords);die;
        foreach ($ords as $key => $value) {
            $idVal = explode('_',$value);
            $mCeniModel->_put = array(
                'ord'=>$key
            );
            $mCeniModel->setId($idVal[1]);
            $mCeniModel->update();
        }
        echo json_encode(array('error'=>true));
    }
}