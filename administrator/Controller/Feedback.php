<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
//use Model\Categories as CategoriesModel;
use Model\Feedback as Mfeedback ;

class Feedback extends BaseController
{

    public function __construct($route, $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array(
            //"url"=>"categories",
            //'controller'=>'categories',
            //"table"=>"categories",
            'images_path' => '../../../../assets/images/content/feedback/',
            'images_action' => '../assets/images/content/feedback/',
        );


        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'feedback') {
                $this->index();
            }elseif ($countRoute == 2 && $route[0] == 'feedback' && is_numeric($route[1])) {
                $this->item($route[1]);
            }elseif ($countRoute == 2 && $route[0] == 'feedback' && $route[1] == 'add' ) {
                $this->item();
            }
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($countRoute == 2 && $route[0] == 'feedback' &&$route[1] == 'add') {
                $this->addItem();
            }elseif ($countRoute == 2 && $route[0] == 'feedback' && is_numeric($route[1])) {
                $this->ItemEdit($route[1]);
            }elseif ($countRoute == 2 && $route[0] == 'feedback' && $route[1] == 'sort') {
                $this->sortItems();
            }elseif ($countRoute == 2 && $route[0] == 'feedback' && $route[1] == 'delete') {
                $this->DeleteItem();
            }
        }
    }

    private function index(){
        $mFeedback = new Mfeedback();
        $this->result['result'] = $mFeedback->findAll(array());
        $this->renderView("Pages/feedback", "feedback", $this->result);
    }
    private function item($id = false){
        $mFeedback = new Mfeedback();
        if($id){
            $this->result['result'] = $mFeedback->findById($id);
        }
        $this->renderView("Pages/feedbackinfo", "feedbackinfo", $this->result);
    }
    private function addItem(){

        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unlink($this->result['own']['images_action'].$_POST['upimg']);
                unset($_POST['upimg']);
            }
        }else{
            $_POST['image'] =$_POST['upimg'];
            unset($_POST['upimg']);
        }
//        var_dump($_POST);die;
        $mFeedback = new Mfeedback();
        $mFeedback->_post=$_POST;
        $lastId = $mFeedback->insert();
        $this->headreUrl('feedback') ;
    }
    private function ItemEdit($id){
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time()+20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath($this->result['own']['images_action']);
            $this->uploadData = $this->objImage->upload();

            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unlink($this->result['own']['images_action'].$_POST['upimg']);
                unset($_POST['upimg']);
            }
        }else{
            $_POST['image'] =$_POST['upimg'];
            unset($_POST['upimg']);
        }
//        var_dump($_POST);die;
        $mFeedback = new Mfeedback();
        $mFeedback->_put=$_POST;
        $mFeedback->setId($id);
        $mFeedback->update();
        $this->headreUrl('feedback') ;
    }
    private function DeleteItem(){
        $id = $_POST['id'];
        $mFeedback = new Mfeedback();
        $aFeed = $mFeedback->findById($id);
        unlink($this->result['own']['images_action'].$aFeed['image']);
        $mFeedback->delFildName = 'id';
        $mFeedback->delValue = $id;
        $mFeedback->delete();
        echo json_encode(array('error'=>true));
    }
}