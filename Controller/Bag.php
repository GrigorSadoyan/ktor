<?php
/**
 * Created by PhpStorm.
 * User: Grigor
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\Bag as BagModel;
use Model\Guests;
use Model\Product;
use Model\KarnizPrice;
use Model\Filtrs;
use Model\TkanPrice;
use Model\Orders;
class Bag extends BaseController
{
    public function __construct($route = FALSE, $countRoute = FALSE)
    {
        parent::__construct();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'bag') {
                $this->index();
            }else {
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($countRoute == 2 && $route[0] == 'bag' && $route[1] == 'add') {
                $this->addToBag();
            }elseif ($countRoute == 2 && $route[0] == 'bag' && $route[1] == 'update') {
                $this->updateBag();
            }elseif ($countRoute == 2 && $route[0] == 'bag' && $route[1] == 'delete') {
                $this->deleteBag();
            }elseif ($countRoute == 2 && $route[0] == 'bag' && $route[1] == 'checkout') {
                $this->checkout();
            }else {
                die();
            }
        }
    }
    /*
     *
//        ride =>
//            1  --- В пределах МКАД
//            2  --- ЗА пределами МКАД

//        type =>
//            1  --- Оплата на месте
//            2  --- Оплата онлайн
//            3  --- Самовывоз
     *
     */
    private function checkout(){
        $mOrders = new Orders();
        $mGuests = new Guests();
        $oFiltrs = new Filtrs();
        $oTkanPrice = new TkanPrice();
        $oKarnizPrice = new KarnizPrice();
        $oProduct = new Product();
        $mBagModel = new BagModel();
        $aGuests = $mGuests->findByName(array('fild_name'=>'token','fild_val'=>$_POST['guest']));
        $aBagModel = $mBagModel->findByName(array('fild_name'=>'guest_id','fild_val'=>$aGuests[0]['id']));

        foreach ($aBagModel as &$item) {
            $item['product'] = $oProduct->findById($item['product_id']);
            if($item['product']['type_id'] == '1'){
                $item['feature'] = $oFiltrs->findById($item['feature_id']);
                $item['feature']['cena'] = $oTkanPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$item['product_id']));
            }elseif($item['product']['type_id'] == '2'){
                $item['feature'] = $oKarnizPrice->findById($item['feature_id']);
            }
        }
        $price = 0;
//        for($i = 0; $i < count(); $i++){
            foreach($aBagModel as $val){
                if($val['type_id'] == '1'){
                    if(is_null($val['feature']['cena'][0]['sale_price'])){
                        $price += intval($val['count']) * intval($val['feature']['cena'][0]['price']);
                    }else{
                        $price += intval($val['count']) * intval($val['feature']['cena'][0]['sale_price']);
                    }
                }elseif($val['type_id'] == '2'){
                    if(is_null($val['feature']['sale_price'])){
                        $price +=intval($val['count']) * intval($val['feature']['price']);
                    }else{
                        $price += intval($val['count']) * intval($val['feature']['sale_price']);
                    }
                 }
            }
//        }
        $_POST['price'] = $price;
        $_POST['guest_id'] = $aGuests[0]['id'];
        unset($_POST['guest']);
        $mOrders->_post=$_POST;
        $lastId = $mOrders->insert();
        echo '<pre>';
        var_dump($lastId);die;
    }
    private function index()
    {
        $oBagModel = new BagModel();
        $oKarnizPrice = new KarnizPrice();
        $oProduct = new Product();
        $oGuests = new Guests();
        $oFiltrs = new Filtrs();
        $oTkanPrice = new TkanPrice();
        if(isset($_COOKIE['gc'])){
            $guestCook = $_COOKIE['gc'];
            $aGuests = $oGuests->findByName(array('fild_name'=>'token','fild_val'=>$guestCook));
            $guestCookId = $aGuests[0]['id'];
        }else{
            echo 'Something Went Wrong. Please Try again';
        }

        $aBag = $oBagModel->findByName(array('fild_name'=>'guest_id','fild_val'=>$guestCookId));
        foreach ($aBag as &$item) {
            $item['product'] = $oProduct->findById($item['product_id']);
                if($item['product']['type_id'] == '1'){
                    $item['feature'] = $oFiltrs->findById($item['feature_id']);
                    $item['feature']['cena'] = $oTkanPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$item['product_id']));
                }elseif($item['product']['type_id'] == '2'){
                    $item['feature'] = $oKarnizPrice->findById($item['feature_id']);
                }
//            $item['feature'] =
        }

//        echo '<pre>';
//        var_dump($aBag);die;
        $this->result['result'] = $aBag;
        $this->renderView("Pages/bag", 'bag', $this->result);
    }
    private function addToBag()
    {
        $oBagModel = new BagModel();
        $oGuests = new Guests();
        $typeId = $_POST['typeId'];
        if(isset($_COOKIE['gc'])){
            $guestCook = $_COOKIE['gc'];
            $aGuests = $oGuests->findByName(array('fild_name'=>'token','fild_val'=>$guestCook));
            $guestCookId = $aGuests[0]['id'];
        }else{
            echo 'Something Went Wrong. Please Try again';
        }
        if($typeId == '1'){
            $feature_id =$_POST['colorid'];
        }elseif($typeId == '2'){
            $feature_id =$_POST['dlinaId'];
        }
        $options = [
            'product_id' => $_POST['id'],
            'guest_id' => $guestCookId,
            'feature_id' => $feature_id,
            'type_id' => $typeId
        ];
        $aBagModel = $oBagModel->findByMultyName($options);

        if(count($aBagModel) == 1){
            $lastId = $aBagModel[0]['id'];
            $lastCount = $aBagModel[0]['count'];
            if($typeId == '1'){
                $insertData = [];
                $insertData['feature_id'] = intval($feature_id);
                $insertData['product_id'] = intval($_POST['id']);
                $insertData['count'] = intval($_POST['count']) + intval($lastCount);
                $insertData['guest_id'] =  intval($guestCookId);
                $insertData['type_id'] =  intval($typeId);
                $oBagModel->_put=$insertData;
                $oBagModel->setId($lastId);
                $oBagModel->update();
                echo json_encode(array('error' => true));
            }elseif($typeId == '2'){
                $insertData = [];
                $insertData['feature_id'] = intval($feature_id);
                $insertData['product_id'] = intval($_POST['id']);
                $insertData['count'] = intval($_POST['count']) + intval($lastCount);
                $insertData['guest_id'] = intval($guestCookId);
                $insertData['type_id'] =  intval($typeId);
                $oBagModel->_put=$insertData;
                $oBagModel->setId($lastId);
                $oBagModel->update();
                echo json_encode(array('error' => true));
            }
        }elseif(count($aBagModel) == 0){
            if($typeId == '1'){
                $insertData = [];
                $insertData['feature_id'] = intval($_POST['colorid']);
                $insertData['product_id'] = intval($_POST['id']);
                $insertData['count'] = intval($_POST['count']);
                $insertData['guest_id'] = intval($guestCookId);
                $insertData['type_id'] =  intval($typeId);
                $oBagModel->_post = $insertData;
                $oBagModel->insert();
                echo json_encode(array('error' => true));
            }elseif($typeId == '2'){
                $insertData = [];
                $insertData['feature_id'] = intval($_POST['dlinaId']);
                $insertData['product_id'] = intval($_POST['id']);
                $insertData['count'] = intval($_POST['count']);
                $insertData['guest_id'] = intval($guestCookId);
                $insertData['type_id'] =  intval($typeId);
                $oBagModel->_post = $insertData;
                $oBagModel->insert();
                echo json_encode(array('error' => true));
            }
        }
    }
    private function updateBag(){
        $id = $_POST['id'];
        $oBagModel = new BagModel();
        $insertData['count'] = intval($_POST['count']);
        $oBagModel->_put=$insertData;
        $oBagModel->setId($id);
        $oBagModel->update();
        echo json_encode(array('error' => true));
    }
    private function deleteBag(){
        $id = $_POST['id'];
        $oBagModel = new BagModel();
        $oBagModel->delFildName = 'id';
        $oBagModel->delValue = $id;
        $oBagModel->delete();
        echo json_encode(array('error'=>true));
    }
}