<?php
/**
 * Created by PhpStorm.
 * User: Grigor
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\Ceni;
use Model\PagesContent;


class Price extends BaseController
{
    public function __construct($route = FALSE, $countRoute = FALSE)
    {
        parent::__construct();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'price') {
                $this->index();
            } else {
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            die();
        }
    }

    private function index()
    {
        $oCeni = new Ceni();
        $aCeni = $oCeni->findAll(array('order'=>array('asc'=>'ord')));
        $this->result['result'] = $aCeni;

        $mPagesContent = new PagesContent();
        $CoverInfo = $mPagesContent->findByName(array('fild_name'=>'page_id','fild_val'=>'3'));
        $this->result['cover_info'] = $CoverInfo[0];

        $this->seo = array(
            'title'=>$CoverInfo[0]['seo_title'],
            'desc'=>$CoverInfo[0]['seo_desc'],
            'key'=>$CoverInfo[0]['seo_text']
        );
        $this->renderView("Pages/price", 'price', $this->result);
    }
}