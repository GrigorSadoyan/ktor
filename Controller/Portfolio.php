<?php
/**
 * Created by PhpStorm.
 * User: Grigor
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\Portfolio as PortfolioModel;
use Model\PortfolioGallery;
use Model\PagesContent;


class Portfolio extends BaseController
{
    public function __construct($route = FALSE, $countRoute = FALSE)
    {
        parent::__construct();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'portfolio') {
                $this->index();
            } else {
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            die();
        }
    }

    private function index()
    {
        $mPortfolioModel = new PortfolioModel();
        $mPortfolioGallery = new PortfolioGallery();
        $aPortfolioModel = $mPortfolioModel->findAll(array('order' => array('asc' => 'ord')));

        $this->result['portfolio_id'] = [];
        foreach ($aPortfolioModel as &$port){
            $this->result['portfolio_id'][] = $port['id'];
            $port['gallery'] = $mPortfolioGallery->findByName(array('fild_name'=>'pid','fild_val'=>$port['id'],'order'=>array('asc'=>'ord')));
        }
        $this->result['portfolio'] = $aPortfolioModel;

        $mPagesContent = new PagesContent();
        $CoverInfo = $mPagesContent->findByName(array('fild_name'=>'page_id','fild_val'=>'5'));
        $this->result['cover_info'] = $CoverInfo[0];

        $this->seo = array(
            'title'=>$CoverInfo[0]['seo_title'],
            'desc'=>$CoverInfo[0]['seo_desc'],
            'key'=>$CoverInfo[0]['seo_text']
        );
        $this->result['portfolio_id'] = json_encode($this->result['portfolio_id']);
        $this->renderView("Pages/portfolio", 'portfolio', $this->result);
    }
}