<?php
/**
 * Created by PhpStorm.
 * User: Grigor
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\Message;
class Msg extends BaseController
{
    public function __construct($route = FALSE, $countRoute = FALSE)
    {
        parent::__construct();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
//            if ($countRoute == 1 && $route[0] == 'privacy') {
//                $this->index();
//            } else {
                $this->renderNotFound('main');
                die();
//            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($countRoute == 2 && $route[0] == 'msg' && $route[1] == 'invite') {
                $this->sendToInvite();
            }

        }
    }

    private function index()
    {
        $this->renderView("Pages/privacy", 'privacy', $this->result);
    }
    private function sendToInvite(){
        $oMessage = new Message();
        $insertData = [];
        $insertData['name'] = $_POST['name'];
        $insertData['phone'] = $_POST['phone'];
        $insertData['email'] = $_POST['email'];

        if($_POST['type'] == '1'){
            $insertData['type'] = 'ПРИГЛАСИТЕ ДИЗАЙНЕРА';
        }
        $oMessage->_post=$_POST;
        $lastId = $oMessage->insert();
        if($lastId){
            $res = [
                'error'=>false,
                'msg'=>'1000'
            ];
        }else{
            $res = [
                'error'=>true
            ];
        }
        echo json_encode($res);
    }
}