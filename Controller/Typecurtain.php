<?php
/**
 * Created by PhpStorm.
 * User: Grigor
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\CurtainCat;
use Model\Curtains;
use Model\PagesContent;


class Typecurtain extends BaseController
{
    public function __construct($route = FALSE, $countRoute = FALSE)
    {
        parent::__construct();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'typecurtain') {
                $this->index();
            }elseif($countRoute == 2 && $route[0] == 'typecurtain' && is_numeric($route[1])){
                $this->item($route[1]);
            } else {
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            die();
        }
    }

    private function index()
    {
        $oCurtainCat = new CurtainCat();
        $oCurtains = new Curtains();
        $aCurtainCat = $oCurtainCat->findAll(array('order'=>array('asc'=>'ord')));

        foreach ($aCurtainCat as &$item) {
            $item['items'] = $oCurtains->findByName(array('fild_name'=>'cur_id','fild_val'=>$item['id'],'order'=>array('asc'=>'ord')));
        }

//        echo '<pre>';
//        var_dump($aCurtainCat);die;
        $this->result['result'] = $aCurtainCat;

        $mPagesContent = new PagesContent();
        $CoverInfo = $mPagesContent->findByName(array('fild_name'=>'page_id','fild_val'=>'4'));
        $this->result['cover_info'] = $CoverInfo[0];

        $this->seo = array(
            'title'=>$CoverInfo[0]['seo_title'],
            'desc'=>$CoverInfo[0]['seo_desc'],
            'key'=>$CoverInfo[0]['seo_text']
        );
        $this->renderView("Pages/typecurtain", 'typecurtain', $this->result);
    }
    private function item($id)
    {
        $oCurtains = new Curtains();
        $oCurtainCat = new CurtainCat();
        $aCurtains = $oCurtains->findById($id);
        $this->result['result_s'] = $aCurtains;



//        $aCurtainCat = $oCurtainCat->findById($aCurtains['cur_id']);
//
//        $aCurtainAll = $oCurtains->findByName(array('fild_name'=>'cur_id','fild_val'=>$aCurtainCat['id'],'order'=>array('asc'=>'ord')));
//        $this->result['curs_name'] = $aCurtainCat['name'];
//        $this->result['all_curs'] = $aCurtainAll;
//        echo '<pre>';
//        var_dump($aCurtainAll);die;
        $aCurtainCat = $oCurtainCat->findAll(array('order'=>array('asc'=>'ord')));
        foreach ($aCurtainCat as &$item) {
            $item['items'] = $oCurtains->findByName(array('fild_name'=>'cur_id','fild_val'=>$item['id'],'order'=>array('asc'=>'ord')));
        }

//        echo '<pre>';
//        var_dump($aCurtainCat);die;
        $this->result['result'] = $aCurtainCat;
        $this->renderView("Pages/typec", 'typec', $this->result);
    }
}