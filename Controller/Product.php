<?php
/**
 * Created by PhpStorm.
 * User: Grigor
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\Product as ProductModel;
use Model\Filtrs;
use Model\FiltrProduct;
use Model\FiltrName;
use Model\KarnizPrice;
use Model\TkanPrice;
use Model\KarnizRyad;

class Product extends BaseController
{
    public function __construct($route = FALSE, $countRoute = FALSE)
    {
        parent::__construct();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 4 && $route[0] == 'product' && is_numeric($route[1]) && is_string($route[2]) && is_numeric($route[3])) {
                $this->index($route[1],$route[3]);
            } else {
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            die();
        }
    }

    private function index($typeId,$id)
    {
        $oProductModel = new ProductModel();
        $aProductModel = $oProductModel->findById($id);
        $this->result['result'] = $aProductModel;


        if($aProductModel['type_id'] == '1'){
            $thisFilter =[];
            $oFiltrProduct = new FiltrProduct();
            $aFiltrProduct = $oFiltrProduct->findByName(array('fild_name'=>'product_id','fild_val'=>$id));


            $oFiltrName = new FiltrName();
            $oFiltrs = new Filtrs();
            $aFiltrName = $oFiltrName->findByName(array('fild_name'=>'view_type','fild_val'=>'1'));

            for($i = 0; $i<count($aFiltrProduct); $i++){
                if($aFiltrProduct[$i]['filtr_name_id'] == $aFiltrName[0]['id']){
                    $thisFilter[] = $aFiltrProduct[$i];
                }
            }

            foreach ($thisFilter as &$val) {
                $val['main_color'] = $oFiltrs->findById($val['filtrs_id']);

            }
            $this->result['fil_colors'] = $thisFilter;

            $mTkanPrice = new TkanPrice();
            $this->result['prices'] = $mTkanPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$id));
//            echo '<pre>';
//            var_dump($mTkanPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$id)));die;
        }elseif($aProductModel['type_id'] == '2'){

            $oKarnizPrice = new KarnizPrice();
            $aKarnizPrice = $oKarnizPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$id));
            $this->result['ryads'] = $aKarnizPrice;

            $NewData = array();

            foreach($aKarnizPrice as $key => $item)
            {
                $NewData[$item['shtor_ryad_id']][$key] = $item;
            }

            ksort($NewData, SORT_NUMERIC);
//            echo '<pre>';
//            var_dump($NewData);die;

            $this->result['data'] = $NewData;
        }
//        echo '<pre>';
//        var_dump($NewData);die;
        $this->renderView("Pages/product", 'product', $this->result);
    }
}