<?php
/**
 * Created by PhpStorm.
 * User: Grigor
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\About as mAbout ;
use Model\AboutItems;

class About extends BaseController
{
    public function __construct($route = FALSE, $countRoute = FALSE)
    {
        parent::__construct();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'about') {
                $this->index();
            } else {
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            die();
        }
    }

    private function index()
    {
        $mAbout = new mAbout();
        $this->result['result'] = $mAbout->findById('1');
        $mAboutItems = new AboutItems();
        $this->result['cifr'] = $mAboutItems->findAll(array('order'=>array('asc'=>'ord')));
        $this->renderView("Pages/about", 'about', $this->result);
    }
}