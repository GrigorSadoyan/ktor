<?php
/**
 * Created by PhpStorm.
 * User: Grigor
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\Likes;
use Model\Guests;
use Model\Product;



class Like extends BaseController
{
    public function __construct($route = FALSE, $countRoute = FALSE)
    {
        parent::__construct();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'like') {
                $this->index();
            } else {
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($countRoute == 2 && $route[0] == 'like' && $route[1] == 'add' ) {
                $this->addLike();
            }elseif ($countRoute == 2 && $route[0] == 'like' && $route[1] == 'delete' ) {
                $this->deleteLike();
            }else{
                die();
            }
        }
    }

    private function index()
    {
        $oGuests = new Guests();
        $oProduct = new Product();
        $usCok = $_COOKIE['gc'];
        $aGuests = $oGuests->findByName(array('fild_name'=>'token','fild_val'=>$usCok));
        $oLikes = new Likes();
        $aLikes = $oLikes->findByName(array('fild_name'=>'guest_id','fild_val'=>$aGuests[0]['id']));


        foreach ($aLikes as &$val){
            $val['product'] = $oProduct->findById($val['pid']);
        }
//        echo '<pre>';
//        var_dump($aLikes);die;
        $this->result['result'] = $aLikes;
        $this->renderView("Pages/like", 'like', $this->result);
    }
    private function addLike(){
        $oLikes = new Likes();
        $oGuests = new Guests();
        $token = $_COOKIE['gc'];
        $rr = $oGuests->findByName(array('fild_name'=>'token','fild_val'=>$token));

        $aLikes = $oLikes->findByMultyName(array(
                'pid'=>$_POST['pid'],
                'guest_id'=>$rr[0]['id']
            )
        );

        if(count($aLikes) > 0){
            echo  json_encode(array('error'=> true));
        }else{
            $_POST['guest_id'] = $rr[0]['id'];
            $oLikes->_post=$_POST;
            $oLikes->insert();
            echo  json_encode(array('error'=> false));
        }

    }
    private function deleteLike(){
        $id = $_POST['id'];
        $oLikes = new Likes();
        $oLikes->delFildName = 'id';
        $oLikes->delValue = $id;
        $oLikes->delete();
        echo json_encode(array('error'=>false));
    }
}