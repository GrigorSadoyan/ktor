<?php
/**
 * Created by PhpStorm.
 * User: Grigor
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\Album as AlbumModel;
class Album extends BaseController
{
    public function __construct($route = FALSE, $countRoute = FALSE)
    {
        parent::__construct();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'album') {
                $this->index();
            }else  if ($countRoute == 2 && $route[0] == 'album'  && is_numeric($route[1])) {
                $this->item($route[1]);
            } else {
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            die();
        }
    }

    private function index()
    {
        $res = [
            '0'=> '1',
            '1'=> '2',
            '2'=> '3'
        ];
        $oAlbumModel = new AlbumModel();
        for($i = 0; $i < count($res); $i++){
            $aAlbumModel[] = $oAlbumModel->findByName(array('fild_name'=>'album_id','fild_val'=>$res[$i],'order'=>array('asc'=>'ord')));
        }
        $this->result['result'] = $aAlbumModel;
//        echo '<pre>';
//        var_dump($aAlbumModel);die;
        $this->renderView("Pages/album", 'album', $this->result);
    }
    private function item($id){
        $oAlbumModel = new AlbumModel();
        $this->result['result']  = $oAlbumModel->findById($id);
        $this->result['all_result']  = $oAlbumModel->findByName(array('fild_name'=>'album_id','fild_val'=>$this->result['result']['album_id']), false);
//        echo '<pre>';
//        var_dump($this->result['all_result']);die;
        $this->renderView("Pages/album_item", 'album_item', $this->result);
    }
}