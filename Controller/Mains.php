<?php
namespace Controller;
use Core\Controller as BaseController;
use Model\KarnizRyad;
use Model\KarnizPrice;
use Model\TkanPrice;
use Model\Product;
use Model\SlideMain;
use Model\Portfolio as PortfolioModel;
use Model\PortfolioGallery;
use Model\Feedback;
use Model\Howwework;
use Model\PagesContent;


class Mains extends BaseController{

    public function __construct()
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET'){
            $this->index();
        }else{
            $this->renderNotFound('main');
            die();
        }
    }

    public function index()
    {
        $oProduct = new Product();
        $oFeedback = new Feedback();
        $oTkanPrice = new TkanPrice();
        $oShtorPrice = new KarnizPrice();
        $oHowwework = new Howwework();

        $this->result['how_we_work'] = $oHowwework->findAll(array());

        $_aTkani = $oProduct->findByName(array('fild_name'=>'type_id','fild_val'=>'1','order'=>array('asc'=>'id')));
        foreach ($_aTkani as &$val) {
            $val['prices'] = $oTkanPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$val['id']));
        }

        $this->result['tkani'] = $_aTkani;


        $aShtori = $oProduct->findByName(array('fild_name'=>'type_id','fild_val'=>'2','order'=>array('asc'=>'id')));

        foreach ($aShtori as &$val) {
            $val['prices'] = $oShtorPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$val['id'],'order'=>array('asc'=>'id')));
        }
        $this->result['shtori'] = $aShtori;

        $oSlideMain = new SlideMain();
        $aSlideMain = $oSlideMain->findAll(array('order'=>array('asc'=>'ord')));
        $this->result['m_gallery'] = $aSlideMain;

        $mPortfolioModel = new PortfolioModel();
        $mPortfolioGallery = new PortfolioGallery();
        $aPortfolioModel = $mPortfolioModel->findAll(array('order' => array('asc' => 'ord')));

        $this->result['portfolio_id'] = array();
        foreach ($aPortfolioModel as &$port){
            $this->result['portfolio_id'][] = $port['id'];
            $port['gallery'] = $mPortfolioGallery->findByName(array('fild_name'=>'pid','fild_val'=>$port['id'],'order'=>array('asc'=>'ord')));
        }
        $this->result['portfolio'] = $aPortfolioModel;

        $this->result['feedback'] = $oFeedback->findAll(array());

        $mPagesContent = new PagesContent();
        $CoverInfo = $mPagesContent->findByName(array('fild_name'=>'page_id','fild_val'=>'1'));
        $this->result['cover_info'] = $CoverInfo[0];

        $this->seo = array(
            'title'=>$CoverInfo[0]['seo_title'],
            'desc'=>$CoverInfo[0]['seo_desc'],
            'key'=>$CoverInfo[0]['seo_text']
        );
        $this->result['portfolio_id'] = json_encode($this->result['portfolio_id']);

        $this->renderView("Pages/main","main",$this->result);
    }
}
