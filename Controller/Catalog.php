<?php
/**
 * Created by PhpStorm.
 * User: Grigor
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\FiltrName;
use Model\Filtrs;
use Model\FiltrProduct;
use Model\Product;
use Model\TkanPrice;
use Model\KarnizPrice;
use Model\PagesContent;

class Catalog extends BaseController
{
    public function __construct($route = FALSE, $countRoute = FALSE)
    {
        parent::__construct();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 3 && $route[0] == 'catalog' && is_string($route[1]) && is_numeric($route[2])) {
                $this->index($route[1]);
            }else {
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($countRoute == 3 && $route[0] == 'catalog' && $route[1] == 'filtr' && $route[2] == 'change') {
                $this->changeFiltr();
            }else{
                die;
            }

        }
    }

    private function index($catUrl)
    {
        for($i = 0; $i < count($this->CatsArray); $i++){
            if($this->CatsArray[$i]['url'] == $catUrl){
                $catId = $this->CatsArray[$i]['cat_id'];
                break;
            }
        }
        $oProduct = new Product();
        $oTkanPrice = new TkanPrice();
        $oKarnizPrice = new KarnizPrice();
        $aProduct = $oProduct->findByName(array('fild_name'=>'type_id','fild_val'=>$catId,'order'=>array('asc'=>'ord')));

        foreach ($aProduct as &$val){
            if($val['type_id'] == '1'){
                $val['price'] = $oTkanPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$val['id']));
            }
            if($val['type_id'] == '2'){
                $val['price'] = $oKarnizPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$val['id']));
            }
//            if($val['type_id'] == '2'){
//                $val['price'] = $oTkanPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$val['id']));
//            }
        }
        $mPagesContent = new PagesContent();
        $this->result['products'] = $aProduct;
        $oFiltrs = new Filtrs();
        $this->result['filtrs_result'] = $oFiltrs->getCurrentFiltr($aProduct);
        if($catId == '1'){
            $pageMainTitle = 'Каталог тканей';

            $CoverInfo = $mPagesContent->findByName(array('fild_name'=>'page_id','fild_val'=>'6'));
            $this->result['cover_info'] = $CoverInfo[0];

            $this->seo = array(
                'title'=>$CoverInfo[0]['seo_title'],
                'desc'=>$CoverInfo[0]['seo_desc'],
                'key'=>$CoverInfo[0]['seo_text']
            );
        }elseif($catId == '2'){
            $pageMainTitle = 'КАРНИЗЫ';

            $CoverInfo = $mPagesContent->findByName(array('fild_name'=>'page_id','fild_val'=>'7'));
            $this->result['cover_info'] = $CoverInfo[0];

            $this->seo = array(
                'title'=>$CoverInfo[0]['seo_title'],
                'desc'=>$CoverInfo[0]['seo_desc'],
                'key'=>$CoverInfo[0]['seo_text']
            );
        }elseif($catId == '3'){
            $pageMainTitle = 'СОЛНЦЕЗАЩИТНАЯ СИСТЕМА';

            $CoverInfo = $mPagesContent->findByName(array('fild_name'=>'page_id','fild_val'=>'8'));
            $this->result['cover_info'] = $CoverInfo[0];

            $this->seo = array(
                'title'=>$CoverInfo[0]['seo_title'],
                'desc'=>$CoverInfo[0]['seo_desc'],
                'key'=>$CoverInfo[0]['seo_text']
            );
        }
//        echo '<pre>';
//        var_dump($aProduct);die;
        $this->result['pageMainTitle'] = $pageMainTitle;
        $this->renderView("Pages/catalog", 'catalog', $this->result);
    }

    private function changeFiltr()
    {
        $id = $_POST['id'];
        $typeIds = $_POST['type_id'];
        $allTypeIds = explode(',', $id);
        $oFiltrProduct = new FiltrProduct();
        $oProduct = new Product();
        $products = array();
        $NoDublicate = array();
        $aFiltrProduct = [];
        for($i = 0; $i <count($allTypeIds); $i++ ){
            $asd = $oFiltrProduct->findByName(array('fild_name'=>'filtrs_id','fild_val'=>$allTypeIds[$i]));
            for($p = 0; $p < count($asd); $p++){
                $aFiltrProduct[] = $asd[$p]['product_id'];
            }
        }

        for($k = 0; $k < count($aFiltrProduct); $k++){
            if(!in_array($aFiltrProduct[$k],$NoDublicate)){
                $NoDublicate[] = $aFiltrProduct[$k];
            }
        }
//        echo '<pre>';
//        var_dump($NoDublicate);die;
        for($j = 0; $j < count($NoDublicate); $j++){
            $pandProduct = $oProduct->findByMultyName(
                array(
                    'id' => $NoDublicate[$j],
                    'type_id' => $typeIds,
                ),true
            );

            if(isset($pandProduct['id'])){
                $products[]=$pandProduct;
            }
        }
//        echo '<pre>';
//        var_dump(11,$products);die;
        $oTkanPrice = new TkanPrice();
        $oKarnizPrice = new KarnizPrice();
        foreach ($products as &$val){
            if($val['type_id'] == '1'){
                $val['price'] = $oTkanPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$val['id']));
            }
            if($val['type_id'] == '2'){
                $val['price'] = $oKarnizPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$val['id']));
            }
//            if($val['type_id'] == '2'){
//                $val['price'] = $oTkanPrice->findByName(array('fild_name'=>'product_id','fild_val'=>$val['id']));
//            }
        }

//        echo '<pre>';
//        var_dump(77,$products);die;
        echo json_encode($products);
    }
}